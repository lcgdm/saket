class favourite {
  package{"subversion":}
  exec{"export-fav":
    command => "/usr/bin/svn export http://svn.cern.ch/guest/saket/trunk/favourite/favourite /opt/favourite",
    creates => "/opt/favourite"
  }

  Package["subversion"] -> Exec["export-fav"]
}

