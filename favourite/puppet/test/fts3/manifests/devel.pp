class fts3::devel inherits fts3 {
  yumrepo{"fts3-trunk-build":
    descr    => "FTS3 Trunk Continuous build",
    baseurl  => "http://grid-deployment.web.cern.ch/grid-deployment/dms/fts3/repos/el6/testing/\$basearch",
    gpgcheck => 0,
    enabled  => 1,
    protect  => 0
  }
}

