class fts3 {
  yumrepo{"fts3-stage-build":
    descr    => "FTS3 Stage Continuous build",
    baseurl  => "http://grid-deployment.web.cern.ch/grid-deployment/dms/fts3/repos/el6/\$basearch",
    gpgcheck => 0,
    enabled  => 1,
    protect  => 0
  }

  class{"deploy":}

  package{"fts-tests":}
  class{"favourite":}
}

