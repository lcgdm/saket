define download($url, $mode) {
  exec{"download $url":
    command => "/usr/bin/wget '$url' -O '$name'",
    creates => $name,
    require => Package["wget"]
  }

  file{"file $name":
    path => $name,
    mode => $mode
  }

  Exec["download $url"] -> File["file $name"]
}

class deploy {
  yumrepo{"egi-trustanchors":
    descr    => "EGI trust anchors",
    baseurl  => "http://repository.egi.eu/sw/production/cas/1/current/",
    gpgcheck => 0,
    enabled  => 1,
    protect  => 0
  }

  yumrepo{"epel":
    descr => "EPEL",
    baseurl => "https://dl.fedoraproject.org/pub/epel/6/\$basearch/",
    gpgcheck => 1,
    gpgkey => "file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6",
    enabled => 1,
    protect => 0
  }

  yumrepo{"epel-testing":
    descr => "EPEL Testing",
    baseurl => "https://dl.fedoraproject.org/pub/epel/testing/6/\$basearch/",
    gpgcheck => 1,
    gpgkey => "file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6",
    enabled => 1,
    protect => 0
  }

  yumrepo{"slc6-base":
    descr => "SLC6 Base",
    baseurl => "http://linuxsoft.cern.ch/cern/slc6X/\$basearch/",
    gpgcheck => 0,
    enabled => 1,
    protect => 1,
    priority => 1
  }

  yumrepo{"slc6-updates":
    descr => "SLC6 Updates",
    baseurl => "http://linuxsoft.cern.ch/cern/slc6X/updates/\$basearch/RPMS/",
    gpgcheck => 0,
    enabled => 1,
    protect => 1,
    priority => 1
  }

  # Make sure defaults are not there
  tidy {"default-repos":
    path => "/etc/yum.repos.d",
    recurse => true,
    matches => ["*.repo"]
  }

  Tidy["default-repos"] -> YumRepo<||>

  # Packages
  package{"lcg-CA":}
  package{"fetch-crl":}
  package{"voms-clients":}
  package{"wget":}

  Yumrepo <| |> -> Package <| |>

  # Run fetch-crl
  exec{"/usr/sbin/fetch-crl":
    require => [Package["fetch-crl"], Package["lcg-CA"]]
  }

  # Create voms files
  file{["/etc/vomses", "/etc/grid-security/", "/etc/grid-security/vomsdir/",
        "/etc/grid-security/vomsdir/dteam/", "/etc/grid-security/vomsdir/testers.eu-emi.eu/"]:
    ensure => directory,
  }

  file{"/etc/vomses/testers.eu-emi.eu":
    ensure => present,
    content => '"testers.eu-emi.eu" "emitestbed07.cnaf.infn.it" "15002" "/C=IT/O=INFN/OU=Host/L=CNAF/CN=emitestbed07.cnaf.infn.it" "testers.eu-emi.eu"',
    require => File["/etc/vomses/"]
  }
  file{"/etc/vomses/dteam-hellas.cern.ch":
    ensure => present,
    content => '"dteam" "voms.hellasgrid.gr" "15004" "/C=GR/O=HellasGrid/OU=hellasgrid.gr/CN=voms.hellasgrid.gr" "dteam" 24',
    require => File["/etc/vomses/"]
  }
  file{"/etc/vomses/dteam-hellas2.cern.ch":
    ensure => present,
    content => '"dteam" "voms2.hellasgrid.gr" "15004" "/C=GR/O=HellasGrid/OU=hellasgrid.gr/CN=voms2.hellasgrid.gr" "dteam" 24',
    require => File["/etc/vomses/"]
  }

  file{"/etc/grid-security/vomsdir/dteam/voms.hellasgrid.gr.lsc":
    ensure => present,
    content => "/C=GR/O=HellasGrid/OU=hellasgrid.gr/CN=voms.hellasgrid.gr
/C=GR/O=HellasGrid/OU=Certification Authorities/CN=HellasGrid CA 2006",
    require => File["/etc/grid-security/vomsdir/dteam/"]
  }
  file{"/etc/grid-security/vomsdir/dteam/voms2.hellasgrid.gr.lsc":
    ensure => present,
    content => "/C=GR/O=HellasGrid/OU=hellasgrid.gr/CN=voms2.hellasgrid.gr
/C=GR/O=HellasGrid/OU=Certification Authorities/CN=HellasGrid CA 2006",
    require => File["/etc/grid-security/vomsdir/dteam/"]
  }

  file{"/etc/grid-security/vomsdir/testers.eu-emi.eu/emitestbed07.cnaf.infn.it.lsc":
    ensure => present,
    content =>  "/C=IT/O=INFN/OU=Host/L=CNAF/CN=emitestbed07.cnaf.infn.it
/C=IT/O=INFN/CN=INFN CA",
    require => File["/etc/grid-security/vomsdir/testers.eu-emi.eu/"]
  }


  # Download test user certificates
  download{"/tmp/ucert.pem":
    url  => "http://aalvarez.web.cern.ch/aalvarez/certs/saketag-cert.pem",
    mode => 0400
  }
  download{"/tmp/ukey.pem":
    url => "http://aalvarez.web.cern.ch/aalvarez/certs/saketag-key.pem",
    mode => 0400
  }
}

