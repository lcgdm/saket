# 
# Setup for a deployment using production rpms for all but dmlite core.
#
# Packages not currently available in production but required for tests also come from bamboo.
#
# In summary:
# - productions rpms for all products release in production (epel with protect, emi2 for non-epel compliant)
# - continuous build (bamboo) rpm for dmlite core only (dmlite-libs)
# - continuous build (bamboo) rpms for lcgdm-tests*, dmlite-test*
#

#
# Required repositories.
#
class repos {

  exec{"clean-repos":
	command => "/bin/rm -f /etc/yum.repos.d/*.repo"}

  Exec["clean-repos"] -> Yumrepo <| |>

  yumrepo{
    "lcgdm-cbuild":
      baseurl  => "http://grid-deployment.web.cern.ch/grid-deployment/dms/lcgdm/repos/el$lsbmajdistrelease/\$basearch",
      descr    => "LCGDM Continuous Builds",
      enabled  => 1,
      gpgcheck => 0,
      protect  => 1,
      includepkgs => "dmlite-libs*,lcgdm-tests*,dmlite-test*,dpm-nfs-server*";
    "slc6-os":
      descr    => "Scientific Linux CERN ${lsbmajdistrelease} (SLC${lsbmajdistrelease}) base system packages",
      baseurl  => "http://linuxsoft.cern.ch/cern/slc${lsbmajdistrelease}X/\$basearch/yum/os/",
      gpgcheck => 0,
      enabled  => 1,
      protect  => 1;
    "slc6-updates":
      descr    => "Scientific Linux CERN ${lsbmajdistrelease} (SLC${lsbmajdistrelease}) bugfix and security updates",
      baseurl  => "http://linuxsoft.cern.ch/cern/slc${lsbmajdistrelease}X/\$basearch/yum/updates/",
      gpgcheck => 0,
      enabled  => 1,
      protect  => 1;
    "slc6-extras":
      descr    => "Scientific Linux CERN ${lsbmajdistrelease} (SLC${lsbmajdistrelease}) add-on packages",
      baseurl  => "http://linuxsoft.cern.ch/cern/slc${lsbmajdistrelease}X/\$basearch/yum/extras/",
      gpgcheck => 0,
      enabled  => 1,
      protect  => 1;
    "slc6-cernonly":
      descr    => "Scientific Linux CERN ${lsbmajdistrelease} (SLC${lsbmajdistrelease}) CERN-only packages",
      baseurl  => "http://linuxsoft.cern.ch/onlycern/slc${lsbmajdistrelease}X/\$basearch/yum/cernonly/",
      gpgcheck => 0,
      enabled  => 1,
      protect  => 1;
    "epel":
      descr    => "Extra Packages for Enterprise Linux add-ons",
      baseurl  => "http://linuxsoft.cern.ch/epel/${lsbmajdistrelease}/\$basearch",
      gpgcheck => 0,
      enabled  => 1,
      protect  => 1;
    "epel-testing":
      descr    => "Extra Packages for Enterprise Linux add-ons, testing",
      baseurl  => "http://linuxsoft.cern.ch/epel/testing/${lsbmajdistrelease}/\$basearch",
      gpgcheck => 0,
      enabled  => 1,
      protect  => 1;
    "egi-trustanchors":
      descr    => "EGI trust anchors",
      baseurl  => "http://repository.egi.eu/sw/production/cas/1/current/",
      gpgcheck => 0,
      enabled  => 1,
      protect  => 0;
    "emi2-base":
      descr    => "EMI 2 base",
      baseurl  => "http://emisoft.web.cern.ch/emisoft/dist/EMI/2/sl${lsbmajdistrelease}/\$basearch/base",
      gpgcheck => 0,
      enabled  => 1,
      protect  => 0;
    "emi2-updates":
      descr    => "EMI 2 updates",
      baseurl  => "http://emisoft.web.cern.ch/emisoft/dist/EMI/2/sl${lsbmajdistrelease}/\$basearch/updates",
      gpgcheck => 0,
      enabled  => 1,
      protect  => 0;
    "emi2-third-party":
      descr    => "EMI 2 third-party",
      baseurl  => "http://emisoft.web.cern.ch/emisoft/dist/EMI/2/sl${lsbmajdistrelease}/\$basearch/third-party",
      gpgcheck => 0,
      enabled  => 1,
      protect  => 0;
  }

}

# 
# Some workarounds currently needed, to be removed ASAP (probably as soon as 1.8.7 is released).
#
# We're defining packages we don't own... that's not good.
#
class workarounds {
        package{"redhat-lsb":}
	package{"finger":} # https://its.cern.ch/jira/browse/LCGDM-978
}

#
# Things should be setup in the proper order, we define all here.
#
# Only the ones that cannot go into the modules directly (like deps between daemons
# that could go in different machines) should go here.
# 
Class[Repos] -> Package <| |>
Service[mysqld] -> Class[Lcgdm::Ns::Service] 
Class[Lcgdm::Dpm::Service] -> Lcgdm::Ns::Domain <| |>
Class[Lcgdm::Dpm::Service] -> Lcgdm::Dpm::Pool <| |>
Class[Lcgdm::Ns::Service] -> Class[Lcgdm::Dpm::Service] 
Class[Lcgdm::Dpm::Service] -> Class[Lcgdm::Rfio::Service]
Class[Lcgdm::Dpm::Service] -> Class[Dmlite::Plugins::Adapter::Install]
Class[Lcgdm::Dpm::Service] -> Class[Dmlite::Srm::Service]
Class[Lcgdm::Dpm::Service] -> Class[Gridftp::Service]
Class[Dmlite::Plugins::Adapter::Install] -> Class[Dmlite::Plugins::Adapter::Config]
Class[Dmlite::Plugins::Adapter::Config] -> Class[Dmlite::Srm]
Class[Dmlite::Plugins::Adapter::Config] -> Class[Dmlite::Gridftp]
Class[Dmlite::Plugins::Adapter::Config] -> Class[Dmlite::Dav::Service]
#Class[Dmlite::Plugins::Adapter::Config] -> Class[Dmlite::Nfs]
Class[Bdii::Install] -> Class[Lcgdm::Bdii::Dpm]
Class[Lcgdm::Bdii::Dpm] -> Class[Bdii::Service]

class{"repos":}
class{"workarounds":}

#
# MySQL server setup - by default it's not installed, as it could be remote.
# 
class{"mysql::server":
	enabled	    => true,
	config_hash => {"root_password" => "root"}
}


#
# Base lcgdm configuration
# 
class{"lcgdm::base::config":}
class{"lcgdm::base::install":}

#
# Nameserver client and server configuration.
#
class{"lcgdm::ns::config":
	dbflavor 	=> "mysql",
	dbuser		=> "dpmmgrns",
	dbpass		=> "dpmmgr",
	dbhost		=> "localhost",
	dbmanage	=> "yes",
	active 		=> "yes",
	readonly	=> "no",
	disableautovids => "no",
	ulimitn		=> 4096,
	coredump	=> "yes",
	numthreads	=> 20
}
class{"lcgdm::ns::install":
	require => [Class["mysql::server"], Class["workarounds"], Class["repos"]]
}
class{"lcgdm::ns::service":}
class{"lcgdm::ns::client":}

#
# DPM daemon configuration.
#
class{"lcgdm::dpm::config":
	dbflavor => "mysql",
	dbuser   => "dpmmgrdpm",
	dbpass   => "dpmmgr",
	dbhost   => "localhost",
	dbmanage => "yes",
	active   => "yes",
	syncget  => "yes",
	ulimitn  => 4096,
	coredump => "yes",
	numfthreads => 20,
}
class{"lcgdm::dpm::install":}
class{"lcgdm::dpm::service":}

#
# RFIO configuration.
#
class{"lcgdm::rfio::install":}
class{"lcgdm::rfio::config":}
class{"lcgdm::rfio::service":}

#
# shift.conf setup.
#
class{"lcgdm::shift::config":}
lcgdm::shift::trust_entry{"DPM": component => "DPM"}
lcgdm::shift::trust_entry{"DPNS": component => "DPNS"}
# TODO: RFIO shift config?

# 
# dmlite plugins configuration (we go with the adapter for now).
# 
class{"dmlite::plugins::adapter::config":
	ns_host => "$fqdn",
	connection_timeout => 10,
	enable_io => false,
	enable_rfio => true
}
class{"dmlite::plugins::adapter::install":}

#
# Create path for domain and VOs to be enabled.
# 
lcgdm::ns::domain{"cern.ch":}
lcgdm::ns::vo{["dteam", "testers.eu-emi.eu", "testers2.eu-emi.eu"]:
	domain  => "cern.ch",
}

# 
# Pools for the tests (for now only one).
# 
file{"/storage":
	ensure  => "directory",
	group   => "dpmmgr",
	mode    => 0775,
	require => Class["lcgdm::dpm::install"] # for the required dpmmgr
}
lcgdm::dpm::pool{"the_dpm_pool":
	def_filesize => "150M"
}
lcgdm::dpm::filesystem{"$fqdn:/storage":
	pool    => "the_dpm_pool",
	server  => "$fqdn",
	fs      => "/storage",
	require => [File["/storage"], Lcgdm::Dpm::Pool["the_dpm_pool"]]
}

#
# VOMS configuration (same VOs as above).
#
class{"voms::dteam":}
class{"voms::emitesters":}

#
# Gridmapfile configuration.
# 
class{"lcgdm::mkgridmap::install":}

lcgdm::mkgridmap::file {"lcgdm-mkgridmap":
	configfile   => "/etc/lcgdm-mkgridmap.conf",
	mapfile      => "/etc/lcgdm-mapfile",
	localmapfile => "/etc/lcgdm-mapfile-local",
	logfile      => "/var/log/lcgdm-mkgridmap.log",
	groupmap     => {
		"vomss://voms.hellasgrid.gr:8443/voms/dteam?/dteam/Role=lcgadmin" => "dteam",
		"vomss://voms.hellasgrid.gr:8443/voms/dteam?/dteam/Role=production" => "dteam",
		"vomss://voms.hellasgrid.gr:8443/voms/dteam?/dteam" => "dteam",
		"vomss://emitestbed07.cnaf.infn.it:8443/voms/testers.eu-emi.eu" => "testers.eu-emi.eu",
		"vomss://emitestbed27.cnaf.infn.it:8443/voms/testers2.eu-emi.eu" => "testers2.eu-emi.eu"
	},
	localmap    => {"nobody" => "nogroup"}
}


exec{"/usr/sbin/edg-mkgridmap --conf=/etc/lcgdm-mkgridmap.conf --safe --output=/etc/lcgdm-mapfile":
	require => Lcgdm::Mkgridmap::File["lcgdm-mkgridmap"]
}

# 
# dmlite frontends configuration.
#
class{"dmlite::dav::config":
	ns_flags           => "write",
	ns_secure_redirect => "on",
	disk_flags         => "write",
	log_level          => "debug",
	coredump_dir       => "/var/log/httpd",
	ulimit             => "-c unlimited"
}
class{"dmlite::dav::install":}
class{"dmlite::dav::service":}

#class{"dmlite::nfs":}

class{"dmlite::srm":}

class{"dmlite::gridftp":}

# make sure hostkey is 0400
file{"/etc/grid-security/hostkey.pem":
     ensure => present,
     mode => 0400}

#
# bdii
#
class{"bdii::config":}
class{"bdii::install":
	selinux => false
}
class{"lcgdm::bdii::dpm":
      sitename => "CERN-TB",
      vos => ["dteam", "testers.eu-emi.eu", "testers2.eu-emi.eu"]
}
class{"bdii::service":}

