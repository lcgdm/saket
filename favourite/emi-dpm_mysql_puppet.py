#! /usr/bin/env python
##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Deployment script for dpm_mysql with Puppet
#
##############################################################################

import commands
import favourite.core
import socket
from favourite.core       import Engine, Opter
from favourite.filesystem import Append, Directory, File, Chmod
from favourite.installer  import YumRepo, Package, Downgrade
from favourite.mysql      import MySQL
from favourite.predefined import EMI
from favourite.services   import Exec
from favourite.puppet     import Base,Puppet

import favourite.wrapper
from favourite.wrapper import TestSetDefinition

class DPMHeadPuppet(EMI):
  """
  Implementation of a DPM Head node
  """

  def __init__(self):
    """
    Setup the node
    """
    super(DPMHeadPuppet, self).__init__()
    
    # Repositories
    opter = Opter.instance()
    
    #download manifest
    Exec("git clone https://github.com/cern-it-sdc-id/lcgdm-puppet-examples");
    
    Base().__init__()

    for x in range(0, 3):
        Puppet("lcgdm-puppet-examples/deployment_test/puppet-dpm/puppet_dpm_legacy_test.pp"); 
    
    # Need continuous repo for test installation
    YumRepo("lcgdm-cbuild",
            baseurl = "http://grid-deployment.web.cern.ch/grid-deployment/dms/lcgdm/repos/el6/$basearch",
            descr = "Continous builds",
            enabled = 1,
            gpgcheck = 0)

    #installing other packages needed for testing purpose
    Package(["semsg-server", "semsg-plugins-lcgdm", "dpm"])
    Package(["nagios-plugins-lcgdm", "nagios-plugins-dpm-disk", "nagios-plugins-dpm-head", "nagios-plugins-lcgdm-common"])
 
  def logs(self):
    """
    Returns a list of logs to recover
    """
    return ["/var/log/dpm/",
            "/var/log/dpns/",
            "/var/log/dpmcopy/",
            "/var/log/dpm-gsiftp/",
            "/var/log/messages",
            "/var/log/bdii/",
            "/var/log/httpd/",
            "/var/log/xrootd/redir/",
            "/var/log/xrootd/disk/",
            "/etc/sysconfig/rfiod",
            "/etc/sysconfig/globus",
            "/etc/sysconfig/lcg",
            "/etc/sysconfig/SEMsgdaemon",
            "/etc/sysconfig/dpmcopyd",
            "/etc/sysconfig/dpnsdaemon",
            "/etc/sysconfig/httpd",
            "/etc/sysconfig/xrootd",
            "/etc/sysconfig/dpm-nfs",
            "/etc/sysconfig/edg",
            "/etc/sysconfig/ldap",
            "/etc/sysconfig/dpm",
            "/etc/sysconfig/dpm-gsiftp",
            "/etc/sysconfig/nfs",
            "/var/log/rfio/",
            "/var/log/srmv2.2/",
            "/etc/httpd/conf.d",
            "/etc/dmlite.conf",
            "/etc/dmlite.conf.d/",
            "/etc/lcgdm-mapfile",
            "/etc/lcgdm-mkgridmap.conf",
            "/etc/httpd/conf/httpd.conf",
            "/etc/lcgdm-mapfile-local"]

  def test(self):
    """
    Performs the testing
    This is called by the wrapper, who expects a TestDefinition as a response
    """
    Engine.instance().deploy([Package("lcgdm-tests")],
                             label = "DPM test preparation")

    return TestSetDefinition(title = "Testing",
                             location = ["/usr/share/lcgdm/tests/functional", "/usr/share/lcgdm/tests/regression"],
                             prefixes = ["dpm", "dpns", "rfio", "dav", "nagios", "nfs", "xrootd", "gridftp"])

# Acts like entry point
if __name__ == "__main__":
  favourite.core.run(DPMHeadPuppet)
