##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Requirements involving the filesystem: Files, Directories, etc.
#
##############################################################################

import os
import os.path
import urllib2
from core import Requirement

class Directory(Requirement):
  """
  A required directory. All the elements within the path will be created.
  """

  def __init__(self, path, mode = 0755):
    """
    Constructor
    @param path The path that has to be created
    @param mode The octal mode for the folder
    """
    super(Directory, self).__init__()
    self.path = path

  def __str__(self):
    """
    String representation
    """
    return "[Directory] %s" % self.path

  def compile(self):
    """
    Bash equivalent command
    """
    return "mkdir -p %s" % self.path

  def do(self):
    """
    Perform the action
    """
    if   os.path.isdir(self.path) : return (0, "%s already exists" % self.path, "")
    elif os.path.isfile(self.path): return (1, "%s exists and it is a file" % self.path, "")
    else:
      try:                 os.makedirs(self.path)
      except Exception, e: return (1, "Error creating %s" % self.path, str(e))
    return (0, "%s created" % self.path, "")

class File(Requirement):
  """
  A required file. The parent directory will also be created
  """

  def __init__(self, path, source = None, mode = 0644, content = None):
    """
    Constructor
    @param path    The path to the file
    @param source  Where to copy the file from. [Optional]
    @param content If no source is specified, the content can be explicit [Optional]
    @param mode    The octal mode for the file
    """
    super(File, self).__init__()
    self.path    = path
    self.source  = source
    self.mode    = mode
    self.content = content

  def __str__(self):
    """
    String representation
    """
    return "[File] %s" % self.path

  def compile(self):
    """
    Bash equivalent command
    """
    cmd = "mkdir -p %s\n" % os.path.dirname(self.path)
    if self.source:    cmd += "curl -k %s -o %s" % (self.source, self.path)
    elif self.content: cmd += "cat > %s <<EOF\n%s\nEOF" % (self.path, self.content)
    else:              cmd += "touch %s\nchmod %.4o %s" % (self.path)
    cmd += "\nchmod %.4o %s" % (self.mode, self.path)
    return cmd

  def do(self):
    """
    Perform the action
    """
    if os.path.isdir(self.path):
      return (1, "%s exists and it is a directory" % self.path, "")
    elif os.path.exists(self.path):
      try:
        os.chmod(self.path, self.mode)
      except Exception, e:
        return (1, "Error changing the mode of %s" % self.path, str(e))
      return (0, "%s already exists" % self.path, "")
    else:
      try:
        file = open(self.path, "w")
        if self.source is not None: file.write(urllib2.urlopen(str(self.source)).read())
        elif self.content:          file.write(self.content)
        file.close()
        os.chmod(self.path, self.mode)
      except Exception, e:
        return (1, "Error creating %s" % self.path, str(e))
    return (0, "%s created" % self.path, "")

class Append(Requirement):
  """
  Append a line to a file if the file does not contain it already
  """

  def __init__(self, lines, path):
    """
    Constructor
    @param lines The lines to append
    @param path  The file the lines will be appended to
    """
    super(Append, self).__init__()
    if isinstance(lines, str): self.lines = lines.split("\n")
    else:                      self.lines = lines
    self.path = path

  def __str__(self):
    """
    String representation
    """
    return "[Append] %s" % self.path

  def compile(self):
    """
    Bash equivalent command
    """
    return "cat >> %s << EOF\n%s\nEOF" % (self.path, "\n".join(map(str, self.lines)))

  def do(self):
    """
    Performs the action
    """
    try:
      # Do not append lines we already have
      lines = [str(l) for l in self.lines]
      file = open(self.path, "r+")
      for l in file:
        try:    del lines[lines.index(l[:-1])] # \n is put inside
        except: pass
      # We are at the end, and we have removed the lines we don't want to
      # re-append
      for l in lines: print >>file, l
      file.close()
      return (0, "\n".join(lines), "")
    except Exception, e:
      return (1, "Could not append", str(e))

class Chmod(Requirement):
  """
  Change mode
  """

  def __init__(self, path, mode):
    """
    Constructor
    @param path    The path to the file or directory.
    @param mode    The numeric mode for the file.
    """
    super(Chmod, self).__init__()
    self.path    = path
    self.mode    = mode

  def __str__(self):
    """
    String representation
    """
    return "[Chmod] %s %o" % (self.path, self.mode)

  def compile(self):
    """
    Bash equivalent command
    """
    return "chmod %.4o %s" % (self.mode, self.path)

  def do(self):
    """
    Perform the action
    """
    try:
      os.chmod(self.path, self.mode)
    except Exception, e:
      return (1, "Error changing mode of %s" % self.path, str(e))
    return (0, "Mode of %s changed to %.4o" % (self.path, self.mode), "")

