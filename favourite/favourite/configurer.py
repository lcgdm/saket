##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Requirements involving configuration. i.e. Yaim.
#
##############################################################################

import commands
from core import Requirement

class Yaim(Requirement):
  """
  Yaim call
  """

  LOCATION = "/opt/glite/yaim/bin/yaim"

  def __init__(self, node, siteinfo = "/etc/yaim/site-info.def"):
    """
    Constructor
    @param node     The node label used to call Yaim (-n node)
    @param siteinfo Where the site-info.def file is located
    """
    super(Yaim, self).__init__()
    self.node = node
    self.info = siteinfo

  def __str__(self):
    """
    String representation
    """
    return "[Yaim] %s with %s" % (self.node, self.info)

  def compile(self):
    """
    Bash equivalent command
    """
    return "%s -c -s %s -n %s" % (Yaim.LOCATION, self.info, self.node)

  def do(self):
    """
    Perform the action
    """
    (exit, stdout) = commands.getstatusoutput(self.compile())
    return (exit, stdout, "")
