##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Class to generate the reports
#
##############################################################################

import socket
import string
import sys
import xml.dom.minidom

from datetime import datetime

class Report:
  """
  Common report format
  """

  SAKET_NAMESPACE = "https://svnweb.cern.ch/trac/saket/"

  def __init__(self, title, debugging = False):
    """
    Constructor
    @param title     The title of the report
    @param debugging If True, debug messages will be printed through stderr
    """
    impl = xml.dom.minidom.getDOMImplementation()
    self.document = impl.createDocument(Report.SAKET_NAMESPACE, "sak:batch", None)
    self.root     = self.document.documentElement

    style = self.document.createProcessingInstruction('xml-stylesheet', 'type="text/xsl" href="sak2html.xsl"')
    self.document.insertBefore(style, self.root)

    self.root.setAttribute("xmlns:sak", Report.SAKET_NAMESPACE)
    self.root.setAttribute("name", title)
    self.root.setAttribute("host", socket.gethostname())

    self.currentGroup = self.root
    self.debugging    = debugging

    self.id = 0

    self.metrics = [(False, datetime.now(), 0, 0)] # (Keep track, start, passed, failed)

  def setTitle(self, title):
    """
    Changes the report title
    """
    self.root.setAttribute("name", title)

  def printable(self, message):
    """
    Removes un-printable characters
    @param message The message to clean
    """
    if message: return "".join([c for c in message if c in string.printable])
    else:       return ""

  def openGroup(self, name, metrics = True):
    """
    Open a group
    @param name    The name of the group
    @param metrics If True, when the group is closed an element with
                   statistics will be added
    """
    group = self.document.createElementNS(Report.SAKET_NAMESPACE, 'sak:group')
    group.setAttribute('name', name)
    self.currentGroup.appendChild(group)
    self.currentGroup = group

    self.metrics.append((metrics, datetime.now(), 0, 0))

  def closeGroup(self):
    """
    Closes the last open group
    """
    if self.currentGroup != self.root:
      # Append metrics
      (track, start, passed, failed) = self.metrics.pop()
      end = datetime.now()

      if track:
        stats = self.document.createElementNS(Report.SAKET_NAMESPACE, 'sak:stats')
        stats.setAttribute('start',    start.strftime("%a %b %d %H:%M:%S %Y"))
        stats.setAttribute('end',      end.strftime("%a %b %d %H:%M:%S %Y"))
        stats.setAttribute('duration', str((end - start).seconds))
        stats.setAttribute('passed',   str(passed))
        stats.setAttribute('failed',   str(failed))
        stats.setAttribute('total',    str(passed + failed))
        self.currentGroup.appendChild(stats)
        
      self.currentGroup = self.currentGroup.parentNode

  def debug(self, message):
    """
    New debug element
    @param message The debug message
    """
    if self.debugging: print >>sys.stderr, "[DEBUG] %s" % message

  def info(self, message):
    """
    New info element
    @param message The info message
    """
    print >>sys.stderr, "[INFO]  %s" % message

    info = self.document.createElementNS(Report.SAKET_NAMESPACE, "sak:info")
    info.appendChild(self.document.createTextNode(message))

    self.currentGroup.appendChild(info)

  def warning(self, message):
    """
    New warning element
    @param message The warning message
    """
    print >>sys.stderr, "[WARN]  %s" % message

    warn = self.document.createElementNS(Report.SAKET_NAMESPACE, "sak:warning")
    warn.appendChild(self.document.createTextNode(message))

    self.currentGroup.appendChild(warn)

  def error(self, message):
    """
    New error element
    @param message The error message
    """
    print >>sys.stderr, "[ERROR] %s" % message

    error = self.document.createElementNS(Report.SAKET_NAMESPACE, "sak:error")
    error.appendChild(self.document.createTextNode(message))

    self.currentGroup.appendChild(error)

  def entry(self, name, exit, stdout, stderr, command = None):
    """
    Append an execution entry
    @param name    A identificable name for the element. e.g. the command name
    @param exit    The exit code
    @param stdout  A string containing the standard output
    @param stderr  A string containing the stantard error
    @param command The executed command. If None, a default will be written
    """
    execute = self.document.createElementNS(Report.SAKET_NAMESPACE, "sak:execute")
    execute.setAttribute("id", str(self.id))
    execute.setAttribute("name", name)
    self.id += 1

    if not command:
      execute.setAttribute("command", "- Hidden -")
    else:
      execute.setAttribute("command", command)

    execute.setAttribute("exit", str(exit))

    out = self.document.createElementNS(Report.SAKET_NAMESPACE, 'sak:stdout')
    err = self.document.createElementNS(Report.SAKET_NAMESPACE, 'sak:stderr')
    out.appendChild(self.document.createTextNode(self.printable(stdout)))
    err.appendChild(self.document.createTextNode(self.printable(stderr)))

    execute.appendChild(out)
    execute.appendChild(err)

    self.currentGroup.appendChild(execute)

    # Metric
    p = int(exit == 0)
    current = self.metrics.pop()
    self.metrics.append((current[0], current[1], current[2] + p, current[3] + (1 - p)))

  def link(self, href, content = None):
    """
    Creates a link
    @param href    The destination URL for the link
    @param content The text for the link. If not specified, href will be used
    """
    if not content:
      content = href
    link = self.document.createElementNS(Report.SAKET_NAMESPACE, 'sak:a')
    link.setAttribute('href', href)
    link.appendChild(self.document.createTextNode(content))

    self.currentGroup.appendChild(link)

  def write(self, file = sys.stdout):
    """
    Prints the report
    @param file Where to write the report
    """
    self.document.writexml(file, addindent="", newl="\n", encoding="UTF-8")

  def indexCustom(self, source, dest):
    """
    Creates a index-custom-Whatever suitable for ETICS
    @param source The original file to be linked
    @param dest   Where to write the custom HTML code
    """
    custom = open(dest, "w")

    print >>custom, """
<html>
  <head>
    <meta http-equiv="refresh" content="0; url=%s"/>
  </head>
</html>
    """ % source

    custom.close()
