##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Requirements involving the installation of software: repositories, packages...
#
##############################################################################

import urllib2
import commands
import os.path
import platform
from ConfigParser import ConfigParser
from core import Requirement

if os.path.exists("/usr/bin/yum"):
  INSTALL   = "yum --nogpgcheck -y install"
  REMOVE    = "yum -y remove"
  UPDATE    = "yum -y update"
  CLEAN     = "yum clean all"
  CHECK     = "rpm -qa | grep"
  DOWNGRADE = "yum downgrade"
  LIST_ALL  = "rpm -qa | sort"
elif os.path.exists("/usr/bin/aptitude"):
  INSTALL   = "aptitude -y install"
  REMOVE    = "aptitude -y remove"
  UPDATE    = "aptitude -y safe-upgrade"
  CLEAN     = "aptitude clean"
  CHECK     = "dpkg-query -W"
  DOWNGRADE = "echo TODO DOWNGRADE FOR DEBIAN"
  LIST_ALL  = "dpkg-query -l"
else:
  raise IOError("There is no supported package manager")

class Package(Requirement):
  """
  Required package
  """

  def __init__(self, package, enablerepo = None, tolerant = False):
    """
    Constructor
    @param package    The package name. It can be a string or a list of strings.
    @param enablerepo If specified, the parameter --enablerepo will be passed to yum with its value
    @param tolerant   Do not force general failure even if failed
    """
    super(Package, self).__init__()
    if type(package) == list: self.pkg = package
    else:                     self.pkg = [package]
    self.enablerepo = enablerepo
    self.tolerant   = tolerant

  def __str__(self):
    """
    String representation
    """
    return "[Package] " + ",".join(self.pkg)

  def compile(self):
    """
    Bash equivalent command
    """
    if self.enablerepo: return "%s --enablerepo %s %s" % (INSTALL, self.enablerepo, " ".join(self.pkg))
    else:               return "%s %s"                 % (INSTALL, " ".join(self.pkg))

  def do(self):
    """
    Perform the action
    """
    (exit, stdout) = commands.getstatusoutput(self.compile())
    # Installers do not return failure when the package can not be found
    # so double check
    for pkg in self.pkg:
      check = ""

      # Supress architecture suffix
      pkg = pkg.split('.')[0]

      if exit == 0: (exit, check) = commands.getstatusoutput("%s %s" % (CHECK, pkg))

      # If tolerant, live with this
      if exit != 0 and self.tolerant:
        exit = 0
    
      # For debugging: rpm -qa
      if exit != 0:
        (_, rpmout) = commands.getstatusoutput(LIST_ALL)
        check += "\nPackages: \n" + rpmout

    return (exit, stdout, check)

class Remove(Requirement):
  """
  Removes a package
  """
  def __init__(self, package):
    super(Remove, self).__init__()
    if type(package) == list: self.pkg = package
    else:                     self.pkg = [package]

  def __str__(self):
    """
    String representation
    """
    return "[Remove] " + ",".join(self.pkg)

  def compile(self):
    """
    Bash equivalent
    """
    return "%s %s" % (REMOVE, " ".join(self.pkg))

  def do(self):
    """
    Perform the action
    """
    (exit, stdout) = commands.getstatusoutput(self.compile())
    return (0, stdout, "")

class YumRepo(Requirement):
  """
  Required YUM repository. Will be ignored for other platforms.
  """

  def __init__(self, repo, source = None, descr = "", baseurl = None,
               enabled = 1, protect = 0, gpgkey = "", gpgcheck = 0,
               priority = None):
    """
    Constructor
    @param repo     The repository name
    @param source   If specified, the repository will be copied from here [Optional]
    @param descr    The name field in the repository [Optional]
    @param baseurl  The baseurl field [Optional]
    @param enabled  If the repository must be enabled [Optional]
    @param protect  If the repository must be protected
    @param gpgkey   The gpgkey, if any
    @param gpgcheck If the packages' signature must be checked
    @param priority If None, won't be set or modified
    """
    super(YumRepo, self).__init__()
    self.repo     = repo
    self.path     = "/etc/yum-puppet.repos.d/%s.repo" % repo
    self.source   = source
    self.descr    = descr
    self.baseurl  = baseurl
    self.enabled  = enabled
    self.protect  = protect
    self.gpgkey   = gpgkey
    self.gpgcheck = gpgcheck
    self.priority = priority

  def __str__(self):
    """
    String representation
    """
    return "[YumRepo] %s (enabled=%d)" % (self.path, self.enabled)

  def compile(self):
    """
    Bash equivalent command
    """
    # Get
    if self.source: cmd = "curl %s -o %s\n" % (self.source, self.path)
    elif self.baseurl:
      if self.priority is None: priorityLine = ""
      else:                     priorityLine = "priority=%d" % self.priority
      cmd = """cat > %s << EOF
[%s]
name=%s
baseurl=%s
enabled=%d
protect=%d
gpgkey=%s
gpgcheck=%d
%s
EOF
""" % (self.path, self.repo,
       self.descr, self.baseurl,
       self.enabled, self.protect,
       self.gpgkey, self.gpgcheck,
       priorityLine)
    else: cmd = ""

    # Attributes
    if not self.baseurl:
      cmd += "sed -i 's/enabled=.*/enabled=%d/g' %s\n" % (self.enabled, self.path)
      cmd += "sed -i 's/protect=.*/protect=%d/g' %s\n" % (self.protect, self.path)
      if self.priority is not None:
        cmd += "sed -i 's/priority=.*/priority=%d/g' %s" % (self.priority, self.path)

    # Return
    return cmd

  def do(self):
    """
    Perform the action
    """
    repoParser = ConfigParser()
    # Repo content
    if self.source:
      # Retrieve
      try:                 repoParser.readfp(urllib2.urlopen(self.source))
      except Exception, e: return (1, "Could not retrieve %s" % self.source, str(e))

    elif self.baseurl:
      # Generate
      repoParser.add_section(self.repo)
      repoParser.set(self.repo, "name",     self.descr)
      repoParser.set(self.repo, "baseurl",  self.baseurl)
      repoParser.set(self.repo, "enabled",  self.enabled)
      repoParser.set(self.repo, "protect",  self.protect)
      repoParser.set(self.repo, "gpgkey",   self.gpgkey)
      repoParser.set(self.repo, "gpgcheck", self.gpgcheck)
      if self.priority is not None:
        repoParser.set(self.repo, "priority", self.priority)
    elif os.path.isfile(self.path):
      # Try to read
      try:                 repoParser.readfp(open(self.path, "r"))
      except Exception, e: return (1, "Could not read %s", self.path, str(e))
    elif self.enabled:
      return (1, "", "The repository has to be enabled, but it doesn't exist, neither a baseurl nor a source have been specified")
    else:
      return (0, "", "The repository doesn't exist, but it is fine as it is expected to be disabled")

    # Replace properties
    for section in repoParser.sections():
      repoParser.set(section, "enabled", self.enabled)
      repoParser.set(section, "protect", self.protect)
      if self.priority is not None:
        repoParser.set(section, "priority", self.priority)
      repoParser.set(section, "gpgcheck", self.gpgcheck)

    # Write
    try:
      repoFile = open(self.path, "w")
      repoParser.write(repoFile)
      repoFile.close()
    except Exception, e:
      return (2, "Could not write %s" % self.path, str(e))

    # Re-read from disk to get debug output
    repoFile = open(self.path, "r")
    content = repoFile.read()
    repoFile.close()
    return (0, content, "")

class Update(Requirement):
  """
  Update the system.
  """

  def __init__(self, justTry = False):
    """
    Constructor
    @param justTry If set to True, an error here won't be considered a failure"
    """
    self.justTry = justTry

  def __str__(self):
    """
    String representation
    """
    return "[Update]"

  def compile(self):
    """
    Bash equivalent command
    """
    return UPDATE

  def do(self):
    """
    Perform the action
    """
    (exit, stdout) = commands.getstatusoutput(UPDATE)
    if justTry and exit != 0:
      return (0, stdout, "The update failed, but it will be ignored")
    else:
      return (exit, stdout, "")

class Clean(Requirement):
  """
  Clean the cache
  """

  def __str__(self):
    """
    String representation
    """
    return "[Clean]"

  def compile(self):
    """
    Bash equivalent command
    """
    return CLEAN

  def do(self):
    """
    Perform the action
    """
    (exit, stdout) = commands.getstatusoutput(CLEAN)
    return (exit, stdout, "")


class Downgrade(Requirement):
  """
  Downgrade a package
  """

  def __init__(self, package):
    """
    Constructor
    """
    self.package = package

  def __str__(self):
    """
    String representation
    """
    return "[Downgrade] %s" % self.package

  def compile(self):
    """
    Bash equivalent command
    """
    return "%s %s"  % (DOWNGRADE, self.package)

  def do(self):
    """
    Perform the action
    """
