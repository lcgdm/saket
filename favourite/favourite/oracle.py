##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Requirements involving Oracle: Oracle server, initialization and user
# synchronization
#
##############################################################################

import commands
import os
import os.path
import platform
import signal
import socket
import subprocess
import sys
import time
from core      import Definition, Requirement, Opter
from datetime  import datetime
from installer import Package, YumRepo

# This module requires few extra options
ORACLE_OPTIONS = {
  "oracle-conn"   : ("The Oracle connection string", False, ""),
  "oracle-user"   : ("The Oracle user account. Can be several separated by commas", False, None),
  "oracle-pass"   : ("The Oracle user password. The same has to be shared between multiple accounts if specified together", False, None),
  "oracle-postfix": ("If specified, oracle-user will be used for privileged operations, and oracle-user + oracle-postfix for the deployment", False, "_W"),
  "sync-user"     : ("The Oracle user used for synchronization", False, None),
  "sync-pass"     : ("The Oracle password for synchronization", False, None),
  "oracle-version": ("The Oracle version to use. Only 10 and 11 are supported.", False, "10")
}

Opter.instance().expand(ORACLE_OPTIONS)

# Methods
def oracleSyncRun(query):
  """
  Executes a query in sqlplus avoiding innecessary output
  @param query The SQL query
  """
  cmd = """sqlplus -S -L "%s/%s@%s" << EOF
set echo off
set pagesize 0
set pause off
set feedback 0
set termout off
WHENEVER SQLERROR EXIT FAILURE;
%s;
EOF
""" % (Opter.instance()["sync-user"], Opter.instance()["sync-pass"],
       Opter.instance()["oracle-conn"], query)

  proc = subprocess.Popen(cmd, stdin = None, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
  (stdout, stderr) = proc.communicate()

  if proc.returncode == 0: return stdout
  else:                    raise IOError("%s (%s)" % (stderr + stdout, cmd))

# The classes
class Oracle(Definition):
  """
  Base for Oracle clients
  """

  def __init__(self, version=None):
    """
    Constructor
    """
    distMajor = int(platform.dist()[1][0])
    super(Oracle, self).__init__()
    YumRepo("CERN-only",
            descr   = "SLC cernonly packages",
            baseurl = "http://linuxsoft.cern.ch/onlycern/slc%dX/$basearch/yum/cernonly" % distMajor,
            enabled = 1,
            protect = 1)

    if platform.machine() == "x86_64":
      suffix = "x86_64"
    else:
      suffix = "i386"

    packages = ["oracle-instantclient-basic", "oracle-instantclient-devel", "oracle-instantclient-sqlplus"]

    for i in xrange(len(packages)):
        if version is not None:
            packages[i] = "%s-%s" % (packages[i], version)
        packages[i] = "%s.%s" % (packages[i], suffix)

    oraVersion = Opter.instance()["oracle-version"]
    Package(packages, enablerepo = "CERN-only")
        

class OracleQuery(Requirement):
  """
  Used to execute some Oracle sql files
  """

  def __init__(self,  user, passwd, file = None, sql = None, out = None):
    """
    Constructor
    @param user   The Oracle account
    @param passwd The Oracle account password
    @param file   The SQL file
    @param sql    The SQL statement, if the file is not specified
    @param out    A file where to put the output
    """
    super(OracleQuery, self).__init__()
    self.user   = user
    self.passwd = passwd
    self.file   = file
    self.sql    = sql
    self.out    = out

  def __str__(self):
    """
    String representation
    """
    if self.file: return "[OracleQuery] %s" % self.file
    else:         return "[OracleQuery] %s" % self.sql

  def compile(self):
    """
    Bash equivalent
    """
    opter = Opter.instance()
    if self.file and self.out:
      return "yes quit\; | /usr/bin/sqlplus -S -L \"%s/%s@%s\" @%s > %s" % (self.user,
                                                               self.passwd,
                                                               opter["oracle-conn"],
                                                               self.file, self.out)
    elif self.file:
      return "yes quit\; | /usr/bin/sqlplus -S -L \"%s/%s@%s\" @%s" % (self.user,
                                                          self.passwd,
                                                          opter["oracle-conn"],
                                                          self.file)
    elif self.out:
      return "yes quit\; | /usr/bin/sqlplus -S -L \"%s/%s@%s\" > %s << EOF\n%s\nEOF" % (self.user,
                                                                           self.passwd,
                                                                           opter["oracle-conn"],
                                                                           self.out,
                                                                           self.sql)
    else:
      return "yes quit\; | /usr/bin/sqlplus -S -L \"%s/%s@%s\" << EOF\n%s\nEOF" % (self.user,
                                                                      self.passwd,
                                                                      opter["oracle-conn"],
                                                                      self.sql)

  def do(self):
    """
    Perform the action
    """
    proc = subprocess.Popen(self.compile(), shell = True, stdin = None, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    (stdout, stderr) = proc.communicate()
    exit = proc.returncode
    return (exit, stdout, stderr)

class SafeOracleAccount(Requirement):
  """
  Ensures a free oracle user is used, when more than one is available
  This MUST be called before any other element that uses the Oracle user
  """

  def __init__(self):
    """
    Constructor
    """
    super(SafeOracleAccount, self).__init__()
    opter = Opter.instance()
    self.dict = {'oracle-user'    : opter['oracle-user'],
                 'oracle-pass'    : opter['oracle-pass'],
                 'oracle-conn'    : opter['oracle-conn'],
                 'oracle-location': 'undefined'}
    if self.dict['oracle-user'] is not None:
      self.dict['oracle-unpriv'] = opter['oracle-user'] + opter['oracle-postfix']
    else:
      self.dict['oracle-unpriv'] = None

  def __str__(self):
    """
    String representation
    """
    return "[Oracle Sync]"

  def daemon(self):
    """
    Return directly when parent. Stay in the background updating the database
    if child
    """
    if os.fork() > 0:
      return

    # Detach from calling environment
    os.chdir("/")
    os.setsid()
    os.umask(0)

    log = open("/tmp/favora.log", "a+")
    sys.stdout.flush()
    sys.stderr.flush()
    os.dup2(log.fileno(), sys.stdout.fileno())
    os.dup2(log.fileno(), sys.stderr.fileno())
    os.close(sys.stdin.fileno())

    # Second fork
    if os.fork() > 0:
      sys.exit(0) # This forces the child to be orphan => init adopts it

    # Need the pid for later
    pidfile = open("/tmp/favora.pid", "w")
    print >>pidfile, os.getpid()
    pidfile.close()

    # And the loop
    try:
      print "Favourite Oracle Daemon started at %s" % datetime.now().strftime("%H:%M:%S %d/%m/%Y")
      while True:
        oracleSyncRun("UPDATE YG_USER_SYNC SET LAST_UPDATE=CURRENT_TIMESTAMP, HOST='%s' WHERE ACCOUNT='%s'" % (socket.gethostname(), self.dict["oracle-user"]))
        print "[%s] Updated %s" % (datetime.now().strftime("%H:%M:%S %d/%m/%Y"), self.dict["oracle-user"])
        time.sleep(60)
    except Exception, e:
      print >>log, str(e)
      sys.exit(1)

  def do(self):
    """
    Performs the action
    """
    opter    = Opter.instance()
    # Check
    if not opter["oracle-user"]:
      return (1, "", "When SafeOracleAccount is used, oracle-user must be specified!")

    # Find oracle location
    (exit, out) = commands.getstatusoutput("which sqlplus")
    if exit != 0:
      raise IOError("Could not resolve sqlplus")
    self.dict["oracle-location"] = os.path.realpath(out)[:-19]

    # List of users
    accounts = opter["oracle-user"].split(",")

    # Check sync
    if not opter["sync-user"] or not opter["sync-pass"]:
      if len(accounts) > 1:
        return (1, "", "When multiple users are specified, sync-user and sync-pass must be specified.")
      else:
        return (0, "No synchronization mechanism will be used. Be careful.", "")

    # Create the table if it doesn't exist
    try:
      oracleSyncRun("DESCRIBE YG_USER_SYNC")
    except:
      oracleSyncRun("CREATE TABLE YG_USER_SYNC (ACCOUNT varchar2(32), HOST varchar(256), LAST_UPDATE TIMESTAMP)")


    # This string will be useful for queries
    userList = ",".join(["'%s'" % a for a in accounts])

    # Insert users that are not in the database
    defined = oracleSyncRun("SELECT ACCOUNT FROM YG_USER_SYNC WHERE ACCOUNT IN (%s)" % userList).split()
    for a in accounts:
      if a not in defined: oracleSyncRun("INSERT INTO YG_USER_SYNC (ACCOUNT, HOST) VALUES ('%s', '%s')" % (a, socket.gethostname()))

    # Pick a free account
    attemp    = 0
    available = []
    while True:
      available = oracleSyncRun("SELECT ACCOUNT FROM YG_USER_SYNC WHERE (LAST_UPDATE < (CURRENT_TIMESTAMP - interval '2' minute) OR LAST_UPDATE IS NULL) AND ACCOUNT IN (%s)" % userList).split()
      attemp += 1
      if len(available) == 0:
        attemp += 1
        if attemp >= 3:
          return (1, "", "Could not find a free account")
        print >>sys.stderr, "Attemp %d: No free account found. Waiting..." % attemp
        time.sleep(120)
      else:
        break

    # Update internal dictionary
    self.dict["oracle-user"]   = available[0]
    self.dict["oracle-unpriv"] = available[0] + opter["oracle-postfix"]
    self.dict["oracle-pass"]   = opter["oracle-pass"]
    self.dict["oracle-conn"]   = opter["oracle-conn"]
    

    # Kill previous
    if os.path.isfile("/tmp/favora.pid"):
      pidfile = open("/tmp/favora.pid", "r")
      prevpid = int(pidfile.read())
      pidfile.close()
      try:    os.kill(prevpid, signal.SIGTERM)
      except: pass # Maybe already dead

    # Spawn the daemon to update the database
    self.daemon()

    # End of this
    return (0, "Found %s" % self.dict["oracle-user"], "")

  def __getitem__(self, key):
    """
    Allows to access to SafeOracleAccount instances as a dictionary
    """
    return self.dict[key]

class OraclePurge(Requirement):
  """
  Purge all content from the database
  """

  def __init__(self, user, passwd):
    """
    Constructor
    @param user   The user
    @param passwd The password
    """
    super(OraclePurge, self).__init__()
    self.user   = user
    self.passwd = passwd

  def __str__(self):
    """
    String representation
    """
    return "[Oracle Purge] %s" % str(self.user)

  def compile(self):
    """
    Bash equivalent
    """
    opter = Opter.instance()
    return """/usr/bin/sqlplus -S -L \"%s/%s@%s\" << EOF
set feedback off
set pagesize 0
spool /tmp/AllObjectsDrop.sql
select 'drop view '||view_name||';' from user_views;
select distinct 'drop sequence '||sequence_name|| ';'from user_sequences;
select distinct 'drop table '||table_name|| ';'from user_tables;
select distinct 'drop procedure '||name|| ';'from user_source where type = 'procedure';
select distinct 'drop function '||name|| ';'from user_source where type = 'function';
select distinct 'drop package '||name|| ';'from user_source where type = 'package';
select 'drop synonym '||synonym_name||';' from user_synonyms where synonym_name not like 'sta%%' and synonym_name like 's_%%'
spool off
EOF;
/usr/bin/sqlplus -S -L \"%s/%s@%s\" @/tmp/AllObjectsDrop.sql""" % (self.user, self.passwd, opter["oracle-conn"],
                                                                   self.user, self.passwd, opter["oracle-conn"])

  def do(self):
    """
    Perform the action
    """
    proc = subprocess.Popen(self.compile(), shell = True, stdin = None, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    (stdout, stderr) = proc.communicate()
    exit = proc.returncode
    return (exit, stdout, stderr)
