#! /usr/bin/env python
##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# This is the deployer core. Defines the minimum necessary to work. The rest
# should be extendible.
#
##############################################################################

import os
import copy
import getopt
import inspect
import os.path
import shutil
import subprocess
import sys
import threading
import traceback
from Queue  import Queue
from report import Report

VERSION = ("0.0.1")

# The values follow the schema (Description, Flag?, Default)
BASE_OPTIONS = {
  "help"    : ("Help about this program", True, False),
  # Log
  "debug"   : ("Show debug information", True, False),
  "log-dir" : ("Where to put the logs" , False, "/tmp"),
  # Timeout
  "timeout" : ("Timeout for executions", False, 1800)
}

# Options defined by the core itself
CORE_OPTIONS = {
  "log-deploy": ("The log name for the deployment", False, "favourite.xml"),
  "log-copy"  : ("Log or folder to recover", False, []),
  # Deployment
  "just-tell" : ("Show the descriptions of the steps that would be taken", True, False),
  "compile"   : ("Print the Bash equivalent code", True, False),
  "repository": ("Repository to be used" , False, [])
}

# Constants
FAILED  = 1
SUCCESS = 0

# a utility like shutil.copytree, but only attempt to copy regular files or their sym links
# (we maybe using a python which doesn't have the 'ignore' option for standard shutil.copytree)
def mycopytree(src, dst):
  names = os.listdir(src)
  os.makedirs(dst)
  errors = []
  for name in names:
    srcname = os.path.join(src, name)
    dstname = os.path.join(dst, name)
    try:
      if os.path.isdir(srcname):
        mycopytree(srcname, dstname)
      elif os.path.isfile(srcname):
        shutil.copy2(srcname, dstname)
    except (IOError, os.error), why:
      errors.append((srcname, dstname, str(why)))
    except Error, err:
      errors.extend(err.args[0])
  try:
    shutil.copystat(src, dst)
  except OSError, why:
    errors.extend((src, dst, str(why)))
  if errors:
    raise Error, errors

# Option handler
class Opter(object):
  """
  Keeps track of system attributes and explicit parameters
  """
  _instance = None

  @staticmethod
  def instance():
    """
    Singleton implementation
    """
    if not Opter._instance:
      Opter._instance = Opter()
    return Opter._instance

  def __init__(self, default = BASE_OPTIONS):
    """
    Constructor
    @param default Default options to attach
    """
    self.internal = {}
    self.values   = {}
    self.expand(default)

  def __getitem__(self, key):
    """
    Return value
    @param key The corresponding parameter
    """
    return self.values[key]

  def expand(self, opts):
    """
    Expand the given options
    @param opts Is a dictionary following the supported structure
    """
    self.internal.update(opts)
    for (o, t) in self.internal.iteritems():
      self.values[o] = copy.copy(t[2])

  def set(self, opts):
    """
    Clear the existing options and append the new
    @param opts Is a dictionary following the supported structure
    """
    self.internal.clear()
    self.values.clear()
    self.expand(opts)

  def help(self):
    """
    Prints the help for the program
    """

    print "Favourite %s" % VERSION
    print "Director script for automated deployment and testing of DMS components"
    print "Usage: %s [options]" % sys.argv[0]
    print

    for opt in sorted(self.internal.keys()):
      t = self.internal[opt]
      print "\t--%s\t%s. Default %s" % (opt, t[0], str(t[2]))

    print
    print "Send and email to saket.dms@cern.ch if you have questions"
    print "regarding the use of this software, to submit patches or suggest improvements."

  def parse(self, argv = sys.argv[1:]):
    """
    Parse the arguments and set some facts from it
    @param argv The argument array
    """
    # Initialize recognised options
    options = []
    for (o, t) in self.internal.iteritems():
      if t[1]: options.append(o)
      else:    options.append(o + "=")

    # Get from the vector and set up
    (opts, args) = getopt.getopt(argv, "", options)
    for o, a in opts:
      id = o[2:]
      if not a:                           self.values[id] = True
      elif type(self.values[id]) == list: self.values[id].append(a)
      elif type(self.values[id]) == int:  self.values[id] = int(a)
      else:                               self.values[id] = a

# Class that wraps Requirement properties so they are
# resolved each time the instance is used
class Property(object):
  """
  Class that wraps Requirement properties so they are
  resolved each time the instance is used
  """

  def __init__(self, o, format, *args):
    """
    Constructor
    @param o The object
    @param format Will format the string with the property specified [optional]
    """
    self.o = o
    self.p = args
    self.format = format

  def __str__(self):
    """
    String value of the field
    """
    values = []

    for pname in self.p:
      if pname in dir(self.o):           v = getattr(self.o, pname)
      elif '__getitem__' in dir(self.o): v = self.o[pname]
      else:                              v = "TBD(%s)" % pname
      values.append(v)

    return self.format % tuple(values)

  def __repr__(self):
    """
    Representation
    """
    return str(self)


# Base class for deployment requirements
class Requirement(object):
  """
  Base class for requirements
  """

  def __init__(self):
    """
    Constructor
    @param active If the object will be added to the list by default
    """
    # Get who calls us, but not being us
    # (Inheritance)
    i = 0
    stack = inspect.stack()

    while i < len(stack):
      frame = stack[i][0]
      
      if "self" not in frame.f_locals:
        self.parent = None
        break
      self.parent = frame.f_locals["self"]

      if not self is self.parent: break
      i += 1

    # No parent
    if not self.parent:  return

    # Now, if the caller is a definition, autoappend
    if isinstance(self.parent, Definition): self.parent.append(self)

  def get(self, key):
    """
    Returns a Property instance wrapping the propery key
    @param key A string
    """
    return Property(self, key)

  def compile(self):
    """
    Bash equivalent command
    """
    return "# %s does not provide a Bash equivalent" % type(self).__name__

  def do(self):
    """
    Performs the action. Must return a tuple like (code, stdout, stderr)
    """
    return (1, "", "Not implemented")

# Base class for manifests
class Definition(Requirement):
  """
  Base class for manifests
  """

  def __init__(self):
    """
    Constructor
    """
    super(Definition, self).__init__()
    self.subtasks = []

  def __str__(self):
    """
    String representation
    """
    string = ""
    ldoc   = 0
    if self.__doc__:
      doc  = self.__doc__.strip()
      ldoc = len(doc)
      string += "\n%s\n%s\n" % (doc, ">" * ldoc)
    string += "\n".join([str(s) for s in self.subtasks])
    string += "\n%s" % ("<" * ldoc)
    return string

  def __iter__(self):
    """
    Iterator
    """
    return self.subtasks.__iter__()

  def append(self, sub):
    """
    Append a task to the subtask list
    @param sub A requirement instance
    """
    if not isinstance(sub, Requirement):
      raise ValueError("Must be a Requirement instance")
    
    if sub not in self.subtasks:
      self.subtasks.append(sub)

  def remove(self, sub):
    """
    Removes a task from the subtask list
    @param sub Removes a task from the list, if found
    """
    del self.subtasks[self.subtasks.index(sub)]
    
  def compile(self):
    """
    Bash equivalent command
    """
    return "\n".join([s.compile() for s in self.subtasks])

  def do(self):
    """
    Overrides the method from Requirement
    """
    raise RuntimeError("Can not call the method do directly on Definitions")

# Safe execution of anything (with timeout)
class SafeExecution:
  """
  Safe execution of anything (with timeout)
  To be used as decorator
  """

  class Thread(threading.Thread):
    """
    Thread implementation to surround the call
    """

    def __init__(self, retQ, f, *args, **kwargs):
      """
      Constructor
      @param retQ Where the return value will be put
      @param f A callable
      """
      super(SafeExecution.Thread, self).__init__()
      self.retQ   = retQ
      self.f      = f
      self.args   = args
      self.kwargs = kwargs

    def run(self):
      """
      Code
      """
      try:
        ret = self.f(*self.args, **self.kwargs)
        self.retQ.put(ret)
      except SystemExit:
        raise
      except Exception, e:
        self.retQ.put((1, "", str(e)))
        traceback.print_exc(file = sys.stderr)

  def __init__(self, f, timeout = None, ontimeout = (1, "", "Timeout reached")):
    """
    Constructor
    @param f         A callable object (function, class...)
    @param timeout   Timeout in seconds
    @param ontimeout What to return when a timeout is reached
    """
    if timeout: self.timeout = timeout
    else:       self.timeout = Opter.instance()["timeout"]
    self.ontimeout = ontimeout
    self.f = f

  @staticmethod
  def fromCommand(command, env = os.environ, cwd = None, timeout = None):
    """
    Create a SafeExecution instance that will call a system command
    @param env     The environment that will be used for the call [optional]
    @param cwd     The working directory [optional]
    @param timeout The timeout
    """
    def cmdFun():
      """
      Function that will be wrapped by SafeExecution
      """
      proc = subprocess.Popen(command,
                              stdout = subprocess.PIPE, stderr = subprocess.PIPE,
                              env = env, cwd = cwd,
                              shell = True)
      (stdout, stderr) = proc.communicate()
      exit = proc.returncode
      return (exit, stdout, stderr)
    
    return SafeExecution(cmdFun, timeout)

  def __call__(self, *args, **kwargs):
    """
    The instances are callable
    """
    retQ = Queue()
    th = SafeExecution.Thread(retQ, self.f, *args, **kwargs)
    th.start()
    th.join(self.timeout)

    if th.isAlive():
      return self.ontimeout
    else:
      return retQ.get(block = False)

# Execution engine
class Engine:
  """
  Execution engine
  """

  extensions = []
  _instance  = None

  @staticmethod
  def extension(f):
    """
    Register a new extension. f has to be a callable that
    receives one argument: the root definition
    """
    Engine.extensions.append(f)

  def __init__(self):
    """
    Constructor
    """
    self.reportStack = []
    self.report = Report("Deployment", Opter.instance()["debug"])
    self.abort  = False

  @staticmethod
  def instance():
    """
    Singleton
    """
    if not Engine._instance:
      Engine._instance = Engine()
    return Engine._instance

  def pushOutputReport(self, newReport):
    """
    Output will be printed in a different report
    @param newReport The new output report
    """
    self.reportStack.append(self.report)
    self.report = newReport

  def popOutputReport(self):
    """
    Return to the previous report
    """
    self.report = self.reportStack.pop()

  def deploy(self, root, label = None, ignoreFailure = False):
    """
    Traverse the definition and calls the actions,
    populating the log
    @param root          A Definition instance used as a start point
    @param label         An optional label if __doc__ is not to be used
    @param ignoreFailure If an error happen, it will be ignored
    """

    if not label and root.__doc__: label = root.__doc__.strip()
    if label: self.report.openGroup(label)

    stat = SUCCESS

    for s in root:
      if isinstance(s, Definition):
        subStat = self.deploy(s)
      elif isinstance(s, Requirement):
        self.report.debug(str(s))

        (exit, stdout, stderr) = SafeExecution(s.do)()

        if exit == 0:
          subStat = SUCCESS
        elif ignoreFailure:
          subStat = SUCCESS
          self.report.warning("An error ocurred, but it will be ignored")
        else:
          subStat = FAILED

        self.report.entry(str(s), exit, stdout, stderr, s.compile())
      else:
        raise ValueError("Unexpected instance of %s in the queue" % type(s))

      if subStat != SUCCESS:
        stat = FAILED
        break

    if label: self.report.closeGroup()
    return stat

  def do(self, root):
    """
    Starts the engine
    @param root A Definition instance used as start point
    """
    opter = Opter.instance()
    # Create log output
    if not os.path.exists(opter["log-dir"]):
      os.mkdir(opter["log-dir"])
      self.report.debug("Created %s" % opter["log-dir"])

    # Deploy
    if self.deploy(root) == FAILED:
      self.report.error("Execution aborted because a failure has occurred")
      self.abort = True

    # Extensions (i.e. test wrapper)
    for e in Engine.extensions:
      self.report.debug("Calling extension %s" % e)
      try:
        self.abort = e(root, abort = self.abort)
      except Exception, exc:
        self.report.error("Extension %s failed with %s" % (e, str(exc)))
        if opter["debug"]: traceback.print_exc(file = sys.stderr)

    # Copy style
    stylePath = os.path.join(os.path.dirname(__file__), "style")
    for f in os.listdir(stylePath):
      source = os.path.join(stylePath, f)
      dest   = os.path.join(opter["log-dir"], f)
      if not os.path.exists(dest) and not f.startswith("."):
        self.report.debug("Copying %s to %s" % (source, dest))
        if os.path.isdir(source): shutil.copytree(source, dest, False)
        else:                     shutil.copy2(source, dest)

    # Recover logs
    logList = opter["log-copy"][:]
    if "logs" in dir(root):
      for i in root.logs():
        if i not in logList: logList.append(i)

    self.report.openGroup("Recovered logs", metrics = False)
    destDir = os.path.join(opter["log-dir"], "recovered")
    if not os.path.isdir(destDir): os.mkdir(destDir)
    for source in logList:
      # Copy
      self.report.debug("Recovering log %s" % source)
      if source.endswith("/"): copyTo = os.path.join(destDir, os.path.basename(source[:-1]))
      else:                    copyTo = os.path.join(destDir, os.path.basename(source))
      try:
        if os.path.isdir(source): mycopytree(source, copyTo)
        else:                     shutil.copy(source, copyTo)
      except Exception, e:
        self.report.warning("Could not copy %s (%s)" % (source, str(e)))

    # Link in the report
    baseLength = len(opter["log-dir"])
    if not opter["log-dir"].endswith("/"): baseLength += 1
    stack      = [destDir]
    while len(stack):
      path = stack.pop()
      # Recursively append
      if os.path.isdir(path):
        for entry in os.listdir(path): stack.append(os.path.join(path, entry))
      # Link
      else:
        self.report.debug("Linking %s" % path[baseLength:])
        self.report.link(path[baseLength:])

    self.report.closeGroup()

    # Deployment log
    try:
      deployLogPath = os.path.join(opter["log-dir"], opter["log-deploy"])
      self.report.write(open(deployLogPath, "w"))
      indexCustom = os.path.join(opter["log-dir"], "index-custom-Deployment.html")
      self.report.indexCustom(opter["log-deploy"], indexCustom)
    except Exception, e:
      print >>sys.stderr, "Could not write the deployment log: %s" % str(e)
      return FAILED

    self.report.debug("Done")
    if self.abort: return FAILED
    else:          return SUCCESS

# Entry point for manifests
def run(type):
  """
  This method can be used as entry point
  """
  # Get options for command line
  opter = Opter.instance()
  opter.expand(CORE_OPTIONS)
  try:
    opter.parse()
  except getopt.GetoptError, e:
    print e
    sys.exit(1)

  if opter["help"]:
    opter.help()
    sys.exit(0)

  try:
    # Surround the instantiation to isolate errors coming from "outside"
    try:                      node = type()
    except KeyboardInterrupt: raise
    except Exception, e:
      print >>sys.stderr, "Error loading the definition %s" % str(type)
      print >>sys.stderr, str(e)
      if opter["debug"]:
        traceback.print_exc(file = sys.stderr)
      sys.exit(2)

    # Only show what will be done
    if opter["just-tell"]: print node   
    # Compile?
    elif opter["compile"]: print node.compile()
    # Just do it
    else:                  sys.exit(Engine.instance().do(node))

  except KeyboardInterrupt:
    print "Execution interrupted by the user!"
    sys.exit(0)
  except SystemExit:
      raise
  except Exception, e:
    # If we are here, an unknown error from the core itself it popping
    # We want the full output
    traceback.print_exc(file = sys.stderr)
    sys.exit(1)

# Called directly
if __name__ == "__main__":
  print >>sys.stderr, "Can not be called directly (yet)"
  sys.exit(1)
