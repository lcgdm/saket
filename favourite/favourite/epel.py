##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Base definitions for EPEL nodes
#
##############################################################################
import commands
import os
import os.path
import platform
import socket
from core       import Definition, Requirement, Property, Opter
from installer  import Package, Update, YumRepo, Clean
from filesystem import Directory, File
from services   import Exec

class Base(Definition):
  """
  Base for deployments
  """

  def __init__(self, protectEPEL = True):
    """
    Performs the setup
    """
    super(Base, self).__init__()

    # First of all, root access for debugging :)
    Directory("/root/.ssh")
    File("/root/.ssh/saket_rsa.pub",
         source = "http://grid-deployment.web.cern.ch/grid-deployment/dms/saket_rsa.pub")
    Exec("cat /root/.ssh/saket_rsa.pub >> /root/.ssh/authorized_keys")

    # Assure we have a minimal working path
    os.environ["PATH"] += ":/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin"

    # Standard repositories
    self.distMajor = int(platform.dist()[1][0])

    base = "os"

    if self.distMajor == 6 and platform.architecture()[0] == '64bit':
       # Remove all repos (workaround for ETICS pushing repos)
      Exec("rm -f /etc/yum.repos.d/*.repo")

      YumRepo("sl6",
              #baseurl  = """http://ftp.scientificlinux.org/linux/scientific/$releasever/$basearch/os/
              #              http://mirror.switch.ch/ftp/mirror/scientificlinux/$releasever/$basearch/os/""",
	      baseurl  = """http://ftp.scientificlinux.org/linux/scientific/6.6/$basearch/os/
                            http://mirror.switch.ch/ftp/mirror/scientificlinux/6.6/$basearch/os/""",
              descr    = "Scientific Linux 6",
              enabled  = 1,
              gpgcheck = 1,
              gpgkey   = """file:///etc/pki/rpm-gpg/RPM-GPG-KEY-sl
                            file:///etc/pki/rpm-gpg/RPM-GPG-KEY-sl6
                            file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern""")
      YumRepo("sl6-security",
              #baseurl  = """http://ftp.scientificlinux.org/linux/scientific/$releasever/$basearch/updates/security/
              #              http://mirror.switch.ch/ftp/mirror/scientificlinux/$releasever/$basearch/updates/security/""",
	      baseurl  = """http://ftp.scientificlinux.org/linux/scientific/6.6/$basearch/updates/security/
                            http://mirror.switch.ch/ftp/mirror/scientificlinux/6.6/$basearch/updates/security/""",
              descr    = "Scientifix Linux 6 - Security updates",
              enabled  = 1,
              gpgcheck = 1,
              gpgkey   = """file:///etc/pki/rpm-gpg/RPM-GPG-KEY-sl
                            file:///etc/pki/rpm-gpg/RPM-GPG-KEY-sl6
                            file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern""")
      YumRepo("sl6-updates",
              baseurl  = "http://linuxsoft.cern.ch/cern/slc6X/$basearch/yum/updates/",
              descr    = "Scientifix Linux 6 - updates",
              enabled  = 1,
              gpgcheck = 1,
	      priority = 5,
              gpgkey   = "file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern")
    else:
      YumRepo("wlcg",
              baseurl  = "http://linuxsoft.cern.ch/wlcg/centos7/$basearch" % self.distMajor,
              descr    = "WLCG Repository",
              gpgcheck = 0,
              enabled  = 1,
              protect  = 0)

    # EPEL
    YumRepo("epel",
            baseurl  = "http://linuxsoft.cern.ch/epel/%d/$basearch" % self.distMajor,
	    #baseurl  = "http://mirror.switch.ch/ftp/mirror/epel/%d/$basearch" % self.distMajor,
            descr    = "Extra Packages for Enterprise Linux add-ons",
            gpgkey   = """file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL
                          file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6""",
            gpgcheck = 0,
            enabled  = 1,
            protect  = protectEPEL)
    YumRepo("epel-testing",
            baseurl = "http://linuxsoft.cern.ch/epel/testing/%d/$basearch" % self.distMajor,
	    #baseurl  = "http://mirror.switch.ch/ftp/mirror/epel/testing/%d/$basearch" % self.distMajor,
            descr   = "Extra Packages for Enterprise Linux add-ons (Testing)",
            gpgkey  = """file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL
                         file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6""",
            gpgcheck = 0,
            enabled  = 1,
            protect  = protectEPEL)
    YumRepo("EGI-trustanchors",
            descr    = "EGI-trustanchors",
            baseurl  = "http://repository.egi.eu/sw/production/cas/1/current/",
            gpgkey   = "http://repository.egi.eu/sw/production/cas/1/GPG-KEY-EUGridPMA-RPM-3",
            gpgcheck = 0,
            enabled  = 1)

    # Default packages
    if self.distMajor == 6:
      Package("yum-plugin-protectbase")

    # Disable SELinux
    Exec("/usr/sbin/setenforce 0", tolerant = True)

    # Where to put the site-info.def
    Directory("/etc/yaim")

    # Vomses
    Directory("/etc/vomses"),
    File("/etc/vomses/dteam-voms2.hellasgrid.gr",
          content = '"dteam" "voms2.hellasgrid.gr" "15004" "/C=GR/O=HellasGrid/OU=hellasgrid.gr/CN=voms2.hellasgrid.gr" "dteam" "24"')

    Directory("/etc/grid-security/vomsdir/dteam")
    File("/etc/grid-security/vomsdir/dteam/voms2.hellasgrid.gr.lsc", content = "/C=GR/O=HellasGrid/OU=hellasgrid.gr/CN=voms2.hellasgrid.gr\n/C=GR/O=HellasGrid/OU=Certification Authorities/CN=HellasGrid CA 2006")
