<?xml version="1.0"?>
<!--
##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Álvarez Ayllón <alejandro.alvarez.ayllon@cern.ch>, CERN
#
##############################################################################
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sak="https://svnweb.cern.ch/trac/saket/"
  xmlns="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml" indent="yes"/> 

  <!-- Document level -->
  <xsl:template match="/sak:batch">
    <xsl:variable name="title">
      <xsl:choose>
        <xsl:when test="not(@name)">Batch report</xsl:when>
        <xsl:otherwise><xsl:value-of select="@name"/></xsl:otherwise>
      </xsl:choose> [<xsl:value-of select="@host"/>]
    </xsl:variable>
    <html>
    <head>
      <title><xsl:value-of select="$title"/></title>
      <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
      <link rel="stylesheet" type="text/css" href="sak2html.css"/>

      <script type="text/javascript">
        function toggleView(id)
        {
	        var obj = document.getElementById(id);
          if(obj.style.display != 'none')
            obj.style.display = 'none';
          else
            obj.style.display = 'block';
        }
      </script>
    </head>
    <body>
      <h1><xsl:value-of select="$title"/></h1>
      <xsl:apply-templates/>
    </body>
    </html>
  </xsl:template>

  <!-- Template to use <br/> when there is a new line -->
  <xsl:template name="break">
    <xsl:param name="text"/>
    <xsl:choose>
      <xsl:when test="contains($text, '&#xa;')">
        <xsl:variable name="before" select="substring-before($text, '&#xa;')"/>
        <xsl:if test="$before">
          <xsl:value-of select="$before"/>
          <br/>
        </xsl:if>
        <xsl:call-template name="break">
          <xsl:with-param name="text" select="substring-after($text, '&#xa;')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Log nodes -->
  <xsl:template match="sak:info">
    <div class="info-line">
      <p>
        <xsl:call-template name="break">
          <xsl:with-param name="text" select="."/>
        </xsl:call-template>
      </p>
    </div>
  </xsl:template>

  <xsl:template match="sak:warning">
    <div class="warning-line">
      <p>
        <xsl:call-template name="break">
          <xsl:with-param name="text" select="."/>
        </xsl:call-template>
      </p>
    </div>
  </xsl:template>

  <xsl:template match="sak:error">
    <div class="error-line">
      <p>
        <xsl:call-template name="break">
          <xsl:with-param name="text" select="."/>
        </xsl:call-template>
      </p>
    </div>
  </xsl:template>

  <xsl:template match="sak:debug">
    <div class="debug-line">
      <p>
        <xsl:call-template name="break">
          <xsl:with-param name="text" select="."/>
        </xsl:call-template>
      </p>
    </div>
  </xsl:template>

  <!-- Standard output -->
  <xsl:template match="sak:stdout">
    <pre class="stdout">
      <xsl:value-of select="."/>
    </pre>
  </xsl:template>

  <!-- Standard error -->
  <xsl:template match="sak:stderr">
    <pre class="stderr">
      <xsl:value-of select="."/>
    </pre>
  </xsl:template>

  <!-- External reference -->
  <xsl:template match="sak:a">
    <p class="link">
      <a>
        <xsl:attribute name="href">
          <xsl:value-of select="@href"/>
        </xsl:attribute>
        <xsl:value-of select="."/>
      </a>
    </p>
  </xsl:template>

  <!-- Execution blocks -->
  <xsl:template match="sak:execute">
    <!-- Fancy exit code -->
    <xsl:variable name="exitStatus">
      <xsl:choose>
        <xsl:when test="@exit = 0"><span class="success">SUCCESS</span></xsl:when>
        <xsl:otherwise><span class="failed">FAILED</span></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- Output -->
    <div class="execution-line">
      <label>
        <xsl:attribute name="onclick">javascript:toggleView('details-<xsl:value-of select="@id"/>')</xsl:attribute>
        <xsl:value-of select="@name"/> [ <xsl:copy-of select="$exitStatus"/> ]
      </label>
      <div class="execution-details">
        <xsl:attribute name="id">details-<xsl:value-of select="@id"/></xsl:attribute>
        <xsl:attribute name="style">
          <xsl:choose>
            <xsl:when test="@exit = '0'">display:none</xsl:when>
            <xsl:otherwise>display:block</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <p class="command-line">
          <xsl:call-template name="break">
            <xsl:with-param name="text" select="@command"/>
          </xsl:call-template>
        </p>
        <p class="exit-code">
          Exit code: <xsl:value-of select="@exit"/>
        </p>
        <xsl:apply-templates/>
      </div>
    </div>
  </xsl:template>


  <!-- Stats -->
  <xsl:template match="sak:stats">

    <xsl:variable name="duration">
      <xsl:value-of select="floor(@duration div 3600)"/>h
      <xsl:value-of select="floor(@duration div 60) mod 60"/>m
      <xsl:value-of select="@duration mod 60"/>s
    </xsl:variable>

    <p class="suite-stats">
      Success rate: <xsl:value-of select="round((@passed div @total) * 10000) div 100"/>%
      (<xsl:value-of select="@passed"/>/<xsl:value-of select="@total"/>)
      <br/>
      Start time: <xsl:value-of select="@start"/>
      <br/>
      End time: <xsl:value-of select="@end"/>
      <br/>
      Duration: <xsl:value-of select="$duration"/>
    </p>  
  </xsl:template>

  <!-- Test suite -->
  <xsl:template match="sak:group">
    <!-- Fancy exit code -->
    <xsl:variable name="exitStatus">
      <xsl:choose>
        <xsl:when test="not(sak:stats)"></xsl:when>
        <xsl:when test="sak:stats/@total = sak:stats/@passed"> [ <span class="success">SUCCESS</span> ]</xsl:when>
        <xsl:otherwise> [ <span class="failed">FAILURE</span> ]</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- Output -->
    <div class="suite-line">
        <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
      <xsl:element name="{concat('h', count(ancestor::sak:group)+2)}">
        <xsl:attribute name="onclick">javascript:toggleView('tests-<xsl:value-of select="@name"/>')</xsl:attribute>
        <xsl:value-of select="@name"/> <xsl:copy-of select="$exitStatus"/>
      </xsl:element>
      <div class="suite-tests" style="display:block">
        <xsl:attribute name="id">tests-<xsl:value-of select="@name"/></xsl:attribute>
        <xsl:if test="@collapse">
          <xsl:attribute name="style">display:none</xsl:attribute>
        </xsl:if>
        <xsl:apply-templates select="sak:stats"/>
        <xsl:apply-templates select="./*[not(local-name()='stats')]"/>
      </div>
    </div>
  </xsl:template>

</xsl:stylesheet>

