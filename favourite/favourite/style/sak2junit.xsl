<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sak="https://svnweb.cern.ch/trac/saket/">

  <xsl:output method="xml" indent="yes"/> 

  <!-- Document level -->
  <xsl:template match="/sak:batch">
    <testsuite errors="0"
        failures="0"
        hostname=""
        name=""
	    tests="0"
	    time="0"
	    timestamp="">
	    <xsl:attribute name="errors"><xsl:value-of select="count(sak:execute[@exit != 0])"/></xsl:attribute>
	    <xsl:attribute name="failures"><xsl:value-of select="count(sak:group/sak:execute[@exit != 0])"/></xsl:attribute>
	    <xsl:attribute name="tests"><xsl:value-of select="count(sak:group/sak:execute)"/></xsl:attribute>
	    <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
	    <xsl:attribute name="hostname"><xsl:value-of select="@host"/></xsl:attribute>
	    <xsl:attribute name="time"><xsl:value-of select="//sak:stats[1]/@duration"/></xsl:attribute>
	    <xsl:attribute name="timestamp"><xsl:value-of select="//sak:stats[1]/@start"/></xsl:attribute>
	    
	    <properties/>
	    
        <!-- All the executed tests -->
        <xsl:for-each select="sak:group">
            <xsl:variable name="group" select="@name"/>
            <xsl:for-each select="sak:execute">
                <testcase classname="" name="">
                    <xsl:attribute name="classname"><xsl:value-of select="$group"/></xsl:attribute>
                    <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
                    <xsl:if test="@exit != 0">
                        <failure type="TestFailed">
                            <xsl:value-of select="sak:stdout"/>
                            <xsl:value-of select="sak:stderr"/>
                        </failure>
                    </xsl:if>
                    <!--<system-out><xsl:value-of select="sak:stdout"/></system-out>
                    <system-err><xsl:value-of select="sak:stderr"/></system-err>-->
                </testcase>
            </xsl:for-each>
        </xsl:for-each>
    </testsuite>
  </xsl:template>

</xsl:stylesheet>

