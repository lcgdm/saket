<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sak="https://svnweb.cern.ch/trac/saket/">

  <xsl:output method="xml" indent="yes"/> 

  <!-- Document level -->
  <xsl:template match="/sak:batch">
    <Site BuildName="(empty)"
	    BuildStamp="(empty)"
	    Name="to-be-set"
	    Generator="sak2cunit.xsl"
	    Hostname="to-be-set">
	    <xsl:attribute name="Name"><xsl:value-of select="@name"/></xsl:attribute>
	    <xsl:attribute name="Hostname"><xsl:value-of select="@host"/></xsl:attribute>
        <Testing>
    	    <StartDateTime><xsl:value-of select="//sak:stats[1]/@start"/></StartDateTime>
	        <StartTestTime><xsl:value-of select="//sak:stats[1]/@duration"/></StartTestTime>
	        <!-- All the executed tests -->
	        <TestList>
	            <xsl:for-each select="sak:group/sak:execute">
	                <Test><xsl:value-of select="@command"/></Test>
	            </xsl:for-each>
        	</TestList>
        	
        	<!-- Results -->
        	<xsl:for-each select="sak:group/sak:execute">
        	    <Test>
        	        <xsl:attribute name="Status">
        	            <xsl:choose>
        	                <xsl:when test="@exit = 0">passed</xsl:when>
        	                <xsl:otherwise>failed</xsl:otherwise>
        	            </xsl:choose>
        	        </xsl:attribute>
        	        <Name><xsl:value-of select="@name"/></Name>
        	        <FullCommandLine><xsl:value-of select="@command"/></FullCommandLine>
        	        <Results>
        	            <NamedMeasurement type="numeric/integer" name="Exit code">
                            <Value><xsl:value-of select="@exit"/></Value>
                        </NamedMeasurement>
        	            <NamedMeasurement type="text/string" name="stdout">
        	                <Value><xsl:value-of select="sak:stdout"/></Value>
        	            </NamedMeasurement>
        	            <NamedMeasurement type="text/string" name="stderr">
        	                <Value><xsl:value-of select="sak:stderr"/></Value>
        	            </NamedMeasurement>
        	        </Results>
        	    </Test>
        	</xsl:for-each>
        </Testing>
    </Site>
  </xsl:template>

</xsl:stylesheet>

