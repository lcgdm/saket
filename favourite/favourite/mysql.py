##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Requirements and definitions involving MySQL
#
##############################################################################

import commands
from core      import Definition, Requirement
from installer import Package
from services  import Service

class MySQL(Definition):
  """
  Base for MySQL server
  """

  def __init__(self):
    """
    Constructor
    """
    super(MySQL, self).__init__()
    Package(["mysql", "mysql-server"])
    Service("mysqld")

class MySQLQuery(Requirement):
  """
  MySQL query
  """

  def __init__(self, source, tolerant = False, db = None, user = "root", passwd = None):
    """
    Constructor
    @param source   The source sql
    @param tolerant If True, even when the query fails, the requirement will pass
    """
    super(MySQLQuery, self).__init__()
    self.source   = source
    self.tolerant = tolerant
    if db is None:
      self.db = ""
    else:
      self.db       = db
    self.user     = user
    self.passwd   = passwd
    
  def __str__(self):
    """
    String representation
    """
    return "[MySQLQuery] %s" % self.source

  def compile(self):
    """
    Bash equivalent
    """
    if self.passwd:
      return "mysql -u%s -p%s %s < %s" % (self.user, self.passwd, self.db, self.source)
    else:
      return "mysql -u%s %s < %s" % (self.user, self.db, self.source)

  def do(self):
    """
    Perform the action
    """
    (exit, stdout) = commands.getstatusoutput(self.compile())
    if exit != 0 and self.tolerant: return (0, stdout, "The command failed, but it is fault-tolerant")
    else:                           return (exit, stdout, "")


class MySQLDatabase(Requirement):
  """
  Create a database. 
  """

  def __init__(self, db):
    """
    Constructor
    """
    super(MySQLDatabase, self).__init__()
    self.db     = db

  def __str__(self):
    """
    String representation
    """
    return "[MySQLDatabase] Create %s" % self.db

  def compile(self):
    """
    Bash equivalent
    """
    return "mysql -u root -e \"CREATE DATABASE IF NOT EXISTS %s\"" % self.db

  def do(self):
    """
    Perform the action
    """
    (exit, stdout) = commands.getstatusoutput(self.compile())
    return (exit, stdout, "")



class MySQLGrant(Requirement):
  """
  Grant access to a user
  """

  def __init__(self, db, user, passwd):
    """
    Constructor
    """
    super(MySQLGrant, self).__init__()
    self.db     = db
    self.user   = user
    self.passwd = passwd

  def __str__(self):
    """
    String representation
    """
    return "[MySQLGrant] Grant all on %s to %s" % (self.db, self.user)

  def compile(self):
    """
    Bash equivalent
    """
    return "mysql -u root -e \"GRANT ALL ON %s.* TO '%s'@'localhost' IDENTIFIED BY '%s';\"" % (self.db, self.user, self.passwd)

  def do(self):
    """
    Perform the action
    """
    (exit, stdout) = commands.getstatusoutput(self.compile())
    return (exit, stdout, "")

