##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Base definitions for nodes
#
##############################################################################
import commands
import os
import os.path
import platform
import socket
from core       import Definition, Requirement, Property, Opter
from installer  import Package, Update, YumRepo, Clean
from filesystem import Directory, File
from services   import Exec

# This module requires few extra options
PREDEFINED_OPTIONS = {
  "emi-version"   : ("EMI version", False, None),
}

Opter.instance().expand(PREDEFINED_OPTIONS)

class Base(Definition):
  """
  Base for deployments
  """

  def __init__(self):
    """
    Performs the setup
    """
    super(Base, self).__init__()

    # First of all, root access for debugging :)
    Directory("/root/.ssh")
    File("/root/.ssh/saket_rsa.pub",
         source = "http://grid-deployment.web.cern.ch/grid-deployment/dms/saket_rsa.pub")
    Exec("cat /root/.ssh/saket_rsa.pub >> /root/.ssh/authorized_keys")

    # Assure we have a minimal working path
    os.environ["PATH"] += ":/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin"

    # Standard repositories
    self.distMajor = int(platform.dist()[1][0])
    Exec("rm -f /etc/yum-puppet.repos.d/*.repo")

    base = "os"

    if self.distMajor == 6 and platform.architecture()[0] == '64bit':
      # Remove all repos (workaround for ETICS pushing repos)

      YumRepo("slc6",
              baseurl  = "http://linuxsoft.cern.ch/cern/slc6X/$basearch/yum/os/",
              descr    = "Scientific Linux CERN 6",
              enabled  = 1,
              gpgcheck = 1,
              gpgkey   = "file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern")
      YumRepo("slc6-extras",
              baseurl  = "http://linuxsoft.cern.ch/cern/slc6X/$basearch/yum/extras/",
              descr    = "Scientifix Linux 6 CERN - Extra",
              enabled  = 1,
              gpgcheck = 1,
              gpgkey   = "file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern")
      YumRepo("sl6-updates",
              baseurl  = "http://linuxsoft.cern.ch/cern/slc6X/$basearch/yum/updates/",
              descr    = "Scientifix Linux 6 - updates",
              enabled  = 1,
              gpgcheck = 1,
              priority = 5,
              gpgkey   = "file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern")
      # WLCG repositories
      YumRepo("wlcg",
            baseurl  = "http://linuxsoft.cern.ch/wlcg/sl%d/$basearch" % self.distMajor,
            descr    = "WLCG Repository",
            gpgcheck = 0,
            enabled  = 1,
            protect  = 0)

    else:
       YumRepo("CentOS-7-Base",
            baseurl = "http://linuxsoft.cern.ch//cern/centos/7/os/x86_64",
            enabled = 1,
            gpgcheck = 1,
            gpgkey = "file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7",
            priority =10)
       YumRepo("CentOS-7-Updates",
            baseurl = "http://linuxsoft.cern.ch//cern/centos/7/updates/x86_64",
            enabled = 1,
            gpgcheck = 1,
            gpgkey = "file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7",
            priority =10)
       YumRepo("wlcg",
            baseurl  = "http://linuxsoft.cern.ch/wlcg/centos7/$basearch",
            descr    = "WLCG Repository",
            gpgcheck = 0,
            enabled  = 1,
            protect  = 0)	

    #add epel for both sl6 and c7
    YumRepo("epel",
            baseurl  = "http://linuxsoft.cern.ch/epel/%d/$basearch" % self.distMajor,
            descr    = "Extra Packages for Enterprise Linux add-ons",
            gpgkey   = """file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL
                          file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6""",
            gpgcheck = 0,
            enabled  = 1,
            protect  = 0)
    YumRepo("epel-testing",
            baseurl = "http://linuxsoft.cern.ch/epel/testing/%d/$basearch" % self.distMajor,
            descr   = "Extra Packages for Enterprise Linux add-ons (Testing)",
            gpgkey  = """file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL
                         file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6""",
            gpgcheck = 0,
            enabled  = 1,
            protect  = 0)


    # Default packages
    if self.distMajor == 6:
      Package("yum-plugin-protectbase")

    # Disable SELinux
    Exec("/usr/sbin/setenforce 0", tolerant = True)

    # Where to put the site-info.def
    Directory("/etc/yaim")

    # Vomses
    Directory("/etc/vomses"),

    File("/etc/vomses/dteam-voms2.hellasgrid.gr",
          content = '"dteam" "voms2.hellasgrid.gr" "15004" "/C=GR/O=HellasGrid/OU=hellasgrid.gr/CN=voms2.hellasgrid.gr" "dteam" "24"')

    Directory("/etc/grid-security/vomsdir/dteam")
    File("/etc/grid-security/vomsdir/dteam/voms2.hellasgrid.gr.lsc", content = "/C=GR/O=HellasGrid/OU=hellasgrid.gr/CN=voms2.hellasgrid.gr\n/C=GR/O=HellasGrid/OU=Certification Authorities/CN=HellasGrid CA 2016")



class EMI(Base):
  """
  Base for EMI deployments
  """

  def __init__(self, version = None, enable_testing = True, enable_updates = True):
    """
    Constructor
    """

    # And now the parent
    super(EMI, self).__init__()

    # Guess EMI version
    emiVer = Opter.instance()["emi-version"]

    # Trusanchors
    YumRepo("EGI-trustanchors",
            descr    = "EGI-trustanchors",
            baseurl  = "http://repository.egi.eu/sw/production/cas/1/current/",
            gpgkey   = "http://repository.egi.eu/sw/production/cas/1/GPG-KEY-EUGridPMA-RPM-3",
            gpgcheck = 0,
            enabled  = 1)

    # EMI repositories
    if emiVer:
            File("/etc/pki/rpm-gpg/RPM-GPG-KEY-emi",
                 source = "http://emisoft.web.cern.ch/emisoft/dist/EMI/1/RPM-GPG-KEY-emi")
            File("/etc/pki/rpm-gpg/RPM-GPG-KEY-emi-2",
                 source = "http://emisoft.web.cern.ch/emisoft/dist/EMI/2/RPM-GPG-KEY-emi")

            YumRepo("emi-base",
                    descr    = "EMI Base Repository",
                    baseurl  = "http://emisoft.web.cern.ch/emisoft/dist/EMI/%s/sl%s/$basearch/base" % (emiVer, self.distMajor),
                    gpgcheck = 0,
                    enabled = 1,
                    protect = 1)

            if enable_updates:
              YumRepo("emi-updates",
                      descr    = "EMI Updates Repository",
                      baseurl  = "http://emisoft.web.cern.ch/emisoft/dist/EMI/%s/sl%s/$basearch/updates" % (emiVer, self.distMajor),
                      gpgcheck = 0,
                      enabled  = 1,
                      protect  = 1)
            YumRepo("emi-third-party",
                    descr    = "EMI Third-Party Repository",
                    baseurl  = "http://emisoft.web.cern.ch/emisoft/dist/EMI/%s/sl%s/$basearch/third-party" % (emiVer, self.distMajor),
                    gpgcheck = 0,
                    enabled = 0,
                    protect = 1)

            if enable_testing:
              YumRepo("emi-testing",
                      baseurl  = "http://emisoft.web.cern.ch/emisoft/dist/EMI/testing/%s/sl%s/$basearch/base" % (emiVer, self.distMajor),
                      priority = 2,
                      enabled  = 1,
                      protect  = 1,
                      gpgcheck = 0)
              YumRepo("emi-testing-third-party",
                      baseurl  = "http://emisoft.web.cern.ch/emisoft/dist/EMI/testing/%s/sl%s/$basearch/third-party" % (emiVer, self.distMajor),
                      priority = 2,
                      enabled  = 1,
                      protect  = 1,
                      gpgcheck = 0)

    # Clean yum cache
    Clean()

    # Update
    Update(True)
    Package("ca-policy-lcg")


class EPEL_TESTING(Base):
  """
  Base for deployments on epel testing only
  """

  def __init__(self, version = None, enable_testing = True):
    """
    Constructor
    """
    super(EPEL_TESTING, self).__init__()

    # EPEL
    YumRepo("epel",
            baseurl  = "http://linuxsoft.cern.ch/epel/%d/$basearch" % self.distMajor,
            descr    = "Extra Packages for Enterprise Linux add-ons",
            gpgkey   = """file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL
                          file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6""",
            gpgcheck = 0,
            enabled  = 1,
            protect  = 0)
    YumRepo("epel-testing",
            baseurl = "http://linuxsoft.cern.ch/epel/testing/%d/$basearch" % self.distMajor,
            descr   = "Extra Packages for Enterprise Linux add-ons (Testing)",
            gpgkey  = """file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL
                         file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6""",
            gpgcheck = 0,
            enabled  = 1,
            protect  = 0)
    # EGI certs
    YumRepo("EGI-trustanchors",
            descr    = "EGI-trustanchors",
            baseurl  = "http://repository.egi.eu/sw/production/cas/1/current/",
            gpgkey   = "http://repository.egi.eu/sw/production/cas/1/GPG-KEY-EUGridPMA-RPM-3",
            gpgcheck = 0,
            enabled  = 1)

    # Clean yum cache
    Clean()

    # Update
    Update(True)
    Package("ca-policy-lcg")
