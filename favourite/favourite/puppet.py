##############################################################################
#  Copyright 2013 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Puppet bootsrapping
#
##############################################################################
import os
import os.path
import socket
import predefined
from core       import Definition, Requirement, Opter
from installer  import Package, YumRepo, Clean
from filesystem import File, Directory
from services   import Exec

class Base(predefined.Base):
  """
  Base for puppet-based deployments
  """

  def __init__(self):
    """
    Performs the setup
    """
    super(Base, self).__init__()

    # We need git and puppet and the DPM puppet rpm
    Package(["git", "puppet", "dmlite-puppet-dpm"])

    # Point HOME to /root/
    os.environ['HOME'] = "/root"
    #disable agent
    Exec("puppet agent --disable")
    # download manifest
    #Exec("puppet module install lcgdm-dmlite")
    Exec("puppet module install puppetlabs-firewall")
    Exec("puppet module install lcgdm-gridftp")
    #Exec("puppet module install lcgdm-xrootd")
    Exec("puppet module install lcgdm-voms")
    Exec("puppet module install puppetlabs-mysql --version 3.4.0")
    Exec("puppet module install lcgdm-lcgdm")
    Exec("puppet module install saz-memcached")
    #bdii
    Exec("puppet module install CERNOps-bdii")
    
    #dmlite
    Exec("rm -rf /etc/puppetlabs/code/environments/production/modules/dmlite")
    Exec("git clone -b develop https://github.com/cern-it-sdc-id/puppet-dmlite")
    Exec("mv puppet-dmlite /etc/puppetlabs/code/environments/production/modules/dmlite")
  
    #xrootd
    Exec("rm -rf /etc/puppetlabs/code/environments/production/modules/xrootd")
    Exec("git clone -b develop https://github.com/cern-it-sdc-id/puppet-xrootd")
    Exec("mv puppet-xrootd /etc/puppetlabs/code/environments/production/modules/xrootd")

    #dpm
    Exec("rm -rf /etc/puppetlabs/code/environments/production/modules/dpm")
    Exec("git clone -b develop https://github.com/cern-it-sdc-id/puppet-dpm")
    Exec("mv puppet-dpm /etc/puppetlabs/code/environments/production/modules/dpm")
    
    Exec("puppet module list")

class Puppet(Exec):
    """
    Run puppet command with the given manifest
    """

    def __init__(self, manifest, modulepath="/etc/puppetlabs/code/environments/production/modules"):
        cmd = "puppet apply  --pluginsync --verbose %s --modulepath %s" % (manifest, modulepath)
        super(Puppet, self).__init__(cmd, tolerant = False)
        self.manifest = manifest

    def __str__(self):
        """
        String representation
        """
        return "[Puppet] %s" % self.manifest

    def do(self):
        """
        Perform the action
        """
        (exit, stdout, stderr) = super(Puppet, self).do()
        if exit == 2:
            exit = 0
        return (exit, stdout, stderr)

