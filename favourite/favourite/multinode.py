##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Multinode implementation.
#
##############################################################################

import core
import os
import socket
import subprocess
import sys
from core import Requirement

class Requires(Requirement):
  """
  Blocks the execution until a node publishes itself
  """

  def __init__(self, node):
    """
    Constructor
    @param node The node type to wait for
    """
    super(Requires, self).__init__()
    self.node = node
    
  def __str__(self):
    """
    String representation
    """
    return "[Requires] Wait for %s" % self.node

  def compile(self):
    """
    Bash equivalent
    """
    return "%s=`%s/bin/etics-get -b --timeout 1200 %s`" % (self.node, os.environ["ETICS_HOME"], self.node)

  def do(self):
    """
    Performs the action
    """
    if "ETICS_INFOSYS_UUID" not in os.environ: return (1, "", "ETICS_INFOSYS_UUID must be in the environment!")

    proc = subprocess.Popen("%s/bin/etics-get -b --timeout 600 %s" % (os.environ["ETICS_HOME"], self.node),
                            stdin = None, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
    (stdout, stderr) = proc.communicate()
    if proc.returncode != 0:
      return (proc.returncode, stdout, stderr)

    self.host = stdout.strip()
    return (0, "%s provided by %s" % (self.node, self.host), "")

class Provides(Requirement):
  """
  Publishes the node and returns
  """

  def __init__(self, node):
    """
    Constructor
    @param node The node to publish
    """
    super(Provides, self).__init__()
    self.node = node

  def __str__(self):
    """
    String representation
    """
    return "[Provides] Publish %s as %s" % (self.node, socket.gethostname())

  def compile(self):
    """
    Bash equivalent
    """
    return "%s/bin/etics-set '%s' '%s'" % (os.environ["ETICS_HOME"], self.node, socket.gethostname())

  def do(self):
    """
    Performs the action
    """
    if "ETICS_INFOSYS_UUID" not in os.environ: return (1, "", "ETICS_INFOSYS_UUID must be in the environment!")

    proc = subprocess.Popen(self.compile(),
                            stdin = None, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
    (stdout, stderr) = proc.communicate()
    if proc.returncode != 0:
      return (proc.returncode, stdout, stderr)

    return (0, "%s published" % self.node, "")

class Multinode:
  """
  Implements the extension
  """

  def __call__(self, node, abort = False):
    """
    Called by Core
    """
    if abort and "ETICS_INFOSYS_UUID" in os.environ:
      proc = subprocess.Popen("etics-set --type System abort true",
                              stdin = None, stdout = subprocess.PIPE, stderr = subprocess.PIPE,
                              shell = True)
      (stdout, stderr) = proc.communicate()
      print >>sys.stderr, "ABORTING"


core.Engine.extension(Multinode())
