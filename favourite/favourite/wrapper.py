#! /usr/bin/env python
##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# This script is a wrapper to execute the certification tests being as generic
# as possible.
#
##############################################################################

import core
import getopt
import os
import shutil
import socket
import sys
import traceback
from report import Report

# self.opter
WRAPPER_OPTIONS = {
  # Tests
  "log-test"     : ("Test log",             False, "tests.xml"),
  "user-cert"    : ("User certificate",     False, None),
  "user-key"     : ("User key",             False, None),
  "user-pass"    : ("User password",        False, None),
  "user-proxy"   : ("User proxy location",  False, "/tmp/x509up_u%d" % os.getuid()),
  "no-test"      : ("Do not run the tests", True,  False),
  "voms"         : ("Virtual organization", False, None),
  "target"       : ("TARGET_HOST env will be set to this value", False, None),
  "set-env"      : ("Receives a NAME=VALUE, which will then be exported in the environment. Can be specified multiple times", False, [])
}

STANDALONE_OPTIONS = {
  "test-location": ("Where the tests are located", False, []),
  "test-prefix"  : ("Test prefix", False, [])
}

core.Opter.instance().expand(WRAPPER_OPTIONS)

# Test representation
class Test:
  def __init__(self, path):
    """
    Initializes a test
    @param path The path to the test
    """
    if not os.path.isdir(path):
      self.path = path
    else:
      self.path = os.path.join(path, "execute_test.sh")
      if not os.path.isfile(self.path):
        raise IOError("The test is a directory and there is no execute_test.sh inside")

    self.name   = os.path.basename(path)
    self.prefix = self.name.split("-", 1)[0]

    if not os.access(self.path, os.X_OK):
      raise IOError("The test file is not executable")

    try:
      test = open(self.path, "r")
      for line in test:
        if line.startswith("# meta:") or line.startswith("#meta:"):
          # It is a comment, so maybe some metadata is there
          content = line[1:].strip().split(":", 1)
          if len(content) > 1:
            content = content[1].strip()
            (key, val) = content.split("=", 1)
            self.__dict__[key] = val
      test.close()
    except:
      raise KeyError("Bad formatting of the metadata: %s" % path)

    # Specific needs?
    if self.manual and self.manual.lower() == "true":
      raise KeyError("The test can only be executed manually")

    if self.root and self.root.lower() == "true" and os.getuid() != 0:
      raise KeyError("The test need to be executed as root! Ignored")

    # Preconfigure
    if self.preconfig:
      self.preconfig = os.path.abspath(os.path.join(os.path.dirname(path), self.preconfig))


  def __getattr__(self, key):
    """
    Overrides the access to attributes
    @param key The attribute
    """
    if key in self.__dict__:
      return self.__dict__[key]
    else:
      return None

  def __lt__(self, b):
    """
    Operator <. Used to sort.
    @param b The element to compare with
    """
    return self.name < b.name

  def __str__(self):
    """
    String representation
    """
    return self.path

# A class to handle our special list grouped by requirements
class TestList:

  def __init__(self):
    """
    Initialization of the list
    """
    self._preconfig = []
    self._tests     = {}
    self._nTests    = 0
    self._preconfigLocked = False

  def add(self, path):
    """
    Adds a new tests from a path
    @param path The path to the test
    """
    test = Test(path)
    if test.ignore:
      del test
      return False

    # Add to the specific group
    if test.preconfig:
      pre = os.path.abspath(test.preconfig)
      if not self._preconfigLocked and not pre in self._preconfig:
        self._preconfig.append(pre)

    if not test.prefix in self._tests:
      self._tests[test.prefix] = []
    self._tests[test.prefix].append(test)

    self._nTests += 1

    return True

  def preconfig(self):
    """
    Retuns a list with the preconfiguration commands to run
    @return A list of commands
    """
    return self._preconfig

  def addPreconfig(self, config, lock = False):
    """
    Manually adds a preconfiguration file
    @param config The path to the configuration file
    @param lock   If True, no more preconfiguration files will be accepted
    """
    if not self._preconfigLocked: self._preconfig.append(config)
    if lock:                      self._preconfigLocked = True

  def size(self):
    """
    Returns the number of tests
    """
    return self._nTests

  def __getitem__(self, key):
    """
    Returns the list of files associated with the specified prefix
    @param key The prefix
    """
    if not key in self._tests:
      return []
    else:
      list = self._tests[key]
      list.sort()
      return list

# Test set definition
class TestSetDefinition:
  """
  Definition of a test set
  """

  def __init__(self, title, location, prefixes):
    """
    Constructor
    @param title    The test set title
    @param location An array with the test locations
    @param prefixes An array with the test prefixes
    """
    self.title    = title
    self.location = location
    self.prefixes = prefixes
  
# Main code
class Wrapper:
  """
  Implementation of the wrapper
  """

  def __init__(self):
    """
    Constructor
    """
    self.opter  = core.Opter.instance()
    self.report = Report("Testing", debugging = self.opter["debug"])

  def __call__(self, node, abort = False):
    """
    Instances are callable
    @param node  The node definition that has been deployed
    @param abort If the deployment failed, this will be true
    """
    status = core.SUCCESS
    # If something bad happened
    if abort:
      self.report.error("The deployment failed. No tests will be executed")
    # If no tests were asked for
    elif self.opter["no-test"]:
      self.report.warning("--no-test flag specified, no test will be executed")
    # If no tests are defined
    elif "test" not in dir(node):
      self.report.warning("No test method defined in %s" % type(node).__name__)
    # Finally, there are tests!
    else:
      core.Engine.instance().pushOutputReport(self.report)
      td = node.test()
      core.Engine.instance().popOutputReport()
      if td:
        self.report.setTitle(td.title)
        # Delegate to "do"
        status = self.do(td)
      else:
        self.report.error("No test definition returned!")    

    # Generate self.report.
    try:
      testLog = os.path.join(self.opter["log-dir"], self.opter["log-test"])
      self.report.write(open(testLog, "w"))
      indexCustom = os.path.join(self.opter["log-dir"], "index-custom-Test.html")
      self.report.indexCustom(self.opter["log-test"], indexCustom)
    except Exception, e:
      print >>sys.stderr, "Could not write the test log: %s" % str(e)

    return status

  def X509(self, enable):
    """
    Enable or disable the X509_USER_* environment
    @param enable A boolean specifying if enable (True) or disable (False)
    """
    if enable:
      if self.opter["user-cert"]:
        self.testEnv["X509_USER_CERT"]  = self.opter["user-cert"]
      if self.opter["user-key"]:
        self.testEnv["X509_USER_KEY"]   = self.opter["user-key"]
      if self.opter["user-proxy"]:
        self.testEnv["X509_USER_PROXY"] = self.opter["user-proxy"]
      if self.opter["user-pass"]:
        self.testEnv["PROXYPASSWD"]     = self.opter["user-pass"]
    else:
      if "X509_USER_CERT"  in self.testEnv: del self.testEnv["X509_USER_CERT"]
      if "X509_USER_KEY"   in self.testEnv: del self.testEnv["X509_USER_KEY"]
      if "X509_USER_PROXY" in self.testEnv: del self.testEnv["X509_USER_PROXY"]
      if "PROXYPASSWD"     in self.testEnv: del self.testEnv["PROXYPASSWD"]

  def do(self, td):
    """
    Main logic
    @param td A TestSetDefinition instance
    """
    testList = TestList()

    abort = False

    # Add the log location to the environment, so the test can create files
    # and keep it
    os.environ["TEST_LOG_LOCATION"] = os.path.dirname(self.opter["log-dir"])

    # VOMS location
    self.voms_init = "/usr/bin/voms-proxy-init"

    # Export target host
    if self.opter["target"]:
        os.environ['TARGET_HOST'] = self.opter["target"]

    # Export
    for envVar in self.opter["set-env"]:
        try:
            (name, value) = envVar.split('=', 1)
            os.environ[name] = value
        except:
            self.report.warning("Invalid environment parameter %s" % envVar)

    # VO
    if self.opter["voms"]:
      self.voms = self.opter["voms"].split(',')
    elif "VO" in os.environ:
      self.voms = os.environ["VO"].split(',')
    else:
      self.voms = ["dteam", "testers.eu-emi.eu"]

    # Set envinronment to first
    if "VO" not in os.environ:
        os.environ["VO"] = self.voms[0].split(':')[0]

    # Info
    self.report.info("VOMS are %s"            % ",".join(self.voms))
    self.report.info("Test location are %s"   % ",".join(td.location))
    self.report.info("Test suites are %s"     % ",".join(td.prefixes))
    self.report.info("Hostname is %s"         % socket.gethostname())

    # Iterate through the first level directories
    for path in td.location:
      self.report.debug("Searching under %s" % path)
      try:
        for name in os.listdir(path):
          firstLevel = os.path.join(path, name)
          self.report.debug("Searching under %s" % firstLevel)
          try:
            if os.path.isdir(firstLevel) and not name.startswith("."):
              ntests = 0
              # Iterate through the children and add them to the list
              # if they have the prefix
              for test in os.listdir(firstLevel):
                if test.split("-", 1)[0] in td.prefixes:
                  testPath = os.path.join(firstLevel, test)
                  try:
                    if testList.add(testPath): ntests += 1
                  except KeyError, err: self.report.warning("%s (%s)" % (str(err), test))
                  except IOError, err:  self.report.warning("%s (%s)" % (str(err), test))
              # end for test
              if ntests > 0:
                self.report.debug("%i tests added from %s" % (ntests, firstLevel))
            # end if isdir
          except Exception, e:
            self.report.warning(str(e))
        # end for name
      except Exception, e:
        self.report.warning(str(e))
    # end for path

    self.report.info("%i tests found" % testList.size())
    if testList.size() == 0:
      self.report.error("No test found")
      abort = True

    # Tests environment
    self.testEnv = os.environ.copy()

    # If cert and key are present, create proxy
    if not abort:
      if self.opter["user-cert"] and self.opter["user-key"]:
        self.report.info("User certificate and key specified. Creating proxy.")
        self.X509(True)
        if os.path.isfile("/etc/vomses/dteam-voms.hellasgrid.gr"):
          os.unlink("/etc/vomses/dteam-voms.hellasgrid.gr")
          self.report.info("Removing obsolete /etc/vomses/dteam-voms.hellasgrid.gr")
        f = core.SafeExecution.fromCommand("echo '%s' | %s --debug --rfc --voms %s --pwstdin" % (self.opter["user-pass"], self.voms_init, " --voms ".join(self.voms)),
                                           self.testEnv)
        (proxy, stdout, stderr) = f()
        self.report.entry("voms-proxy-init", proxy, stdout, stderr, None)
        abort = (proxy != 0)
      else:
        self.report.warning("There is no user certificate and password specified. The proxy will not be created.")

    # Execute the configurations
    if abort:
      self.report.error("Could not create the proxy. Aborting.")
    else:
      for c in testList.preconfig():
        self.report.info("Using configuration file %s" % c)
        # Preconfig scripts must be sourced
        cmd = "bash -c \"source '%s' ; env > /tmp/child_env\"" % c
        f = core.SafeExecution.fromCommand(cmd,
                                           cwd = os.path.dirname(c),
                                           env = self.testEnv)
        (exit, stdout, stderr) = f()
        self.report.entry(c, exit, stdout, stderr, cmd)
        abort = (abort or (exit != 0))

        # Put environment in a separate array for next tests
        if not abort:
          for line in open('/tmp/child_env', 'r'):
            line = line.strip()
            try:
              (key, value) = line.split('=', 1)
              if not key in self.testEnv or self.testEnv[key] != value:
                self.testEnv[key] = value
                self.report.debug("Environment variable %s set to %s" % (key, value))
            except:
              self.report.warning("Error processing line from env: '%s'" % line)
    
    # Disable the certificate and key (no proxy)
    if abort:
      self.report.error("Preconfiguration failed. Aborting.")
      ret = core.FAILED
    else:
      self.X509(False)
      # Execute tests
      testResult = core.SUCCESS 
      for group in td.prefixes:
        self.report.openGroup(group)
        
        for test in testList[group]:
          self.report.debug("Executing %s" % test.name)

          # Prepare proxy
          if test.proxy and test.proxy.lower() == "true":
            self.X509(True)
            
          # Run
          # TODO: Plugins
          f = core.SafeExecution.fromCommand(test.path,
                                             env = self.testEnv,
                                             cwd = os.path.dirname(test.path))
          (exit, stdout, stderr) = f()
          self.report.entry(test.name, exit, stdout, stderr, test.path)

          if exit != 0:
            testResult = core.FAILED

          self.X509(False)
          if exit != 0: self.report.debug("The test %s has failed!" % test.name)

        # end for test
        self.report.closeGroup()
      # end for group
      ret = core.SUCCESS
    #end if

    return ret


# The wrapper can be used directly
if __name__ == "__main__":
  opter = core.Opter.instance()
  opter.expand(STANDALONE_OPTIONS)
  try:
    opter.parse()
  except getopt.GetoptError, e:
    print e
    sys.exit(1)

  if opter["help"]:
    opter.help()
    sys.exit(0)

  try:
    # Execute the wrapper code
    td = TestSetDefinition("Tests", opter["test-location"], opter["test-prefix"])
    wrapper = Wrapper()
    # Create log directory
    if not os.path.exists(opter["log-dir"]):
      os.mkdir(opter["log-dir"])
      wrapper.report.debug("Created %s" % opter["log-dir"])
    # Run the tests
    status = wrapper.do(td)
    # Dump the logs
    testLog = os.path.join(opter["log-dir"], opter["log-test"])
    wrapper.report.write(open(testLog, "w"))
    # Copy style
    stylePath = os.path.join(os.path.dirname(__file__), "style")
    for f in os.listdir(stylePath):
      source = os.path.join(stylePath, f)
      dest   = os.path.join(opter["log-dir"], f)
      if not os.path.exists(dest) and not f.startswith("."):
        wrapper.report.debug("Copying %s to %s" % (source, dest))
        if os.path.isdir(source): shutil.copytree(source, dest, False)
        else:                     shutil.copy2(source, dest)

    sys.exit(status)
  except KeyboardInterrupt:
    print "Execution interrupted by the user!"
    sys.exit(1)
  except SystemExit:
    raise
  except Exception, e:
    traceback.print_exc(file = sys.stderr)
    sys.exit(1)
else:
  # Imported
  core.Engine.extension(Wrapper())

