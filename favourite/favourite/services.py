##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Requirements involving services, executables, etc.
#
##############################################################################

import commands
from core import Requirement

class Exec(Requirement):
  """
  Execute a command
  """

  def __init__(self, command, tolerant = False):
    """
    Constructor
    @param command  The command to be executed
    @param tolerant If True, even when the call fails, the requirement will succeed
    """
    super(Exec, self).__init__()
    self.cmd = command
    self.tolerant = tolerant

  def __str__(self):
    """
    String representation
    """
    return "[Exec] %s" % self.cmd

  def compile(self):
    """
    Bash equivalent command
    """
    return str(self.cmd)

  def do(self):
    """
    Perform the action
    """
    (exit, stdout) = commands.getstatusoutput(self.compile())
    exit = exit >> 8 # Upper byte is the actual exit code, lower byte is the signal that killed
    if self.tolerant and exit != 0: return (0, stdout, "Exit code was %d, but this is tolerated" % exit)
    else:                           return (exit, stdout, "")

class Service(Requirement):
  """
  Required services
  """

  START   = 0
  STOP    = 1
  RESTART = 2

  STR_EQUIV = ["start", "stop", "restart"]

  def __init__(self, service, status = START):
    """
    Constructor
    @param service The service name
    @param status  A value that has to be Service.START, Service, STOP or Service.RESTART
    """
    super(Service, self).__init__()
    self.service = service
    self.status  = status

  def __str__(self):
    """
    String representation
    """
    return "[Service] %s (%s)" % (self.service, Service.STR_EQUIV[self.status])

  def compile(self):
    """
    Bash equivalent command
    """
    if self.status == Service.START:  return "/sbin/service %s start"   % self.service
    elif self.status == Service.STOP: return "/sbin/service %s stop"    % self.service
    else:                             return "/sbin/service %s restart" % self.service

  def do(self):
    """
    Perform the action
    """
    (exit, stdout) = commands.getstatusoutput(self.compile())
    return (exit, stdout, "")
