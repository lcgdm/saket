#! /usr/bin/env python
##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Deployment script for FTS-oracle
#
##############################################################################

import platform
import socket
import favourite.core
import favourite.oracle
from favourite.configurer import Yaim
from favourite.core       import Opter, Engine, Property
from favourite.filesystem import Append, Directory, File
from favourite.installer  import YumRepo, Package
from favourite.oracle     import Oracle, SafeOracleAccount, OracleQuery, OraclePurge
from favourite.predefined import EMI
from favourite.services   import Exec, Service

import favourite.wrapper
from favourite.wrapper import TestSetDefinition

class Channel(Exec):
  """
  Channels are requirements for FTS
  """

  def __init__(self, channel, source, dest):
    """
    Constructor
    """
    super(Channel, self).__init__("/usr/bin/glite-transfer-channel-add -v -s https://%s:8443/glite-data-transfer-fts/services/ChannelManagement %s %s %s" \
              % (socket.gethostname(), channel, source, dest))
    self.channel = channel
    self.source  = source
    self.dest    = dest

  def __str__(self):
    """
    String representation
    """
    return "[Channel] %s (%s => %s)" % (self.channel, self.source, self.dest)

class ChannelShare(Exec):
  """
  So are Channel shares
  """

  def __init__(self, channel, vo, share):
    """
    Constructor
    """
    super(ChannelShare, self).__init__("/usr/bin/glite-transfer-channel-setvoshare -s https://%s:8443/glite-data-transfer-fts/services/ChannelManagement %s %s %d"\
              % (socket.gethostname(), channel, vo, share))
    self.channel = channel
    self.vo      = vo
    self.share   = share

  def __str__(self):
    """
    String representation
    """
    return "[ChannelShare] %s (%s: %d)" % (self.channel, self.vo, self.share)

    
class FTSOracle(EMI):
  """
  Implementation of a FTS Oracle node
  """

  def __init__(self):
    """
    Setup the node
    """
    super(FTSOracle, self).__init__(enable_testing = False, enable_updates = False)
    
    opter = Opter.instance()

    # Oracle installation
    Oracle()

    # Repositories
    opter = Opter.instance()
    if opter["repository"]:
      i = 0
      for r in [x for x in opter["repository"] if x.lower() != 'none']:
        YumRepo("emi-fts_oracle%d" % i,
                source  = r,
                protect = 1,
                priority = 0)
        i+= 1

    # FTS and FTA
    arch = platform.uname()[4]
    Package(["emi-fts_oracle.%s" % arch, "emi-fta_oracle.%s" % arch, "xml-commons-apis"])

    # Safe user
    soa = SafeOracleAccount()

    # Drop old tables
    OracleQuery(Property(soa, "%s", "oracle-user"), Property(soa, "%s", "oracle-pass"),
                file = "/etc/glite-data-transfer-fts/schema/oracle/oracle-drop.sql")
    OracleQuery(Property(soa, "%s", "oracle-user"), Property(soa, "%s", "oracle-pass"),
                file = "/var/lib/delegation-interface/schema/oracle/oracle-drop.sql")
    OraclePurge(Property(soa, "%s", "oracle-user"), Property(soa, "%s", "oracle-pass"))

    # Re-create
    OracleQuery(Property(soa, "%s", "oracle-user"), Property(soa, "%s", "oracle-pass"),
                file = "/etc/glite-data-transfer-fts/schema/oracle/oracle-schema.sql")
    OracleQuery(Property(soa, "%s", "oracle-user"), Property(soa, "%s", "oracle-pass"),
                file = "/var/lib/delegation-interface/schema/oracle/oracle-schema.sql")
    OracleQuery(Property(soa, "%s", "oracle-user"), Property(soa, "%s", "oracle-pass"),
                file = "/etc/glite-data-transfer-fts/schema/oracle/fts_history_pack.sql")
    OracleQuery(Property(soa, "%s", "oracle-user"), Property(soa, "%s", "oracle-pass"),
                file = "/etc/glite-data-transfer-fts/schema/oracle/fts_history_body_pack.sql")
    OracleQuery(Property(soa, "%s", "oracle-user"), Property(soa, "%s", "oracle-pass"),
                file = "/etc/glite-data-transfer-fts/schema/oracle/fts_purge_pack.sql")
    OracleQuery(Property(soa, "%s", "oracle-user"), Property(soa, "%s", "oracle-pass"),
                file = "/etc/glite-data-transfer-fts/schema/oracle/fts_purge_body_pack.sql")

    # Grant access to unprivileged user
    OracleQuery(Property(soa, "%s", "oracle-user"), Property(soa, "%s", "oracle-pass"),
      sql = Property(soa, """
      SET FEEDBACK 0;
      SET PAGESIZE 0;
      SET LINESIZE 500;
      select 'grant all on '||TABLE_NAME||' to %s;' from user_tables;
      """, "oracle-unpriv"), out = "/tmp/favgrant.sql")
    OracleQuery(Property(soa, "%s", "oracle-user"), Property(soa, "%s", "oracle-pass"),
                file = "/tmp/favgrant.sql")

    # Create synonyms
    OracleQuery(Property(soa, "%s", "oracle-user"), Property(soa, "%s", "oracle-pass"),
      sql = """
      SET FEEDBACK 0;
      SET PAGESIZE 0;
      SET LINESIZE 500;
      select 'drop synonym '||TABLE_NAME||';' from user_tables;
      """, out = "/tmp/favdropsyn.sql")
    OracleQuery(Property(soa, "%s", "oracle-unpriv"), Property(soa, "%s", "oracle-pass"),
                file = "/tmp/favdropsyn.sql")

    OracleQuery(Property(soa, "%s", "oracle-user"), Property(soa, "%s", "oracle-pass"),
      sql = Property(soa, """
      SET FEEDBACK 0;
      SET PAGESIZE 0;
      SET LINESIZE 500;
      select 'create synonym '||TABLE_NAME||' for %s.'||TABLE_NAME||';' from user_tables;
      """, "oracle-user"), out = "/tmp/favsyn.sql")
    OracleQuery(Property(soa, "%s", "oracle-unpriv"), Property(soa, "%s", "oracle-pass"),
                file = "/tmp/favsyn.sql")

    # Java location
    javaLocation = "/usr/lib/jvm/jre-1.6.0"

    # site-info.def
    Append(["FTS_HOST=%s" % socket.gethostname(),
            "FTS_HOST_ALIAS=$FTS_HOST",
            "FTS_SERVER_URL=\"https://${FTS_HOST}:8443/glite-data-transfer-fts\"",
            "FTS_DB_TYPE=ORACLE",
            Property(soa, "FTS_DBURL=\"jdbc:oracle:thin:@%s\"", "oracle-conn"),
            Property(soa, "FTS_DB_USER=%s",                     "oracle-unpriv"),
            Property(soa, "FTS_DB_PASSWORD=%s",                 "oracle-pass"),
            Property(soa, "FTS_DB_ADMIN_USER=%s",               "oracle-user"),
            Property(soa, "FTS_DB_ADMIN_PASSWORD=%s",           "oracle-pass"),
            Property(soa, "ORACLE_LOCATION=%s",                 "oracle-location"),
            "TOMCAT_USER=tomcat",
            "JAVA_LOCATION=\"%s\"" % javaLocation,
            "FTA_MACHINES=ONE",
            "FTA_AGENTS_ONE_HOSTNAME=$FTS_HOST",
            "FTA_AGENTS_ONE=\"DTEAM VOMSTEST CERN-CERN CERN-DESY DESY-CERN DESY-DESY\"",
            "FTA_CERN_CERN=SRMCOPY",
            "FTA_CERN_CERN_LOG_PRIORITY=DEBUG",
            "FTA_CERN_DESY=URLCOPY",
            "FTA_CERN_DESY_LOG_PRIORITY=DEBUG",
            "FTA_DESY_CERN=URLCOPY",
            "FTA_DESY_CERN_LOG_PRIORITY=DEBUG",
            "FTA_DESY_DESY=SRMCOPY",
            "FTA_DESY_DESY_LOG_PRIORITY=DEBUG",
            "FTA_DTEAM=VOAGENT",
            "FTA_DTEAM_LOG_PRIORITY=DEBUG",
            "FTA_VOMSTEST=VOAGENT",
            "FTA_VOMSTEST_LOG_PRIORITY=DEBUG",
            "FTA_VOMSTEST_AGENT_NAME=org.glite.voms-test",
            "FTA_GLOBAL_DBTYPE=ORACLE",
            Property(soa, "FTA_GLOBAL_DB_CONNECTSTRING=\"%s\"", "oracle-conn"),
            Property(soa, "FTA_GLOBAL_DB_USER=\"%s\"",          "oracle-unpriv" ),
            Property(soa, "FTA_GLOBAL_DB_PASSWORD=\"%s\"",      "oracle-pass" ),
            "FTA_TYPEDEFAULT_SRMCOPY_ACTIONS_SURLNORMALIZATION=compact-with-port",
            "FTS_MSG_BROKER=gridmsg007.cern.ch:6163",
            "FTS_MSG_START=transfer.test.msg.star",
            "FTS_MSG_COMPLETE=transfer.test.msg.complet",
            "FTS_MSG_CRON=transfer.fts_monitoring_queue_statu",
            "FTS_MSG_FQDN=%s" % socket.gethostname(),
            "FTS_MSG_TOPIC=true",
            "FTS_MSG_TTL=24",
            "FTS_MSG_ACTIVE=true",
            "FTS_MSG_ENABLELOG=true",
            "FTS_MSG_ENABLEMSGLOG=true",
            "FTS_MSG_LOGFILEDIR=/var/log/glite/",
            "FTS_MSG_LOGFILENAME=msg.log",
            "FTS_MSG_USERNAME=ftspublisher",
            "FTS_MSG_PASSWORD=ftspub226",
            "FTS_MSG_USE_BROKER_CREDENTIALS=true",
           ], "/etc/yaim/site-info.def")

    # Modify tomcat 
    self.tomcatv = int(platform.dist()[1][0])
    Exec("sed -i '/JAVA_HOME/ d' /etc/tomcat%d/tomcat%d.conf" % (self.tomcatv, self.tomcatv))
    Append("JAVA_HOME=%s" % javaLocation, "/etc/tomcat%d/tomcat%d.conf" % (self.tomcatv, self.tomcatv))

    # Restart tomcat
    Service("tomcat%d" % self.tomcatv, Service.RESTART)

    # Configure FTS_oracle
    Exec("fetch-crl", tolerant = True)
    Yaim("emi_fts2")

    # Add channels
    Channel("CERN-CERN", "cert-tb-cern", "cert-tb-cern")
    Channel("CERN-DESY", "cert-tb-cern", "desycerttb"  )
    Channel("DESY-CERN", "desycerttb"  , "cert-tb-cern")
    Channel("DESY-DESY", "desycerttb"  , "desycerttb"  )

    # Configure FTA_oracle
    Yaim("emi_fta2")

    # Start transfer-agents and sd2cache
    Service("transfer-agents", Service.RESTART)
    Service("glite-sd2cache", Service.RESTART)

    # Run sd2cache
    Exec("/etc/cron.hourly/glite-sd2cache-cron")

    # Add share
    for channel in ["CERN-CERN", "CERN-DESY", "DESY-CERN", "DESY-DESY"]:
      ChannelShare(channel, "dteam", 50)

  def logs(self):
    """
    List of logs to recover
    """
    return ["/var/log/glite",
            "/var/log/tomcat%d" % self.tomcatv,
            "/var/tmp/glite-url-copy-edguser",
            "/tmp/gridftp.log",
            "/var/log/bdii/bdii-update.log",
            "/tmp/agent-stackframes.log",
            "/var/tmp/msg.log",
            "/etc/tomcat%d/tomcat%d.conf" % (self.tomcatv, self.tomcatv)]

  def test(self):
    """
    Performs the testing
    This is called by the wrapper, who expects a TestDefinition as a response
    """
    Engine.instance().deploy([Package("fts-tests"),
      ], label = "FTS Oracle test preparation")

    return TestSetDefinition(title = "Testing",
                             location = ["/usr/tests/fts/functional", "/usr/tests/fts/regression"],
                             prefixes = ["fts"])

# Acts like entry point
if __name__ == "__main__":
  favourite.core.run(FTSOracle)
