#! /usr/bin/env python
import os
import unittest
from Saket.Environment import *

class EnvironmentTest(unittest.TestCase):

  def setUp(self):
    self.env = Environment()

  def testDict(self):
    self.env.whatever = '42'
    assert self.env['whatever'] == '42'

  def testStack(self):
    prevPath = self.env['PATH']
    self.env.saveAndSet({'PATH': '/fake/bin'})
    assert self.env['PATH'] == '/fake/bin'
    self.env.restore()
    assert self.env['PATH'] == prevPath

class ArgTest(unittest.TestCase):

  def setUp(self):
    setupEnvironment(['--verbose', '--force-test',
                      '--config', os.path.join(os.getenv('SAKET_CLI'), 'Config/default.py')])
    self.env = Environment()

  def testSetup(self):
    assert self.env.DEBUG
    assert self.env.FORCE_TEST

if __name__ == "__main__":
  unittest.main()
