#! /usr/bin/env python
# Test the implementation of the deployment parser
# As reference logs are used, the results are known
import unittest
from Saket.Parsers.SaketParser import SaketParser

class DeploymentFailed(unittest.TestCase):
  """
  Test an empty test log
  """
  
  LOG_FILE = "../references/deployment-failed.xml"

  def setUp(self):
    self.logFile = open(self.LOG_FILE, "r")
    self.parser  = SaketParser()
    self.log     = self.parser.parse(self.logFile)

  def tearDown(self):
    self.logFile.close()

  def testTotal(self):
    assert self.log.total == self.log.success + self.log.failed

  def testPassed(self):
    assert self.log.success == 21

  def testFailed(self):
    assert self.log.failed == 1

  def testTitle(self):
    assert self.log.title

  def testTimes(self):
    assert self.log.startTime <= self.log.endTime

  def testStatus(self):
    assert self.log.status == 'failed'


class DeploymentLegacyFailed(DeploymentFailed):
  """
  Test a log that has failed (legacy support)
  """

  LOG_FILE = "../references/deployment-legacy-failed.xml"

  def testPassed(self):
    assert self.log.success == 17 

  def testFailed(self):
    assert self.log.failed == 1

class DeploymentLegacyPassed(DeploymentFailed):
  """
  Test a log that has passed (legacy support)
  """

  LOG_FILE = "../references/deployment-legacy-passed.xml"

  def testPassed(self):
    assert self.log.success == 21

  def testFailed(self):
    assert self.log.failed == 0

  def testStatus(self):
    assert self.log.status == 'success'

if __name__ == "__main__":
  unittest.main()
