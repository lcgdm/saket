#! /usr/bin/env python
# Test the implementation of the build parser
# As reference logs are used, the results are known
import unittest
from Saket.Parsers.ETICSParser import ETICSParser

class BuildFailed(unittest.TestCase):
  """
  Test an empty test log
  """
  
  LOG_FILE = "../references/build-failed.xml"

  def setUp(self):
    self.logFile = open(self.LOG_FILE, "r")
    self.parser  = ETICSParser()
    self.log     = self.parser.parse(self.logFile)

  def tearDown(self):
    self.logFile.close()

  def testTotal(self):
    assert self.log.total == self.log.success + self.log.failed

  def testPassed(self):
    assert self.log.success == 108

  def testFailed(self):
    assert self.log.failed == 1

  def testTitle(self):
    assert self.log.title

  def testTimes(self):
    assert self.log.startTime <= self.log.endTime

  def testStatus(self):
    assert self.log.status == 'failed'


class BuildPassed(BuildFailed):
  """
  Test a log that has passed
  """

  LOG_FILE = "../references/build-success.xml"

  def testPassed(self):
    assert self.log.success == 117

  def testFailed(self):
    assert self.log.failed == 0

  def testStatus(self):
    assert self.log.status == 'success'

if __name__ == "__main__":
  unittest.main()
