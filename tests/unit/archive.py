#! /usr/bin/env python
import unittest
from datetime import datetime
from Saket.Archive import Archive, ArchiveEntry
from Saket.Logger  import Logger

Logger.mute()

class ArchiveEntryTest(unittest.TestCase):

  def setUp(self):
    self.tm    = datetime(year = 2011, month = 06, day = 20,
                          hour= 01, minute = 29, second = 20)
    self.entry = ArchiveEntry(self.tm, "../references")

  def testTimes(self):
    assert self.entry.duration  == 12502
    assert self.entry.eduration == 10602

  def testValues(self):
    assert self.entry.build      == (7, 7)
    assert self.entry.deployment == (8, 15)
    assert self.entry.test       == (4, 14)

class ArchiveTest(unittest.TestCase):

  def setUp(self):
    self.archive = Archive("../references")

  def testDicts(self):
    try:
      entries = self.archive.entries[2011][06][20]
    except:
      self.fail("Tree is not OK")
    assert len(entries) == 1

  def testRepr(self):
    assert str(self.archive) == '1 entries'

if __name__ == "__main__":
  unittest.main()
