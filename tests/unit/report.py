#! /usr/bin/env python
import unittest
from Saket.Report import NodeSet, Log

class NodeSetTest(unittest.TestCase):

  def testSingleArchitecture(self):
    dLog01 = Log()
    dLog01.architecture = 'sl5_x86_64_gcc412'
    dLog02 = Log()
    dLog02.architecture = 'sl5_x86_64_gcc412'
    set = NodeSet('whatever', '55-55-55', [(dLog01, None), (dLog02, None)])

    assert set.get_architecture() == 'sl5_x86_64_gcc412'

  def testMultiArchitecture(self):
    dLog01 = Log()
    dLog01.architecture = 'sl5_x86_64_gcc412'
    dLog02 = Log()
    dLog02.architecture = 'sl5_i386_gcc412'
    set = NodeSet('whatever', '55-55-55', [(dLog01, None), (dLog02, None)])

    assert set.get_architecture() == 'multiplatform'

if __name__ == "__main__":
  unittest.main()
