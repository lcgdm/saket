#! /usr/bin/env python
import unittest
from Saket.Tasks import generateTaskTree

class TaskTreeTest(unittest.TestCase):

  def setUp(self):
    self.tree = generateTaskTree(
      {
        'project': {
            'URLNAME_TEST_INFIX': 'test',
            'CHECKOUT_EXTRA': '--noask --continueonerror --verbose --frombinary --runtimedeps',
            'BUILD_EXTRA': '--verbose --continueonerror',
            'PROJECT_CONFIGURATION': 'first-level',

            'SUBSYSTEMS': {
              'something': {
                'CONFIGURATION': 'something.HEAD',
                'URLNAME_PREFIX': 'something_head',
                'BUILD': {
                  'GL32': {
                    'PROJECT_CONFIGURATION': 'overriden',
                    'ARCHITECTURES': ['sl5_x86_64_gcc412EPEL'],
                  },
                },
                'TEST': {
                    'MyTest': ('TEST_COMPONENT', 'TEST_CONFIGURATION')
                },
              },
              'whatever' : {
                'CONFIGURATION': 'whatever.HEAD',
                'URLNAME_PREFIX': 'whatever_head',
                'BUILD' : {
                  'GL32': {
                    'ARCHITECTURES': ['sl5_x86_64_gcc412']
                  },
                }
              }
            }
        }
      }
    )

  def testBuild(self):
    assert self.tree[0].projectConfiguration == 'overriden'
    assert self.tree[1].projectConfiguration == 'first-level'
    assert self.tree[1].architecture == 'sl5_x86_64_gcc412'

  def testTest(self):
    assert self.tree[0][0].label         == 'MyTest'
    assert self.tree[0][0].module        == 'TEST_COMPONENT'
    assert self.tree[0][0].configuration == 'TEST_CONFIGURATION'
    assert self.tree[0][0].architecture  == 'sl5_x86_64_gcc412EPEL'
    assert self.tree[0][0].parent == self.tree[0]

if __name__ == "__main__":
  unittest.main()
