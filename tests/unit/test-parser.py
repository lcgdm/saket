#! /usr/bin/env python
# Test the implementation of the test parser
# As reference logs are used, the results are known
import unittest
from Saket.Parsers.SaketParser import SaketParser

class TestEmpty(unittest.TestCase):
  """
  Test an empty test log
  """
  
  LOG_FILE = "../references/tests-empty.xml"

  def setUp(self):
    self.logFile = open(self.LOG_FILE, "r")
    self.parser  = SaketParser()
    self.log     = self.parser.parse(self.logFile)

  def tearDown(self):
    self.logFile.close()

  def testTotal(self):
    assert self.log.total == self.log.success + self.log.failed

  def testPassed(self):
    assert self.log.success == 0

  def testFailed(self):
    assert self.log.failed == 0

  def testTitle(self):
    assert self.log.title

  def testTimes(self):
    assert self.log.startTime <= self.log.endTime

  def testStatus(self):
    assert self.log.status == 'failed'

class TestFailed(TestEmpty):
  """
  Test a log that has failed
  """

  LOG_FILE = "../references/tests-failed.xml"

  def testPassed(self):
    assert self.log.success == 50

  def testFailed(self):
    assert self.log.failed == 15

class TestPassed(TestEmpty):
  """
  Test a log that has passed
  """

  LOG_FILE = "../references/tests-passed.xml"

  def testPassed(self):
    assert self.log.success == 63

  def testFailed(self):
    assert self.log.failed == 0

  def testStatus(self):
    assert self.log.status == 'success'

if __name__ == "__main__":
  unittest.main()
