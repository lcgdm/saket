#! /bin/bash
# Wrapper for the tests. It will set up the environment properly so
# all the imports work.
# Then, it will trigger the tests.

# SAKET Command Line Interface
if [ -z "$SAKET_CLI" ]; then
  export SAKET_CLI=`readlink -f ../cli`
fi

# ETICS Home
if [ -z "$ETICS_HOME" ]; then
  echo "ETICS_HOME not set, and I can not guess"
  exit 1
fi

# Python version
PYVER=`python -V 2>&1 | cut -d ' ' -f 2 | cut -d '.' -f 1,2`

# Python Path
echo "$PYTHONPATH" | grep -q "$ETICS_HOME"
if [ $? -ne 0 ]; then
  export PYTHONPATH=$PYTHONPATH:$ETICS_HOME/bin:$ETICS_HOME/lib/python$PYVER/site-packages
fi
export PYTHONPATH=$PYTHONPATH:$SAKET_CLI

# Information
echo
echo "SAKET CLI Path: '$SAKET_CLI'"
echo "ETICS Home:     '$ETICS_HOME'"
echo "Python version: '$PYVER'"
echo "PYTHONPATH:     '$PYTHONPATH'"
echo

# Run the tests!
pushd unit &> /dev/null

for test in *.py; do
  ./$test
done

popd  &> /dev/null
