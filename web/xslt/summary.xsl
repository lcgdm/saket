<?xml version="1.0"?>
<!--
##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Álvarez Ayllón <alejandro.alvarez.ayllon@cern.ch>, CERN
#
##############################################################################
-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sak="https://svnweb.cern.ch/trac/lcgutil/saket"
  xmlns:date="date"
  xmlns="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml"
              indent="yes"
              omit-xml-declaration="no"
              doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
              doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

  <!-- Functions. Note: WebKit browsers do not support imports and includes, so it is not possible
                         right now to keep some modularity :( -->
  <!-- Month name constant -->
  <date:months>
    <date:month abbr="jan">January</date:month>
    <date:month abbr="feb">February</date:month>
    <date:month abbr="mar">March</date:month>
    <date:month abbr="apr">April</date:month>
    <date:month abbr="may">May</date:month>
    <date:month abbr="jun">June</date:month>
    <date:month abbr="jul">July</date:month>
    <date:month abbr="aug">August</date:month>
    <date:month abbr="sep">September</date:month>
    <date:month abbr="oct">October</date:month>
    <date:month abbr="nov">November</date:month>
    <date:month abbr="dec">December</date:month>
  </date:months>

  <!-- Day name constant -->
  <date:days>
    <date:day abbr="mon">Monday</date:day>
    <date:day abbr="tue">Tuesday</date:day>
    <date:day abbr="wed">Wednesday</date:day>
    <date:day abbr="thu">Thursday</date:day>
    <date:day abbr="fri">Friday</date:day>
    <date:day abbr="sat">Saturday</date:day>
    <date:day abbr="sun">Sunday</date:day>
  </date:days>

  <!-- Function to print the month name -->
  <xsl:template name="month-name">
    <xsl:param name="month"/>
    <xsl:value-of select="document('')/*/date:months/date:month[position()=$month]"/>
  </xsl:template>

  <!-- Function to print the day name -->
  <xsl:template name="day-name">
    <xsl:param name="day-of-week"/>
    <xsl:value-of select="document('')/*/date:days/date:day[position()=$day-of-week + 1]"/>
  </xsl:template>

  <!-- Function to replace a string with another -->
  <xsl:template name="replace">
    <xsl:param name="string"/>
    <xsl:param name="pattern"/>
    <xsl:param name="replace"/>
    <xsl:variable name="before" select="substring-before($string, $pattern)"/>
    <xsl:variable name="after" select="substring-after($string, $pattern)"/>
    <xsl:choose>
      <xsl:when test="$after">
        <xsl:value-of select="$before"/>
        <xsl:value-of select="$replace"/>
        <xsl:call-template name="replace">
          <xsl:with-param name="string" select="$after"/>
          <xsl:with-param name="pattern" select="$pattern"/>
          <xsl:with-param name="replace" select="$replace"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Appends a 0 at the beginning if the number has only one digit -->
  <xsl:template name="two-digits">
    <xsl:param name="value"/>
    <xsl:if test="$value &lt; 10 and string-length($value) &lt; 2">0</xsl:if><xsl:value-of select="$value"/>
  </xsl:template>


  <!-- Format seconds as HH:MM:SS -->
  <xsl:template name="format-duration">
    <xsl:param name="seconds"/>

    <xsl:call-template name="two-digits">
      <xsl:with-param name="value">
        <xsl:value-of select="round($seconds div 3600)"/>
      </xsl:with-param>
    </xsl:call-template>:<xsl:call-template name="two-digits">
      <xsl:with-param name="value">
        <xsl:value-of select="floor($seconds div 60) mod 60"/>
      </xsl:with-param>
    </xsl:call-template>:<xsl:call-template name="two-digits">
      <xsl:with-param name="value">
        <xsl:value-of select="$seconds mod 60"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- Percentage -->
  <xsl:template name="percentage">
    <xsl:param name="node"/>
    <xsl:choose>
      <xsl:when test="$node/@total > 0">
        <xsl:value-of select="round(($node/@success div $node/@total) * 100)"/>
      </xsl:when>
      <xsl:otherwise>
        0
      </xsl:otherwise>
    </xsl:choose>%
  </xsl:template>
  
  <!-- Document level -->
  <xsl:template match="/sak:summary">
    <html>
      <head>
        <link rel="stylesheet" href="Templates/css/general.css" type="text/css"/>
        <title><xsl:value-of select="@title"/></title>
        <script type="text/javascript" src="Templates/js/slide.js"></script>
        <script type="text/javascript" src="Templates/js/dialog.js"></script>
      </head>
      <body>
        <div class="header">
          <h1>
            <xsl:value-of select="@title"/>
            <a href="index.html">
              <img src="Templates/img/table.png" alt="[Table]" title="View as a table"/>
            </a>
            <a href="calendar.html">
              <img src="Templates/img/calendar.png" alt="[Calendar]" title="View as a calendar"/>
            </a>
          </h1>
        </div>

        <table class="summary">
          <thead>
            <tr>
              <th>Day</th>
              <th>Time</th>
              <th>Duration</th>
              <th class="log">Build</th>
              <th class="log">Deployment</th>
              <th class="log">Test</th>
            </tr>
          </thead>
          <tbody>
            <xsl:apply-templates select="descendant::sak:month"/>
          </tbody>
        </table>
        
        <div class="footer">
          <p>
            <acronym title="Swiss Army Knife for ETICS Testing">SAKET</acronym> Archive (C) Members of the EGEE Collaboration<br/>
            Alejandro Alvarez &lt;<a href="mailto:aalvarez@cern.ch">aalvarez@cern.ch</a>&gt;
            <a style="float:right" href="https://svnweb.cern.ch/trac/saket/">Documentation</a>
          </p>
        </div>
      </body>
    </html>
  </xsl:template>

  <!-- Per month -->
  <xsl:template match="sak:month">
    <tr class="month">
      <td colspan="6">
        <xsl:call-template name="month-name">
          <xsl:with-param name="month" select="@month"/>
        </xsl:call-template>
        &#160;
        <xsl:value-of select="../@year"/>
      </td>
    </tr>
    <xsl:apply-templates select="descendant::sak:entry"/>
  </xsl:template>


  <!-- Each entry -->
  <xsl:template match="sak:entry">
    <!-- Time replacing : with - (for links) -->
    <xsl:variable name="time-dash">
      <xsl:call-template name="replace">
        <xsl:with-param name="string" select="@time"/>
        <xsl:with-param name="pattern" select="':'"/>
        <xsl:with-param name="replace" select="'-'"/>
      </xsl:call-template>
    </xsl:variable>

    <!-- Two-digit day -->
    <xsl:variable name="day">
      <xsl:call-template name="two-digits">
        <xsl:with-param name="value" select="../@day"/>
      </xsl:call-template>
    </xsl:variable>

    <!-- Two-digit month -->
    <xsl:variable name="month">
      <xsl:call-template name="two-digits">
        <xsl:with-param name="value" select="../../@month"/>
      </xsl:call-template>
    </xsl:variable>

    <!-- Report location -->
    <xsl:variable name="report-location"><xsl:value-of select="../../../@year"/>-<xsl:value-of select="$month"/>-<xsl:value-of select="$day"/>_<xsl:value-of select="$time-dash"/></xsl:variable>

    <!-- Entry in the table -->
    <tr onclick="javascript:window.location='{$report-location}'">
      <td class="day">
        <a href="{$report-location}">
          <xsl:call-template name="day-name">
            <xsl:with-param name="day-of-week" select="../@dweek"/>
          </xsl:call-template>
          &#160;
          <xsl:value-of select="$day"/>
        </a>
      </td>
      <td><xsl:value-of select="@time"/></td>
      <td>
        <!-- Duration is a bunch of seconds. Hence, write it more nicely (as hours:minutes:seconds) -->
        <xsl:call-template name="format-duration">
          <xsl:with-param name="seconds">
            <xsl:choose>
              <xsl:when test="string-length(@duration)">
                <xsl:value-of select="@duration"/>
              </xsl:when>
              <xsl:otherwise>
                <!-- If real time not available, use stimate -->
                <span class="ewarning"><xsl:value-of select="@eduration"/></span>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:with-param>
        </xsl:call-template>
      </td>
      <!-- Success percentages -->
      <td><xsl:call-template name="percentage"><xsl:with-param name="node" select="sak:build"/></xsl:call-template></td>
      <td><xsl:call-template name="percentage"><xsl:with-param name="node" select="sak:deployment"/></xsl:call-template></td>
      <td><xsl:call-template name="percentage"><xsl:with-param name="node" select="sak:test"/></xsl:call-template></td>
    </tr>
  </xsl:template>

</xsl:stylesheet>
