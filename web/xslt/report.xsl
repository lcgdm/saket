<?xml version="1.0"?>
<!--
##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Álvarez Ayllón <alejandro.alvarez.ayllon@cern.ch>, CERN
#
##############################################################################
-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sak="https://svnweb.cern.ch/trac/lcgutil/saket"
  xmlns="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml"
              indent="yes"
              omit-xml-declaration="no"
              doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
              doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

  <!-- Document level -->
  <xsl:template match="/sak:report">
    <html>
      <head>
        <link rel="stylesheet" href="../Templates/css/general.css" type="text/css"/>
        <title><xsl:value-of select="@title"/></title>
      </head>
      <body>
        <div class="header">
          <h1>
            <xsl:value-of select="@title"/> (<xsl:value-of select="@time"/>)
            <a href="..">
              <img src="../Templates/img/up.png" alt="[↑]" title="View archive"/>
            </a>
          </h1>
        </div>
        <xsl:variable name="nprojects" select="count(sak:project)"/>
        <xsl:apply-templates select="sak:project">
          <xsl:with-param name="nprojects" select="$nprojects"/>
        </xsl:apply-templates>
        <div class="footer">
          <p>
            <acronym title="Swiss Army Knife for ETICS Testing">SAKET</acronym> Archive (C) Members of the EGEE Collaboration<br/>
            Alejandro Alvarez &lt;<a href="mailto:aalvarez@cern.ch">aalvarez@cern.ch</a>&gt;
            <a style="float:right" href="https://svnweb.cern.ch/trac/saket/">Documentation</a>
          </p>
        </div>
      </body>
    </html>
  </xsl:template>

  <!-- First level: project -->
  <xsl:template match="sak:project">
    <xsl:param name="nprojects"/>
    <xsl:if test="$nprojects > 1">
      <h2 class="project"><xsl:value-of select="@name"/></h2>
    </xsl:if>
    <div>
      <xsl:call-template name="table"/>
      <xsl:apply-templates select="sak:subsystem"/>
    </div>
  </xsl:template>

  <!-- Second level: subsystem -->
  <xsl:template match="sak:subsystem">
    <h3 class="subsystem"><xsl:value-of select="@name"/></h3>
    <div>
      <xsl:call-template name="table"/>
      <xsl:apply-templates select="sak:component"/>
    </div>
  </xsl:template>

  <!-- Third level: component -->
  <xsl:template match="sak:component">
    <h4 class="component"><xsl:value-of select="@name"/></h4>
    <div>
      <xsl:call-template name="table"/>
    </div>
  </xsl:template>

  <xsl:key name="configuration" match="sak:build" use="concat(@config, '-', ../@name)"/>

  <!-- Put a table with the results (Common) -->
  <xsl:template name="table">
    <xsl:if test="count(sak:build) > 0">
      <!-- Group by configuration -->
      <xsl:for-each select="sak:build[generate-id(.) = generate-id(key('configuration', concat(@config, '-', ../@name))[1])]">
        <xsl:variable name="configuration" select="@config"/>
        <h5>
          <xsl:choose>
            <xsl:when test="@status = 'external'">
              External repositories
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$configuration"/>
            </xsl:otherwise>
          </xsl:choose>
        </h5>
        <table class="report">
          <thead>
            <tr>
              <th class="project">Project config.</th>
              <th class="platform">Platform</th>
              <th class="build">Build</th>
              <th class="component">Test component</th>
              <th class="deployment">Deployment</th>
              <th class="test">Test</th>
            </tr>
          </thead>
          <tbody>

          <xsl:for-each select="../sak:build[@config = $configuration]">
            <xsl:sort select="@projectconfig"/>

            <xsl:variable name="prconfig" select="@projectconfig"/>
            <xsl:variable name="subconf"  select="@config"/>
            <xsl:variable name="groupedProject" select="../sak:build[@config = $configuration and @projectconfig = $prconfig]"/>
            <xsl:variable name="prcount" select="count($groupedProject/sak:deployment | $groupedProject/sak:set) + count($groupedProject[not(sak:deployment | sak:set)])"/>
            <xsl:variable name="ntests" select="count(sak:deployment | sak:set)"/>
            <xsl:variable name="firstOfProject" select="count(preceding-sibling::sak:build[@projectconfig = $prconfig and @config = $subconf]) = 0"/>
            <xsl:variable name="parity">
              <xsl:choose>
                <xsl:when test="(count(preceding::sak:build) + 1) mod 2 = 0">even</xsl:when>
                <xsl:otherwise>odd</xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <xsl:variable name="rowspan">
              <xsl:choose>
                <xsl:when test="$ntests"><xsl:value-of select="$ntests"/></xsl:when>
                <xsl:otherwise>1</xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <tr>
              <xsl:attribute name="class">
                <xsl:value-of select="$parity"/>
                <xsl:choose>
                  <xsl:when test="$firstOfProject">
                    project-row
                  </xsl:when>
                  <xsl:otherwise>
                    architecture-row
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>

              <xsl:if test="$firstOfProject">
                <td rowspan="{$prcount}" class="project-config">
                  <xsl:value-of select="@projectconfig"/>
                </td>
              </xsl:if>
              
              <td rowspan="{$rowspan}">
                <a class="repo">
                  <xsl:attribute name="href">
                    <xsl:choose>
                      <xsl:when test="@status = 'external'">
                        <xsl:value-of select="@external"/>
                      </xsl:when>
                      <xsl:otherwise>
                        http://etics-repository.cern.ch/repository/pm/volatile/repomd/id/<xsl:value-of select="@id"/>/<xsl:value-of select="@architecture"/>/index.html
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:attribute>
                  <xsl:value-of select="@architecture"/>
                </a>
              </td>
              <td rowspan="{$rowspan}"><xsl:call-template name="fancy-status"/></td>
              <xsl:choose>
                <xsl:when test="$ntests > 0">
                  <xsl:apply-templates select="sak:deployment[position() = 1] | sak:set[position() = 1]"/>
                </xsl:when>
                <xsl:otherwise>
                  <td>-</td>
                  <td>-</td>
                  <td>-</td>
                </xsl:otherwise>
              </xsl:choose>
            </tr>
            <xsl:for-each select="sak:deployment[position() > 1] | sak:set[position() > 1]">
              <tr class="{$parity}">
                <xsl:apply-templates select="."/>
              </tr>
            </xsl:for-each>
          </xsl:for-each>

          </tbody>
        </table>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>

  <!-- Multinode support -->
  <xsl:template match="sak:set">
    <xsl:variable name="multinode" select="count(sak:deployment) > 1"/>
    
    <td>
      <xsl:choose>
        <xsl:when test="@architecture != 'None'">
          <a href="http://etics-repository.cern.ch:8080/repository/reports/id/{@id}/{@architecture}/-/reports/index.html">
            <xsl:value-of select="@name"/>
          </a>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@name"/>
        </xsl:otherwise>
      </xsl:choose>
    </td>
    <td>
      <xsl:for-each select="sak:deployment">
        <xsl:if test="@module != 'None' and $multinode">
          <span class="node-label"><xsl:value-of select="@module"/></span>
        </xsl:if>
        <xsl:call-template name="fancy-status"/>
        <br/>
      </xsl:for-each>
    </td>
    <td>
      <xsl:for-each select="sak:deployment">
        <xsl:if test="not(sak:test)">
          -
        </xsl:if>
        <xsl:for-each select="sak:test">
          <xsl:if test="$multinode">
            <span class="node-label"><xsl:value-of select="../@component"/></span>
          </xsl:if>
          <xsl:call-template name="fancy-status"/>
          <br/>
        </xsl:for-each>
      </xsl:for-each>
    </td>
  </xsl:template>

  <!-- Put the result of the deployment -->
  <xsl:template match="sak:deployment">
    <td><xsl:value-of select="@component"/></td>
    <td>
      <xsl:call-template name="fancy-status"/>
    </td>
    <td>
      <xsl:for-each select="sak:test">
        <xsl:call-template name="fancy-status"/>
      </xsl:for-each>
    </td>
  </xsl:template>


  <!-- Link to the log -->
  <xsl:template name="link-log">
    <xsl:param name="content"/>
    <xsl:param name="title"/>
    <xsl:param name="class"/>

    <xsl:variable name="id_architecture">
      <xsl:choose>
        <xsl:when test="ancestor::sak:set and local-name(.) = 'deployment'">
          <xsl:value-of select="../@id"/>/<xsl:value-of select="../@architecture"/>/-/<xsl:value-of select="@log"/>
        </xsl:when>
        <xsl:when test="ancestor::sak:set and local-name(.) = 'test'">
          <xsl:value-of select="../../@id"/>/<xsl:value-of select="../../@architecture"/>/-/<xsl:value-of select="@log"/>
        </xsl:when>
        <xsl:when test="local-name(.) = 'test'"><xsl:value-of select="../@id"/>/<xsl:value-of select="../../@architecture"/>/-/reports/<xsl:value-of select="../@component"/>_tests/tests.xml</xsl:when>
        <xsl:when test="local-name(.) = 'deployment'"><xsl:value-of select="@id"/>/<xsl:value-of select="../@architecture"/>/-/reports/<xsl:value-of select="@component"/>_tests/yaimgen.xml</xsl:when>
        <xsl:otherwise><xsl:value-of select="@id"/>/<xsl:value-of select="@architecture"/>/-/reports/index.html</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <a class="{$class}" title="{$title}"
       href="http://etics-repository.cern.ch:8080/repository/reports/id/{$id_architecture}">
      <xsl:copy-of select="$content"/>
    </a>
  </xsl:template>

  <!-- Link to the NMI log if available -->
  <xsl:template name="link-nmi">
    <xsl:param name="content"/>
    <xsl:if test="@nmi and @nmi != 'None'">
      <a href="{@nmi}">
        <xsl:copy-of select="$content"/>
      </a>
    </xsl:if>
  </xsl:template>

  <!-- Put the status in a nice-looking way -->
  <xsl:template name="fancy-status">
    <xsl:variable name="message"/>
    <xsl:variable name="href"/>

    <xsl:variable name="class">
      <xsl:choose>
        <xsl:when test="@status = 'success'">success</xsl:when>
        <xsl:when test="@status = 'external'">external</xsl:when>
        <xsl:otherwise>failed</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="@status = 'not defined'">
        Not defined
      </xsl:when>
      <xsl:when test="@status = 'not executed'">
        -
      </xsl:when>
      <xsl:when test="@status = 'external'">
        External
      </xsl:when>
      <xsl:when test="@status = 'execution error'">
        Execution error
        <xsl:call-template name="link-nmi">
          <xsl:with-param name="content" select="'[?]'"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="link-log">
          <xsl:with-param name="class">
            <xsl:value-of select="$class"/>
          </xsl:with-param>
          <xsl:with-param name="href"    select="$href"/>
          <xsl:with-param name="content">
            <xsl:value-of select="translate(substring(@status, 1, 1), 'asf', 'ASF')"/>
            <xsl:value-of select="substring(@status, 2)"/>

            <xsl:if test="@total > 0">
              <xsl:text> (</xsl:text><xsl:value-of select="@success"/> / <xsl:value-of select="@total"/>)
            </xsl:if>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
