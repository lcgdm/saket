<?xml version="1.0"?>
<!--
##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Álvarez Ayllón <alejandro.alvarez.ayllon@cern.ch>, CERN
#
##############################################################################
-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sak="https://svnweb.cern.ch/trac/lcgutil/saket"
  xmlns:date="date"
  xmlns:xs="http://www.w3.org/2001/XMLSchema-datatypes"
  xmlns:exsl="http://exslt.org/common"
  xmlns="http://www.w3.org/1999/xhtml">

  <xsl:output method="xml"
              indent="yes"
              omit-xml-declaration="no"
              doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
              doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

  <!-- Functions. Note: WebKit based browsers do not support imports and includes,
                        so it is not possible right now to keep some modularity :(
  -->

                         
  <!-- Month name constant -->
  <date:months>
    <date:month abbr="jan">January</date:month>
    <date:month abbr="feb">February</date:month>
    <date:month abbr="mar">March</date:month>
    <date:month abbr="apr">April</date:month>
    <date:month abbr="may">May</date:month>
    <date:month abbr="jun">June</date:month>
    <date:month abbr="jul">July</date:month>
    <date:month abbr="aug">August</date:month>
    <date:month abbr="sep">September</date:month>
    <date:month abbr="oct">October</date:month>
    <date:month abbr="nov">November</date:month>
    <date:month abbr="dec">December</date:month>
  </date:months>

  <!-- Day name constant -->
  <date:days>
    <date:day abbr="mon">Monday</date:day>
    <date:day abbr="tue">Tuesday</date:day>
    <date:day abbr="wed">Wednesday</date:day>
    <date:day abbr="thu">Thursday</date:day>
    <date:day abbr="fri">Friday</date:day>
    <date:day abbr="sat">Saturday</date:day>
    <date:day abbr="sun">Sunday</date:day>
  </date:days>

  <!-- Function to print the month name -->
  <xsl:template name="month-name">
    <xsl:param name="month"/>
    <xsl:value-of select="document('')/*/date:months/date:month[position()=$month]"/>
  </xsl:template>

  <!-- Function to print the day name -->
  <xsl:template name="day-name">
    <xsl:param name="day-of-week"/>
    <xsl:value-of select="document('')/*/date:days/date:day[position()=$day-of-week + 1]"/>
  </xsl:template>

  <!-- Function to replace a string with another -->
  <xsl:template name="replace">
    <xsl:param name="string"/>
    <xsl:param name="pattern"/>
    <xsl:param name="replace"/>
    <xsl:variable name="before" select="substring-before($string, $pattern)"/>
    <xsl:variable name="after" select="substring-after($string, $pattern)"/>
    <xsl:choose>
      <xsl:when test="$after">
        <xsl:value-of select="$before"/>
        <xsl:value-of select="$replace"/>
        <xsl:call-template name="replace">
          <xsl:with-param name="string" select="$after"/>
          <xsl:with-param name="pattern" select="$pattern"/>
          <xsl:with-param name="replace" select="$replace"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Appends a 0 at the beginning if the number has only one digit -->
  <xsl:template name="two-digits">
    <xsl:param name="value"/>
    <xsl:if test="$value &lt; 10 and string-length($value) &lt; 2">0</xsl:if><xsl:value-of select="$value"/>
  </xsl:template>

  <!-- Returns how many days a specific month has.
       The leap year detection is very simple!! After all, it will work this way
       until 2100 -->
  <xsl:template name="n-days-month">
    <xsl:param name="month"/>
    <xsl:param name="year"/>
    <xsl:choose>
      <xsl:when test="$month = 2 and $year mod 4 = 0">29</xsl:when>
      <xsl:when test="$month = 2 and $year mod 4 != 0">28</xsl:when>
      <xsl:when test="$month &lt; 8 and $month mod 2 = 1">31</xsl:when>
      <xsl:when test="$month &gt; 7 and $month mod 2 = 0">31</xsl:when>
      <xsl:otherwise>30</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Returns the number of days elapsed in N full months
       This takes into account the different length of each month!
       e.g: 2 months = January + February = 31 + 28 (or 29)-->
  <xsl:template name="months-in-days">
    <xsl:param name="month"/>
    <xsl:param name="year"/>

    <xsl:choose>
      <xsl:when test="$month = 0">0</xsl:when>
      <xsl:when test="$month = 1">31</xsl:when>
      <xsl:otherwise>
        <xsl:variable name="previous">
          <xsl:call-template name="months-in-days">
            <xsl:with-param name="month" select="$month - 1"/>
            <xsl:with-param name="year" select="$year"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="current">
          <xsl:call-template name="n-days-month">
            <xsl:with-param name="month" select="$month"/>
            <xsl:with-param name="year" select="$year"/>
          </xsl:call-template>
         </xsl:variable>
         <xsl:value-of select="$previous + $current"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Returns which day of the year is day/month/year
       e.g. 1st of February is the 32nd -->
  <xsl:template name="day-of-year">
    <xsl:param name="day"/>
    <xsl:param name="month"/>
    <xsl:param name="year"/>

    <xsl:variable name="months-in-days">
      <xsl:call-template name="months-in-days">
        <xsl:with-param name="month" select="$month - 1"/>
        <xsl:with-param name="year" select="$year"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:value-of select="$months-in-days + $day"/>
  </xsl:template>


  <!-- Returns which day of the week the specific date is.
       It uses 1st of January of 2000 (which was Saturday) as reference.
       0 is Monday, 7 is Sunday -->
  <xsl:template name="day-of-week">
    <xsl:param name="day"/>
    <xsl:param name="month"/>
    <xsl:param name="year"/>

    <!-- Number of days from Sat, 1 Jan 2000 to 1 Jan $year. Again, very simple leap detection! -->
    <xsl:variable name="years-in-days" select="(($year - 2000) * 365) + floor(($year - 2001) div 4)"/>

    <!-- Number of days from 1 Jan $year to 1 $moth $year -->
    <xsl:variable name="day-of-year">
      <xsl:call-template name="day-of-year">
        <xsl:with-param name="day"   select="$day"/>
        <xsl:with-param name="month" select="$month"/>
        <xsl:with-param name="year"  select="$year"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:value-of select="($day-of-year + $years-in-days + 5) mod 7"/>
  </xsl:template>

  <!-- Generates a sequence of xs:int from start to end, both inclusive.
       If the end is smaller than the start, an empty sequence is returned -->
  <xsl:template name="generate-sequence">
    <xsl:param name="start"/>
    <xsl:param name="end"/>
    <xsl:choose>
      <xsl:when test="$start &lt; $end">
        <xsl:call-template name="generate-sequence">
          <xsl:with-param name="start" select="$start"/>
          <xsl:with-param name="end" select="$end - 1"/>
        </xsl:call-template>
        <xs:int><xsl:value-of select="$end"/></xs:int>
      </xsl:when>
      <xsl:when test="$start = $end">
        <xs:int><xsl:value-of select="$end"/></xs:int>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- Document level -->
  <xsl:template match="/sak:summary">
    <html>
      <head>
        <link rel="stylesheet" href="Templates/css/general.css" type="text/css"/>
        <title><xsl:value-of select="@title"/></title>
        <script type="text/javascript" src="Templates/js/slide.js"></script>
      </head>
      <body onload="populateList()">
        <div class="header">
          <h1>
            <xsl:value-of select="@title"/>
            <a href="index.html">
              <img src="Templates/img/table.png" alt="[Table]" title="View as a table"/>
            </a>
            <a href="calendar.html">
              <img src="Templates/img/calendar.png" alt="[Calendar]" title="View as a calendar"/>
            </a>
          </h1>
        </div>

        <xsl:for-each select="sak:year/sak:month">
            <h3>
              <xsl:call-template name="month-name">
                <xsl:with-param name="month" select="@month"/>
              </xsl:call-template>&#160;
              <xsl:value-of select="../@year"/>
            </h3>
          <xsl:apply-templates select="."/>
          </xsl:for-each>

        <div class="footer">
          <p>
            <acronym title="Swiss Army Knife for ETICS Testing">SAKET</acronym> Archive (C) Members of the EGEE Collaboration<br/>
            Alejandro Alvarez &lt;<a href="mailto:aalvarez@cern.ch">aalvarez@cern.ch</a>&gt;
            <a style="float:right" href="https://svnweb.cern.ch/trac/saket/">Documentation</a>
          </p>
        </div>
      </body>
    </html>
  </xsl:template>

  <!-- Prints a cell in the table, appending the pop-up menu if there is
       available logs -->
  <xsl:template name="put-td">
    <xsl:param name="day"/>
    <xsl:param name="month-tree"/>
    <td>
      <xsl:choose>
        <!-- There is a log entry at least -->
        <xsl:when test="$month-tree/sak:day[@day = $day]">
          <xsl:attribute name="class">with-log</xsl:attribute>
          <!-- To improve readibility, use variables to hold values -->
          <xsl:variable name="curday" select="$month-tree/sak:day[@day = $day]"/>
          <xsl:variable name="year" select="$curday/../../@year"/>
          <xsl:variable name="month">
            <xsl:call-template name="two-digits">
              <xsl:with-param name="value" select="$curday/../@month"/>
            </xsl:call-template>
          </xsl:variable>
          <xsl:variable name="day-two">
            <xsl:call-template name="two-digits">
              <xsl:with-param name="value" select="$curday/@day"/>
            </xsl:call-template>
          </xsl:variable>
          <!-- ID of the pop-up menu -->
          <xsl:variable name="div-id">ll_<xsl:value-of select="$year"/>-<xsl:value-of select="$month"/>-<xsl:value-of select="$day-two"/></xsl:variable>

          <span onclick="javascript:toggleView('{$div-id}')"><xsl:value-of select="$day"/></span>

          <!-- Menu itself -->
          <div class="log-dialog" id="{$div-id}">
            <ol>
              <xsl:for-each select="$month-tree/sak:day[@day = $day]/sak:entry">
                <xsl:variable name="time-dash">
                  <xsl:call-template name="replace">
                    <xsl:with-param name="string" select="@time"/>
                    <xsl:with-param name="pattern" select="':'"/>
                    <xsl:with-param name="replace" select="'-'"/>
                  </xsl:call-template>
                </xsl:variable>

                <!-- Report location -->
                <xsl:variable name="report-location"><xsl:value-of select="$year"/>-<xsl:value-of select="$month"/>-<xsl:value-of select="$day-two"/>_<xsl:value-of select="$time-dash"/></xsl:variable>
                <li>
                  <a href="{$report-location}"><xsl:value-of select="@time"/></a>
                </li>
              </xsl:for-each>
            </ol>
          </div>
        </xsl:when>
        <!-- No entry, so just the day -->
        <xsl:otherwise><xsl:value-of select="$day"/></xsl:otherwise>
       </xsl:choose>
    </td>
  </xsl:template>

  <!-- Per month -->
  <xsl:template match="sak:month">
    <xsl:variable name="sak-month" select="."/>
    <table class="calendar">
      <thead>
        <tr>
          <th>Monday</th>
          <th>Tuesday</th>
          <th>Wednesday</th>
          <th>Thursday</th>
          <th>Friday</th>
          <th>Saturday</th>
          <th>Sunday</th>
        </tr>
      </thead>
      <tbody>
        <!-- Which day of the week (Monday to Sunday) does this month start? -->
        <xsl:variable name="start-dweek">
          <xsl:call-template name="day-of-week">
            <xsl:with-param name="day"   select="'1'"/>
            <xsl:with-param name="month" select="@month"/>
            <xsl:with-param name="year"  select="../@year"/>
          </xsl:call-template>
        </xsl:variable>
        <!-- How many days in this month ? -->
        <xsl:variable name="n-days-month">
          <xsl:call-template name="n-days-month">
            <xsl:with-param name="month" select="@month"/>
            <xsl:with-param name="year"  select="../@year"/>
          </xsl:call-template>
        </xsl:variable>

        <!-- First week is kind of special -->
        <tr>
          <!-- Fill "empty" days -->
          <xsl:variable name="empty-start">
            <xsl:call-template name="generate-sequence">
              <xsl:with-param name="start" select="1"/>
              <xsl:with-param name="end" select="$start-dweek"/>
            </xsl:call-template>
          </xsl:variable>
          <xsl:for-each select="exsl:node-set($empty-start)/xs:int">
            <td class="noday">&#160;</td>
          </xsl:for-each>
          <!-- Rest of the week -->
          <xsl:variable name="days">
            <xsl:call-template name="generate-sequence">
              <xsl:with-param name="start" select="1"/>
              <xsl:with-param name="end" select="7 - $start-dweek"/>
            </xsl:call-template>
          </xsl:variable>
          <xsl:for-each select="exsl:node-set($days)/xs:int">
            <xsl:call-template name="put-td">
              <xsl:with-param name="month-tree" select="$sak-month"/>
              <xsl:with-param name="day"        select="."/>
            </xsl:call-template>
          </xsl:for-each>
        </tr>
        <!-- Full weeks -->
        <xsl:variable name="n-full-weeks" select="floor(($n-days-month - (7 - $start-dweek)) div 7)"/>
        <xsl:variable name="full-weeks">
          <xsl:call-template name="generate-sequence">
            <xsl:with-param name="start" select="1"/>
            <xsl:with-param name="end" select="$n-full-weeks"/>
          </xsl:call-template>
        </xsl:variable>

        <!-- For each week that is complete in this month -->
        <xsl:for-each select="exsl:node-set($full-weeks)/xs:int">
          <tr>
            <xsl:variable name="cur-week" select="."/>
            <xsl:variable name="week-days">
              <xsl:call-template name="generate-sequence">
                <xsl:with-param name="start" select="$cur-week * 7 - $start-dweek + 1"/>
                <xsl:with-param name="end" select="(($cur-week + 1) * 7) - $start-dweek"/>
              </xsl:call-template>
            </xsl:variable>
            <!-- For each day in said week -->
            <xsl:for-each select="exsl:node-set($week-days)/xs:int">
              <xsl:call-template name="put-td">
                <xsl:with-param name="month-tree" select="$sak-month"/>
                <xsl:with-param name="day"        select="."/>
              </xsl:call-template>
            </xsl:for-each>
          </tr>
        </xsl:for-each>

        <!-- Last week is also special, it may be partially "empty" -->
        <xsl:if test="($n-full-weeks + 1) * 7 - $start-dweek &lt; $n-days-month">
          <tr>
            <!-- Remaining days of the month -->
            <xsl:variable name="days-remaining" select="31 - ($n-full-weeks * 7) - $start-dweek"/>
            <xsl:variable name="week-days">
              <xsl:call-template name="generate-sequence">
                <xsl:with-param name="start" select="(($n-full-weeks + 1) * 7) - $start-dweek + 1"/>
                <xsl:with-param name="end" select="$n-days-month"/>
              </xsl:call-template>
            </xsl:variable>
            <!-- Actual days -->
            <xsl:for-each select="exsl:node-set($week-days)/xs:int">
              <xsl:call-template name="put-td">
                <xsl:with-param name="month-tree" select="$sak-month"/>
                <xsl:with-param name="day"        select="."/>
              </xsl:call-template>
            </xsl:for-each>
            <!-- Fill empty daus -->
            <xsl:variable name="remaining-days">
              <xsl:call-template name="generate-sequence">
                <xsl:with-param name="start" select="1"/>
                <xsl:with-param name="end" select="7 - count(exsl:node-set($week-days)/xs:int)"/>
              </xsl:call-template>
            </xsl:variable>
            <xsl:for-each select="exsl:node-set($remaining-days)/xs:int">
              <td class="noday">&#160;</td>
            </xsl:for-each>
          </tr>
        </xsl:if>
      </tbody>
   </table>
  </xsl:template>

</xsl:stylesheet>
