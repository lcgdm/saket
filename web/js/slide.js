var popupList = new Array();

var slide_time      = 5;
var slide_aniLen    = 300;
var slide_height    = new Array();
var slide_direction = new Array();
var slide_timerID   = new Array();
var slide_startTime = new Array();
var slide_obj       = new Array();
var slide_moving    = false;

function populateList()
{
  var doc = document.getElementsByTagName('div')

  for(var i = 0; i < doc.length; i++) {
    if(doc[i].id.match('ll_')) {
      popupList.push(doc[i])
      
      doc[i].style.height = doc[i].offsetHeight + 'px';
      doc[i].style.visibility = 'hidden';
    }
  }
}

function slideStart(id)
{
  if(slide_timerID[id])
    return;

  slide_moving = true;

  slide_obj[id] = document.getElementById(id);

  slide_height[id]    = parseInt(slide_obj[id].style.height);
  slide_startTime[id] = (new Date()).getTime();

  if(slide_direction[id] == 'down')
    slide_obj[id].style.height = '1px';
  slide_obj[id].style.visibility = 'visible';

  slide_timerID[id] = setInterval('slideTick(\'' + id + '\');', slide_time);
}

function slideEnd(id)
{
  clearInterval(slide_timerID[id]);

  if(slide_direction[id] == 'up')
    slide_obj[id].style.visibility = 'hidden';

  slide_obj[id].style.height = slide_height[id] + 'px';

  delete(slide_obj[id]);
  delete(slide_timerID[id]);
  delete(slide_height[id]);
  delete(slide_startTime[id]);
  delete(slide_direction[id]);

  slide_moving = false;
}

function slideTick(id)
{
  var elapsed = (new Date()).getTime() - slide_startTime[id];

  if(elapsed > slide_aniLen)
    slideEnd(id)
  else {
    var d = Math.round(elapsed / slide_aniLen * slide_height[id]);
    var o = elapsed / slide_aniLen;
    if(slide_direction[id] == 'up') {
      d = slide_height[id] - d;
      o = 1 - o;
    }

    slide_obj[id].style.height = d + 'px';
    slide_obj[id].style.opacity = o;
  }
}

function hideAll()
{
  if(! slide_moving ) {
    for(var i = 0; i < popupList.length; i++) {
      var id = popupList[i].id;
      if(popupList[i].style.visibility != 'hidden') {
        slide_direction[id] ='up';
        slideStart(id);
      }
    }
  }
}

function toggleView(id)
{
  var obj = document.getElementById(id);
  if(! slide_moving ) {
    if(obj.style.visibility != 'visible') {
      hideAll()
      slide_direction[id] = 'down';
      slideStart(id);
    } else {
      slide_direction[id] = 'up';
      slideStart(id);
    }
  }
}
