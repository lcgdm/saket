#! /usr/bin/python
##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# This script will use ETICS CLI to submit the builds of all, or some, of the
# subsystems in the configuration script
#
##############################################################################

import sys
from Saket.Environment import setupEnvironment

##############################################################################
# Executing this module from the command line
##############################################################################
if __name__ == "__main__":
    try:
        setupEnvironment(sys.argv[1:])

        from Saket.Dispatchers import SaketBuilder
        from Saket.Logger      import Logger

        builder = SaketBuilder()
        Logger.info("Submitting the builds. This can take a while...")
        builder.build()
        Logger.info("All the builds submitted!")
        sys.exit(0)
    except KeyboardInterrupt:
        print '\nExecution interrupted by the user!'
        sys.exit(1)
