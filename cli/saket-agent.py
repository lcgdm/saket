#! /usr/bin/python
##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# This script executes in a synchronous maner the submission, test and report
# retrieval
#
##############################################################################

import sys
import time
from datetime          import datetime

from Saket.Environment import setupEnvironment, Environment, doubleFork

# Main function
def main():
  from Saket.Dispatchers import SaketBuilder, SaketTester, SaketReporter, SaketNotifier, SaketStatus
  from Saket.Logger      import Logger

  env = Environment()
  env.startTime = datetime.now()

  Logger.info("Timeout is %s" % str(env.AGENT_TIMEOUT))
  if env.FORCE_TEST:
    Logger.info("The tests will be executed even if the build fails!")

  # Submit the builds
  Logger.info("Submitting the builds (this will take a while)...")
  tasks   = SaketBuilder().build()
  running = len(tasks)
  Logger.newline()

  # Now, double fork and wait in the background
  if env.DOUBLE_FORK:
    doubleFork(env.OUTPUT)

  # Wait
  Logger.info("Build commands sent. Now waiting...")
  status = SaketStatus()
  # While there is something
  finished = []
  while running > 0:
    # Sleep
    Logger.info("Sleeping for %d seconds..." % env.AGENT_WAIT)
    time.sleep(env.AGENT_WAIT)
    # Update status
    status.update(tasks)
    changed = False

    for btask in tasks:
      if btask.id not in finished and btask.finished():
        finished.append(btask.id)
        running -= 1
        Logger.info("Build %s finished with '%s'" % (btask.id, btask.status))
        changed = True
        # Submit the tests
        if btask.passed() or env.FORCE_TEST:
          if len(btask) > 0:
            Logger.info("Submitting tests for %s" % btask.id)
            SaketTester(btask).test()
            running += len(btask)
          else:
            Logger.info("There are no tests associated to %s" % btask.id)
        else:
          Logger.info("The build was not successful, so no tests will be executed")

      elif btask.id in finished and (btask.passed() or env.FORCE_TEST):
        # Check the tests
        for ttask in btask:
          if ttask.id not in finished and ttask.finished():
            finished.append(ttask.id)
            running -= 1
            changed = True
            Logger.info("Test %s finished" % (ttask.id))

    # No changes?
    if not changed:
      Logger.info("No changes in the status. %i pending." % running)

    # If we have been waiting for too long, just dump and finish
    if datetime.now() - env.startTime >= env.AGENT_TIMEOUT:
      Logger.warning("Timeout reached!. Aborting execution...")
      running = 0

  # Report
  env.endTime = datetime.now()
  Logger.info("Retrieving the logs...")
  report = SaketReporter(tasks).report()
  report.startTime = env.startTime
  report.endTime   = env.endTime
  Logger.info("Calling the printers...")
  notifier = SaketNotifier()
  notifier.notify(report)
  Logger.info("Success!!")
  return 0

##############################################################################
# Executing this module from the command line
##############################################################################
if __name__ == "__main__":
    try:
        setupEnvironment(sys.argv[1:])
        sys.exit(main())
    except KeyboardInterrupt:
        print '\nExecution interrupted by the user!'
        sys.exit(1)
