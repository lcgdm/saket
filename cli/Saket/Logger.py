##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Prints logging information
#
##############################################################################

import traceback
import sys
from Environment import Environment

class Logger:
  """
  Static class for debug/error/info/warning messages
  """
  env  = Environment()
  _mute = False

  @classmethod
  def mute(cls):
    cls._mute = True

  @classmethod
  def debug(cls, msg):
    """
    Prints a line in stderr if DEBUG is set to True

    @param str The string to print
    """
    if cls.env.DEBUG and not cls._mute:
      sys.stderr.write("[DEBUG] %s\n" % str(msg).replace('\n', '\n[DEBUG] '))
      if isinstance(msg, Exception):
        traceback.print_exc(file = sys.stderr)
      sys.stderr.flush()

  @classmethod
  def error(cls, msg):
    """
    Prints a line in stderr of an error

    @param msg The string to print, or an exception
    """
    if not cls._mute:
      sys.stderr.write("[ERROR] %s\n" % str(msg).replace('\n', '\n[ERROR] '))
      if isinstance(msg, Exception) and cls.env.DEBUG:
        traceback.print_exc(file=sys.stderr)
      sys.stderr.flush()

  @classmethod
  def warning(cls, msg):
    """
    Prints a line in stderr of a warning

    @param msg The string to print, or an exception
    """
    if not cls._mute:
      sys.stderr.write("[WARN]  %s\n" % str(msg).replace('\n', '\n[WARN]  '))
      if isinstance(msg, Exception) and cls.env.DEBUG:
        traceback.print_exc(file=sys.stderr)
      sys.stderr.flush()

  @classmethod
  def info(cls, str):
    """
    Prints a line in stdout of a message

    @param str The string to print
    """
    if not cls._mute:
      sys.stderr.write("[INFO]  %s\n" % str.replace('\n', '\n[INFO]  '))
      sys.stderr.flush()

  @classmethod
  def newline(cls):
    """
    Prints a new line
    """
    if not cls._mute:
      sys.stdout.write("\n")
