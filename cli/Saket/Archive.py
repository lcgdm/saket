#!/usr/bin/python
##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Class to process the archive and model it
#
##############################################################################

import os
import os.path
import sys
import time
import xml.dom.minidom

from datetime import datetime
from Logger import Logger

# Entry in the archive
class ArchiveEntry:
  """
  This class represents an entry in the archive (this is, a Report)
  """

  NAMESPACE='https://svnweb.cern.ch/trac/lcgutil/saket'

  def __init__(self, tm, location):
    """
    Constructor

    @param tm       An instance of datetime identifying the report
    @param location The parent folder (where all the reports are stored)
    """
    self.time = tm
    # Statuses
    build      = [0,0]
    deployment = [0,0]
    test       = [0,0]

    # Defaults
    self.duration   = self.eduration = None
    self.build      = (None, None)
    self.deployment = (None, None)
    self.test       = (None, None)
    
    # Open report
    report = os.path.join(location, self.index())
    if os.access(report, os.R_OK):
      try:
        doc    = xml.dom.minidom.parse(open(report, 'r'))
        root   = doc.getElementsByTagNameNS(self.NAMESPACE, 'report')[0]
        # Duration
        duration  = root.getAttribute('duration')
        eduration = root.getAttribute('eduration')
      except:
        exc_class, exc, tb = sys.exc_info()
        raise IOError, "Could not parse %s" % report, tb

      if duration:
        self.duration = float(duration)
      if eduration:
        self.eduration = float(eduration)

      # Submitter and configuration
      self.submitter     = root.getAttribute('submitter')
      self.configuration = root.getAttribute('configuration')

      # Builds
      builds = root.getElementsByTagNameNS(self.NAMESPACE, 'build')
      for blog in builds:
        build[1] += 1
        if blog.getAttribute('status') in ['success', 'external']:
          build[0] += 1

      # Deployment
      deploys = root.getElementsByTagNameNS(self.NAMESPACE, 'deployment')
      for dlog in deploys:
        deployment[1] += 1
        if dlog.getAttribute('status') == 'success':
          deployment[0] += 1

      # Tests
      tests = root.getElementsByTagNameNS(self.NAMESPACE, 'test')
      for tlog in tests:
        status = tlog.getAttribute('status')
        if status != 'not defined':
          ntests  = int(tlog.getAttribute('total'))
          npassed = int(tlog.getAttribute('success'))
          
          test[1] += ntests
          test[0] += npassed

      # Set tuples
      self.build      = (build[0], build[1])
      self.deployment = (deployment[0], deployment[1])
      self.test       = (test[0], test[1])
    else:
      raise IOError("%s is not accesible" % report)

  def index(self):
    """
    Returns the path to the report.xml
    """
    return "%s/report.xml" % self.time.strftime("%Y-%m-%d_%H-%M-%S")

  def __str__(self):
    """
    String representation of the entry
    """
    return self.time.strftime("%d-%m-%Y")

# Class to handle the archive
class Archive:
  """
  Models the whole archive. Can be iterated.
  """

  def __init__(self, location):
    """
    Constructor

    @param location The folder where all the reports are stored
    """

    self.entries = dict()

    # Iterate through the directory and add the entries
    for dir in os.listdir(location):
      if os.path.isdir(os.path.join(location, dir)):
        try:
          entry = ArchiveEntry(datetime(*(time.strptime(dir, '%Y-%m-%d_%H-%M-%S')[0:6])), location)
          key   = entry.time

          # By year
          if not key.year in self.entries:
            self.entries[key.year] = dict()

          # By month
          if not key.month in self.entries[key.year]:
            self.entries[key.year][key.month] = dict()

          # And by day
          if not key.day in self.entries[key.year][key.month]:
            self.entries[key.year][key.month][key.day] = []

          # Add
          self.entries[key.year][key.month][key.day].append(entry)

        except ValueError:
          Logger.warning("%s could not be added: Format invalid" % dir)
        except IOError:
          Logger.warning("%s does not contain a file called 'report.xml'!!" % dir)
        except Exception:
          raise


  def __iter__(self):
    return self.entries.__iter__()

  def __getitem__(self, key):
    return self.entries[key]

  def items(self):
    return self.entries.items()

  def __str__(self):
    return "%s entries" % len(self.entries)
