##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Classes that actually executes the orders
#
##############################################################################

import time
import Saket.ETICS
import Saket.Utils
import WebClient.BuildSystemWebClient
from Saket             import Tasks, Multinode
from Saket.Logger      import Logger
from Saket.Environment import Environment
from Saket.Report      import Report
from Saket.URLBuilder  import URLBuilder


class ETICSClientSingleton:
  """
  Handles a unique instance of the client
  """

  _instance = None

  @classmethod
  def getClient(cls):
    if not cls._instance:
      cls._instance = WebClient.BuildSystemWebClient.BuildSystemService()
    return cls._instance


class SaketBuilder:
  """
  Class to submit build commands
  """

  def __init__(self, buildTasks = None):
    """
    Constructor

    @param buildTasks (Optional) A list of BuildTasks that will be submitted
    """
    self.client = ETICSClientSingleton.getClient()
    self.env    = Environment()
    
    if buildTasks:
      self.buildTasks = buildTasks
    else:
      self.buildTasks = Tasks.generateTaskTree(self.env.PROJECTS)

  def build(self):
    """
    Executes the build

    @return The list of BuildTasks assigned when created
    """

    externalId = 0
    for task in self.buildTasks:
      if not task.enabled:
        continue

      if task.configuration is not None:
        Logger.info("Sending command for %s..." % task.name)

        checkoutOptions  = "%s --project-config \"%s\" --platform \"%s\"" % (task.checkoutExtra, task.projectConfiguration, task.architecture)
        buildTestOptions = "%s --platform \"%s\"" % (task.buildExtra, task.architecture)
        if task.configuration:
          checkoutOptions  += " --config \"%s\"" % task.configuration
          buildTestOptions += " --config \"%s\"" % task.configuration

        reqs = "client_req_volatile=%s;client_req_urlname=%s;client_req_pmrep=ALL;" % (task.name, task.name)
        if task.eticsClient:
          reqs += "client_req_client_release=%s;" % task.eticsClient

        try:
          self.env.saveAndSet(task.environ)
          jobStatus = self.client.submit(
                             projectName    = task.project,
                             moduleName     = task.module,
                             configName     = task.configuration,
                             submissionType = 'build',
                             platforms      = task.architecture,
                             requirements   = reqs,
                             checkoutOptions  = checkoutOptions,
                             buildTestOptions = buildTestOptions,
                             )
          task.id = jobStatus._id
          Logger.newline()
          Logger.info("Job submitted with id %s" % jobStatus._id)
          self.env.restore()
        except ValueError, err:
          Logger.error(err)
      elif task.externalRepo is not None:
        Logger.info("Using external repository for %s. Not building." % task.name)
        task.id     = 'external-%d' % externalId
        externalId += 1
        task.status = 'external'
      else:
        Logger.error("No build configuration, neither an external repository provided for %s. Ignoring." % task.name)
        
    return self.buildTasks


class SaketTester:
  """
  Class to submit the tests
  """

  def __init__(self, testTasks = None):
    """
    Constructor

    @param testTasks (Optional) A list of BuildTasks that will be submitted.
                     If not specified, it will be populated from the configuration
    """
    self.client        = ETICSClientSingleton.getClient()
    self.env           = Environment()
    self.urlBuilder    = URLBuilder()

    if testTasks:
      self.testTasks = testTasks
    else:
      buildTasks = Tasks.generateTaskTree(self.env.PROJECTS)
      self.testTasks = Tasks.TaskList()

      for b in buildTasks:
        for t in b:
          self.testTasks.append(t)

  def test(self):
    """
    Executes the test

    @return The test task list passed to the constructor, or the automatically
            populated one
    """

    # For each subsystem in the configuration
    for task in self.testTasks:
      if task.enabled:
        repo   = self.urlBuilder.getRepoURL(task)

        # Sleep
        time.sleep(60)

        # Test command
        opt  = "-p component=\"%s\" "         % (task.label)
        opt += "-p repository=\"%s\" "        % (repo)

        opt += " ".join(["-p %s=\"%s\"" % (key, val) for (key, val) in task.test_properties.iteritems()])

        # Test
        Logger.info("Sending command for %s..." % task.name)

        reqs = "runasroot;client_req_volatile=%s;client_req_urlname=%s;" % (task.name, task.name)
        if task.eticsClient:
          reqs += "client_req_client_release=%s;" % task.eticsClient

        try:
          self.env.saveAndSet(task.environ)
          jobStatus = self.client.submit(
                         projectName    = task.project,
                         moduleName     = task.module,
                         configName     = task.configuration,
                         submissionType = 'test',
                         platforms      = task.architecture,
                         requirements   = reqs,
                         checkoutOptions  = "--verbose --project-config %s --config %s %s" % (task.projectConfiguration,
                                                                                                            task.configuration,
                                                                                                            opt),
                         buildTestOptions = "--verbose --config %s %s" % (task.configuration, opt)
                         )

          task.id = jobStatus._id
          Logger.newline()
          Logger.info("Job submitted with id %s" % jobStatus._id)
          self.env.restore()
        except ValueError, err:
          Logger.error(err)

    return self.testTasks


class SaketReporter:
  """
  Class to retrieve the report
  """

  def __init__(self, tasksList = None):
    """
    Constructor

    @param tasksList (Optional) The tasksList to use. It should be a first-level
                     list.
    """
    self.multinode   = Multinode.Multinode()
    self.env         = Environment()
    self.urlBuilder  = URLBuilder()

    if tasksList:
      self.tasksList = tasksList
    else:
      self.tasksList = Tasks.generateTaskTree(self.env.PROJECTS)

  def report(self):
    """
    Executes the action of retrieving the logs

    @return An instance of Report with all the retrieved information
    """

    report = Report()
    self.env.reportTime = report.time

    # For each task
    tasks = []
    tasks.extend(self.tasksList)
    for task in tasks:
      if task.enabled:
        if isinstance(task, Tasks.BuildTask):
          # Build tasks
          buildLogURL = self.urlBuilder.getBuildLogURL(task)
          eticsLogURL = self.urlBuilder.getEticsLogURL(task)

          try:
            if task.configuration is not None:
              parserId = task.parser['BUILD_LOG_PARSER']
            else:
              parserId = 'Saket.Parsers.ExternalParser.ExternalParser'

            Logger.debug('Using parser %s for the build log' % parserId)
            bParser   = Saket.Utils.getClass(parserId)()
            buildLog  = bParser.parse(buildLogURL)

            buildLog.URL  = eticsLogURL
            buildLog.type = 'build'
            buildLog.fromTask(task)

            report.attachLog(buildLog)
            Logger.debug("Build log %s added to the report" % buildLog.id)

            # If success, we need to process the children
            if buildLog.passed() or self.env.FORCE_TEST:
              for ttask in task:
                tarUrl    = self.urlBuilder.getTarUrl(ttask)
                tupleList = self.multinode.process(tarUrl, ttask.parser)
                for dLog, tLog in tupleList:
                  if dLog.configuration in ttask.jobs:
                    dLog.setNMI(ttask.jobs[dLog.configuration].executionID)
                  elif None in ttask.jobs:
                    dLog.setNMI(ttask.jobs[None].executionID) # Single node
                report.attachLog(buildLog, ttask.label, ttask.id, tupleList)
                Logger.debug("Test set %s added to the report" % ttask.label)
            else:
              Logger.warning("Attaching empty deployment log as the build failed")
              for ttask in task:
                report.attachLog(buildLog, ttask.label)
            
          except Exception, e:
            Logger.error(e)
            continue
        else:
          Logger.warning("Unknown type in the task list: %s" % type(task))
      # end task.enabled
    # end for
    return report

class SaketNotifier:
  """
  Class to notify the printers
  """

  def __init__(self):
    """
    Constructor
    """
    self.env = Environment()

    # List of  printers
    self.printers = []
    for printerStr in self.env.PRINTERS:
      Logger.debug("Registering printer %s" % printerStr)
      aClass = Saket.Utils.getClass(printerStr)
      printer = aClass()
      self.printers.append(printer)

  def notify(self, report):
    """
    Uses the configured printers to submit the information

    @param report A Report instance that will be passed to the printers.
    """
    for printer in self.printers:
      Logger.debug("Using the printer %s" % printer.name())

      try:
        printer.write(report)
      except Exception, e:
        Logger.error(Exception("The printer %s can not be used: %s" % (printer.name(), e)))


class SaketStatus:
  """
  Class to retrieve the status of a bunch of IDs
  """

  def __init__(self):
    """
    Constructor
    """
    self.env = Environment()
    self.client = ETICSClientSingleton.getClient()

  def update(self, tasks):
    """
    Updates the status of a list of IDs

    @param tasks  A list/tree of tasks
    """

    for btask in tasks:
      if btask.id and not btask.finished():
        eticsStatus       = self.client.submitStatus(btask.id)[0]
        btask.status      = eticsStatus._status.lower()
        btask.executionID = eticsStatus._executionID
      if isinstance(btask, Tasks.BuildTask):
        for ttask in btask:

          if ttask.id and not ttask.finished():
            eticsStatus       = self.client.submitStatus(ttask.id)[0]

            if eticsStatus._status.startswith('TEST:'):
              # Multinode
              finished = True
              for node in eticsStatus._status.split(':')[1:]:
                (config, platform, subid, executionID, status) = node.split('@')
                if config not in ttask.jobs:
                  ttask.jobs[config] = Tasks.Job(config, platform, subid, executionID)
                ttask.jobs[config].status = status.lower()
                finished = finished and ttask.jobs[config].finished()
            else:
              # Single node
              if None not in ttask.jobs:
                ttask.jobs[None] = Tasks.Job(None, eticsStatus._platformName, ttask.id, eticsStatus._executionID)
              ttask.jobs[None].status  = eticsStatus._status.lower()
              finished = ttask.jobs[None].finished()

            # Mark as finished if needed
            if finished:
              ttask.status = 'Finished'

            
