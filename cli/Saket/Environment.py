##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# A class that wrapps the working environment
#
##############################################################################

import imp
import os
import os.path
import sys

class Environment:
  """
  A class that wraps the working environment
  """

  values = dict()
  config = None
  stack  = []

  @classmethod
  def setConfigurationFile(cls, path):
    """
    Sets the configuration file

    @param path The configuration file
    """
    from Logger import Logger
    
    path = os.path.abspath(path)
    try:
      sys.path.append(os.path.dirname(path))
      Environment.config = imp.load_source('SAKETConfiguration', path)
    except Exception, e:
      raise Exception("The configuration file %s can not be loaded: %s" % (path, e))

    Environment.values['CONFIGURATION'] = Environment.config.__name__
    Logger.debug("Configuration file is %s" % path)

    if 'ENVIRON' in Environment.values:
      os.environ.update(Environment.values['ENVIRON'])

  def __setattr__(self, attr, value):
    """
    Sets a value

    @param attr  The identifier of the value
    @param value The value
    """
    Environment.values[attr] = value

  def __getattr__(self, attr):
    """
    Returns a value from the set values, OS environment or configuration (in that order).
    None if does not exist.

    @param attr  The identifier of the value
    @param value The value, or None id the attribute is not defined
    """

    if Environment.values.has_key(attr):
      return Environment.values[attr]
    elif os.environ.has_key(attr):
      return os.environ[attr]
    elif Environment.config and attr in Environment.config.__dict__:
      return getattr(Environment.config, attr, None)
    else:
      return None

  def __getitem__(self, key):
    """
    Alias for __getattr__
    """
    return self.__getattr__(key)

  def saveAndSet(self, new):
    """
    Updates the environment with the new dictionary, and saves the old
    values

    @param new The dictionary that will be used to update the values
    """
    from Logger import Logger
    
    oldVals = dict()
    if new:
      for key, val in new.iteritems():
        if key in os.environ:
          oldVals[key] = os.environ[key]
        else:
          oldVals[key] = None
        os.environ[key] = val
        Logger.debug("Environment %s = %s" % (key, val))
    Environment.stack.append(oldVals)

  def restore(self):
    """
    Pops from the stack the previous environment status
    """
    from Logger import Logger
    
    oldVals = Environment.stack.pop()
    for key, val in oldVals.iteritems():
      if val:
        os.environ[key] = val
      else:
        del os.environ[key]
      Logger.debug("Environment %s restored to %s" % (key, val))
    del oldVals

# Usage
def usage():
  print(
"""\
%s [-h|--help] [-c|--config file] [-v|--verbose]

\t-h, --help         Show this help
\t-c, --config FILE  Use this configuration file
\t-f, --force-test   In the agent, force the execution of the tests even when the build fails
\t-v, --verbose      Verbose mode
\t    --task-tree    Show the task tree and exit
\t    --fork FILE    Double forks. The output will be written to FILE.

Report bugs to lcgutil-devel@cern.ch\
""" % sys.argv[0]
      )

# Load configuration file
def setupEnvironment(args):
  import getopt
  try:
    # Get arguments
    opts, args = getopt.getopt(args, "hc:vf", ["help", "config=", "verbose", "force-test", "task-tree", "fork="])

    for opt, arg in opts:
      if opt in ("-h", "--help"):
        usage()
        sys.exit(0)
      elif opt in ("-c", "--config"):
        Environment.setConfigurationFile(arg)
      elif opt in ("-v", "--verbose"):
        Environment.DEBUG = True
      elif opt in ("-f", "--force-test"):
        Environment.FORCE_TEST = True
      elif opt in ("--task-tree"):
        Environment.JUST_TASK_TREE = True
        Environment.DEBUG          = True
      elif opt in ("--fork"):
        Environment.DOUBLE_FORK    = True
        Environment.OUTPUT         = arg

    # Default configuration file
    if not Environment.config:
      Environment.setConfigurationFile(os.path.dirname(__file__) + '/../Config/default.py')

    # Just show the task tree
    env = Environment()
    if env.JUST_TASK_TREE:
      import Tasks
      Tasks.generateTaskTree(env.PROJECTS)
      sys.exit(0)

  except getopt.GetoptError, error:
    print >>sys.stderr, error, '\n'
    usage()
    sys.exit(2)

# Double fork
def doubleFork(log):
  # First fork
  try:
    pid = os.fork()
    if pid > 0:
      sys.exit(0)
  except OSError, e:
    print >>sys.stderr, "Fork #1 failed"
    sys.exit(-1)

  # Decouple
  os.chdir("/tmp/")
  os.setsid()

  # Second fork
  try:
    pid = os.fork()
    if pid > 0:
      print "\nSAKET: Background process with ID #%d\n" % pid
      sys.exit(0)
  except OSError, e:
    print >>sys.stderr, "fork #2 failed"
    sys.exit(-1)

  # Point output to the log file
  try:
    out   = open(log, 'a')
    sys.stdout = out
    sys.stderr = out
    sys.stdin  = None
  except Exception, e:
    print >>sys.stderr, str(e)

  return
