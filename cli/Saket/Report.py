##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# This file defines classes that abstract the information about the logs and
# the whole report
#
##############################################################################

from datetime import datetime

# Log class
class Log:
  """@brief A class to store information about logs

  This class is a generic model for logs, storing information as location,
  architecture, metapackage, status (success or failure), log location, and other statistics
  """

  def __init__(self, task = None):
    """
    Constructor

    @param task If present, the relevant attributes will be called
    """
    self.id      = None
    self.URL     = None
    self.status  = None
    self.total   = 0
    self.success = 0
    self.failed  = 0
    self.startTime = None
    self.endTime   = None
    self.duration  = None

    self.architecture = None

    self.project   = None
    self.subsystem = None
    self.component = None

    self.configuration = None
    self.projectConfiguration = None

    if task:
      self.fromTask(task)

    self.nmi    = None
    self.module = None

    self.log    = None

  def setNMI(self, nmi):
    """
    Sets the NMI link
    """
    if self.startTime is not None:
      self.nmi = "http://etics.cern.ch/rundir/tomcat4/%i/%02i/%s" % (self.startTime.year,
                                                                     self.startTime.month,
                                                                     nmi)
    else:
      dt = datetime.now()
      self.nmi = "http://etics.cern.ch/rundir/tomcat4/%i/%02i/%s" % (dt.year,
                                                                     dt.month,
                                                                     nmi)

  def fromTask(self, task):
    """
    Copy the relevant attributes from the task

    @param task Where to copy the values from
    """
    if 'parent' in task.__dict__:
      self.parentId  = task.parent.id
      
    self.architecture  = task.architecture
    self.module        = task.module
    self.project       = task.project
    self.subsystem     = task.subsystem
    self.component     = task.component
    self.module        = task.module
    self.id            = task.id

    self.configuration        = task.configuration
    self.projectConfiguration = task.projectConfiguration

    self.externalRepo         = task.externalRepo

    if task.executionID and self.startTime:
      self.setNMI(task.executionID)

  def percentSuccess(self):
    """
    Returns the percentage of success as an integer, or 0 if there are no items executed

    @return An integer
    """
    if self.total > 0:
      return round((100.0 * self.success) / self.total)
    else:
      return 0

  def passed(self):
    """
    Return True if the build/deployment passed
    """
    return self.status in ['success', 'external']

  def __str__(self):
    """
    Generates a string that represents this object. This method is called by
    Python when a conversion to string is implicit (i.e. print Log())

    @return A String
    """
    return "ID: %s\nMetapackage: %s\nArchitecture: %s\nURL: %s\nStatus: %s\nStart: %s\nEnd: %s\nDuration: %s\nTotal: %i\nSuccess: %i\nFailed: %i" % \
     (self.id, self.metapackage, self.architecture, self.URL, self.status,\
      self.startTime, self.endTime, self.duration,\
      self.total, self.success, self.failed)

# Node set entry
# Useful for multinode, although used for everything
class NodeSet(list):
  """
  This class is just a container for all the sub-deployment and test for a
  specific scenario
  """

  def __init__(self, name, id, initial):
    """
    Constructor
    """
    if initial is not None:
      list.__init__(self, initial)
    else:
      list.__init__(self)
    self.name = name
    self.id   = id

  def get_architecture(self):
    """
    Returns the platform of the set: multiplatform is at least one different
    """
    architecture = None
    for (d, t) in self:
      if architecture is None:
        architecture = d.architecture
      elif architecture != d.architecture:
        return 'multiplatform'
    return architecture

  architecture = property(get_architecture, None, None)

# Report class
class Report:
  """
  This class is a model for the whole report: collection of logs, times, duration,
  etc.
  """

  def __init__(self, reportTime = None):
    """
    Constructor

    @param reportTime The report time. If not specified, the current time will be used.
    """
    self.startTime  = None # Start time    (if available)
    self.endTime    = None # End time      (if available)

    self.projects   = dict()
    self.subsystems = dict()
    self.components = dict()
    self.tests      = dict()

    self.subsysByProject    =  dict()
    self.componentsBySubsys = dict()

    if reportTime:
      self.time = reportTime
    else:
      self.time = datetime.now()

    self._duration  = None
    self._eduration = None

  def attachLog(self, log, setName = None, setId = None, tuple = None):
    """
    If setId is not defined, attaches the log. If it is, attaches the tuple
    using log as the parent node

    @param log   Should be an instance of Log
    @param setId Test set identifier (usually the component name)
    @param tuple A tuple with the deployment/test logs
    """

    # Build log
    if setName is None:
      # Create entries
      if not log.project in self.projects:
        self.projects[log.project]        = []
        self.subsysByProject[log.project] = []

      if log.subsystem and not log.subsystem in self.subsystems:
        self.subsystems[log.subsystem]         = []
        self.componentsBySubsys[log.subsystem] = []
        self.subsysByProject[log.project].append(log.subsystem)

      if log.component and not log.component in self.components:
        self.components[log.component] = []
        self.componentsBySubsys[log.subsystem].append(log.component)

      # If it is a project level log
      if log.component:
        self.components[log.component].append(log)
      elif log.subsystem:
        self.subsystems[log.subsystem].append(log)
      else:
        self.projects[log.project].append(log)
    else:
      # It is a deployment/test log
      if log.id not in self.tests:
        self.tests[log.id] = dict()

      self.tests[log.id][setName] = NodeSet(setName, setId, tuple)

  def setDuration(self, duration):
    """
    Force the value of the duration

    @param duration The duration in seconds
    """
    self._duration = duration

  def duration(self):
    """
    Calculates the duration from the start and end times

    @return The duration of the execution in seconds
    """
    if self._duration:
      return self._duration
    else:
      if not self.startTime or not self.endTime:
        return None
      else:
        self._duration = (self.endTime - self.startTime).seconds
        return self._duration

  def eduration(self):
    """
    Estimates the duration of the process from the logs

    @return An integer with the estimated duration. This is not reliable (might be more, might be less)
    """
    if self._eduration:
      return self._eduration
    else:
      self._eduration = 0
      maxBuild        = 0
      for b in self.getBuilds():
        maxTest = 0
        for setId, set in self.getTests(b.id).iteritems():
          for t in set:
            if t[0].duration > maxTest:
              maxTest = t[0].duration
        if b.duration is not None:
          total = b.duration + maxTest
          if total > self._eduration:
            self._eduration = total

      return self._eduration

  def getBuilds(self, id = None):
    """
    Returns a list of the build logs associated with 'id'

    @param project The project/subsystem/component id. If not specified, all will be returned
    @return A list
    """
    list = []

    # Project build
    for l in self.projects.values():
      for b in l:
        if id is None or id == b.module:
          list.append(b)
    # Subsystem build
    for l in self.subsystems.values():
      for b in l:
        if id is None or id == b.module:
          list.append(b)
    # Component build
    for l in self.components.values():
      for b in l:
        if id is None or id == b.module:
          list.append(b)

    return list

  def getTests(self, parentId):
    """
    Returns the dictionary of the deployment and tests associated with 'parentId'
    The key is the main component to test, and the value a list of tuples

    @param parentId The build
    @return A dictionary
    """
    list = dict()

    if parentId in self.tests:
      list.update(self.tests[parentId])

    return list
    
  def getSubsystems(self, project):
    """
    Returns a list of the subsystems inside a project

    @param project The project ID
    @return A list
    """
    return self.subsysByProject[project]

  def getComponents(self, subsys):
    """
    Returns a list with the components associated with the subsystem 'subsys'

    @param subsys The subsystem ID
    @return A string list
    """
    return self.componentsBySubsys[subsys]

