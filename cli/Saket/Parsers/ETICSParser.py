##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Parser for ETICS logs (XML format, see build-status.xml)
#
##############################################################################

import time
import xml.dom.minidom
import urllib
from datetime          import datetime
from Saket.Environment import Environment
from Saket.Logger      import Logger
from Saket.Report      import Log

class ETICSParser:
  """
  This class abstracts the parsing of ETICS build logs
  """

  env = Environment()

  def secondsFromDuration(self, str):
    """
    Converts a string like hours:minutes:seconds to an integer
    with the total number of seconds

    @param str The string to convert
    @return The number of seconds
    """

    array=str.split(':')

    seconds=int(array[0]) * 3600 + int(array[1]) * 60 + int(array[2])

    return seconds


  def parse(self, file):
    """
    This function will parse a ETICS build log (XML) and create a new log
    with the retrieved information

    @param file It can be a string with the location of the file, or a file object
    @return A log object with the retrieved information
    """

    log = Log()

    # Get the root
    try:
      if isinstance(file, basestring):
        doc     = xml.dom.minidom.parse(urllib.urlopen(file))
        log.URL = file
      else:
        doc     = xml.dom.minidom.parse(file)
        log.URL = None
      root    = doc.getElementsByTagName('project')[0]
    except:
      Logger.error("Error parsing %s" % file)
      log.title     = "Error parsing %s" % file
      log.startTime = log.endTime = datetime.now()
      log.status    = 'execution error'
      return log

    # Create the log and set some attributes we can get from the root
    log.title = "%s_%s" % (root.getAttribute('moduleconfig'), \
                           root.getAttribute('platform'))
    log.startTime = datetime(*(time.strptime(root.getAttribute('starttime'), '%d/%m/%Y %H:%M:%S')[0:6]))

    try:
      log.endTime = datetime(*(time.strptime(root.getAttribute('endtime'), '%d/%m/%Y %H:%M:%S')[0:6]))
    except:
      log.endTime = log.startTime # For some reason, there are times when the endtime is not in ETICS build-status.xml

    # Fix for ticket #45
    # ETICS will report success when the build stage is not even executed
    buildtype = root.getAttribute('buildtype')
    if buildtype != 'build':
      log.status =  'execution error'
      Logger.error("Execution of %s failed because the buildtype is not build: %s" % (log.title, buildtype))
      return log

    # Duration
    try:
      log.duration  = self.secondsFromDuration(root.getAttribute('duration'))
    except:
      log.duration = 1 # Same with duration
      
    log.status    = root.getAttribute('currentstatus').lower()

    # Project configuration
    log.projectConfiguration = root.getAttribute('config')
    log.configuration        = root.getAttribute('moduleconfig')

    # Go through
    modules = root.getElementsByTagName('modules')[0]
    modules = root.getElementsByTagName('module')
    log.total   = 0
    log.success = 0
    log.failed  = 0
    for node in modules:
      if int(node.getAttribute('result')) == 0:
        log.success+=1
      else:
        log.failed+=1
    log.total = log.success + log.failed

    # Return
    return log
