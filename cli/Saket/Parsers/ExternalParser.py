##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Dummy parser for externals (it does not parse anything, really)
#
##############################################################################

import time
import xml.dom.minidom
import urllib
from datetime          import datetime
from Saket.Environment import Environment
from Saket.Report      import Log

class ExternalParser:
  """
  Dummy parser 
  """

  def parse(self, file):
    log = Log()
    log.total    = 1
    log.success  = 1
    log.failed   = 0
    log.duration = 0
    log.title    = 'External'
    log.status   = 'external'
    return log

