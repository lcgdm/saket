##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Parser Yaimgen logs
#
##############################################################################

import time
import xml.dom.minidom
import urllib
from datetime          import datetime
from Saket.Environment import Environment
from Saket.Report      import Log

class SaketParser:
  """
  This class abstracts the parsing of yaimgen logs
  """

  SAKET_NAMESPACE='https://svnweb.cern.ch/trac/saket/'

  env = Environment()

  def parse(self, file):
    """
    This function will parse a ETICS build log (XML) and create a new log
    with the retrieved information

    @param file       It can be a string with the location of the file, or a file object
    @return A log object with the retrieved information
    """

    log = Log()
    log.total   = 0
    log.success = 0
    log.failed  = 0
    log.duration = 0

    # Get the root
    try:
      if isinstance(file, basestring):
        doc     = xml.dom.minidom.parse(urllib.urlopen(file))
        log.URL = file
      else:
        doc     = xml.dom.minidom.parse(file)
        log.URL = None
      root = doc.getElementsByTagNameNS(self.SAKET_NAMESPACE, 'batch')[0]
    except:
      log.title     = "Error parsing %s" % file
      log.startTime = log.endTime = datetime.now()
      log.status    = 'execution error'
      return log

    # Title from the XML
    log.title = root.getAttribute('name')

    executions = root.getElementsByTagNameNS(self.SAKET_NAMESPACE, 'execute')
    for execute in executions:
      if execute.getAttribute('exit') == '0':
        log.success += 1
      else:
        log.failed  += 1
    log.total = log.success + log.failed

    # Set the global status
    if log.failed > 0:
      log.status = 'failed'
    elif log.total == 0:
      errors = root.getElementsByTagNameNS(self.SAKET_NAMESPACE, 'error')
      if len(errors) > 0:
        log.status = 'failed'
      else:
        log.status = 'not defined'
    else:
      log.status = 'success'

    # Return
    return log
