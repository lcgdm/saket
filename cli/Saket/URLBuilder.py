##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Module to build the URLs for ETICS
#
##############################################################################

from Environment import Environment

class URLBuilder:
  """
  This class is used to generate ETICS URLs
  """

  def __init__(self):
    """
    Constructor
    """
    self.env = Environment()

  def getBuildLogURL(self, task):
    """
    Generates the URL of the build log

    @param task An object with the fields urlnamePrefix and architecture
    @return A string with the URL
    """
    return "%s/%s/%s/-/reports/%s" % \
      (self.env.ETICS_BASE, task.id, task.architecture, task.parser['BUILD_LOG'])

  def getRepoURL(self, task):
    """
    Generates the URL of the repository

    @param task An object with the field 'parent' pointing to a executed build task
    @return A string with the URL
    """

    if task.parent.externalRepo:
      return task.parent.externalRepo
    elif task.parent.id:
      return "%s/%s/%s/etics-volatile-build-by-id.repo" % \
        (self.env.ETICS_REPO_BASE, task.parent.id, task.architecture)
    else:
      return "%s/%s_%s/etics-volatile-build-by-id.repo" % \
        (self.env.ETICS_REPO_BASE_NAME, task.parent.urlnamePrefix, task.architecture)

  def getTarUrl(self, task):
    """
    Generates the URL of the tar.gz containing all the logs

    @param task An object with the fields urlnamePrefix and architecture
    @return A string with the URL
    """
    return "%s/%s/%s" % \
      (self.env.ETICS_BASE, task.id, task.architecture)

  def getEticsLogURL(self, task):
    """
    Generates the URL of the ETICS HTML build log

    @param task An object with the fields urlnamePrefix and architecture
    @return A string with the URL
    """
    return "%s/%s/%s/-/reports/%s" % \
      (self.env.ETICS_BASE, task.id, task.architecture, task.parser['BUILD_LOG_LINK'])

  def getDeploymentLogURL(self, task):
    """
    Generates the URL of the deployment log of a specific metapackage

    @param task An object with the fields urlnamePrefix, architecture, urlnameTestInfix and label
    @return A string with the URL
    """
    return "%s/%s/%s/-/reports/%s" % \
      (self.env.ETICS_BASE, task.id, task.architecture, task.label, task.parser['DEPLOY_LOG_LINK'])

  def getTestLogURL(self, task):
    """
    Generates the URL of the test log of a specific metapackage

    @param task An object with the fields urlnamePrefix, architecture, urlnameTestInfix and label
    @return A string with the URL
    """
    return "%s/%s/%s/-/reports/%s" % \
      (self.env.ETICS_BASE, task.id, task.architecture, task.label, task.parser['TEST_LOG_LINK'])
