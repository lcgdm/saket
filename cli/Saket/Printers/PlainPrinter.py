##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2010.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Prints the logs through the standard output
#
##############################################################################

import sys
from BasePrinter       import BasePrinter
from Saket.Environment import Environment

class PlainPrinter(BasePrinter):
  """
  Prints the log informartion to a file (stdout by default) in a plain text format
  """

  def __init__(self, file = sys.stdout):
    """
    Constructor

    @param file   (Optional) Where to write the output
    """
    self.env  = Environment()
    self.file = file

  def write(self, report):
    """
    Generates the report

    @param report An instance of Report with all the information
    """
    self.file.write("Status on %s\n\n" % report.time.strftime("%Y-%m-%d %X"))

    # Iterate through the builds
    for bLog in report.getBuilds():
      self.file.write("%-25s %-20s %s\t(%d/%d)\n" %
                      (bLog.configuration, bLog.architecture, bLog.status.capitalize(), bLog.success, bLog.total))
      # Go through the associated sets
      for setName, set in report.getTests(bLog.id).iteritems():
        self.file.write("\t%s\n" % setName)
        for dLog, tLog in set:
          self.file.write("\t\tDeployment:\t%-25s %-20s %s\n" % (dLog.module, dLog.architecture, dLog.status.capitalize()))
          if tLog:
            self.file.write("\t\t\tTest:\t%s (%d/%d)\n" % (tLog.status.capitalize(), tLog.success, tLog.total))
          else:
            self.file.write("\t\t\tTest:\t-\n")
