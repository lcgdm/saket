##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Generates a report in XML
#
##############################################################################

import sys
from BasePrinter       import BasePrinter
from Saket.Environment import Environment
from Cheetah.Template  import Template

class XMLPrinter(BasePrinter):
  """
  Prints the log information to a file (stdout by default) in HTML format (using templates)
  """

  def __init__(self, file = sys.stdout, template = None):
    """
    Constructor

    @param file     (Optional) Where to write the output
    @param template (Optional) The template path
    """
    self.env  = Environment()
    self.file = file

    if template != None:
      self.template = template
    else:
      self.template = self.env.REPORT_TEMPLATE


  def write(self, report):
    """
    Generates the report

    @param report An instance of Report with all the information
    """
    t = Template(file = self.template, searchList = [{'REPORT': report}, self.env])
    print >>self.file, t
