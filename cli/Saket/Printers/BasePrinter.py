##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Store the logs in a database
#
##############################################################################

from Saket.Environment import Environment

class BasePrinter:
  """
  This clase should be implemented by the printers
  """
  
  def __init__(self):
    """
    Constructor
    """
    self.env = Environment()

  def name(self):
    """
    Name of the printer
    """
    return self.__class__.__name__

  def __str__(self):
    """
    Name of the printer
    """
    return self.name()

  def write(self, report):
    """
    Executes the action associated with the printer
    MUST be implemented

    @param report An instance of Report with all the information
    """
    raise NotImplementedError()
