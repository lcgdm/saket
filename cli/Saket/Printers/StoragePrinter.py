##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Prints the logs through the standard output
#
##############################################################################

from BasePrinter       import BasePrinter
from XMLPrinter        import XMLPrinter
from Saket.Archive import Archive
from Saket.Logger      import Logger
from Saket.Environment import Environment
from Saket.URLBuilder  import URLBuilder

from Cheetah.Template  import Template

import os.path
import libxml2
import libxslt

class StoragePrinter(BasePrinter):
  """
  This printer will store the tar.gz with the logs to the configured location
  """

  NAMESPACE='https://svnweb.cern.ch/trac/lcgutil/saket'

  def __init__(self):
    """
    Constructor
    """
    self.env        = Environment()
    self.urlBuilder = URLBuilder()

  def _createFolder(self, reportTime):
    """
    Create a folder with the reportTime if it doesn't exist

    @param reportTime A datetime instance
    """
    folder = "%s/%s" % (self.env.STORAGE_PRINTER['LOCATION'],
                        reportTime.strftime("%Y-%m-%d_%H-%M-%S"))
    if not os.access(folder, os.F_OK):
      os.mkdir(folder)

    return folder

  def _xmlReport(self, fname, report):
    """
    Generates the XML report using the template

    @param fname  The path to the XML
    @param report An instance of Report with all the information
    """
    fd          = open(fname, 'w')
    htmlPrinter = XMLPrinter(fd, self.env.STORAGE_PRINTER['TEMPLATE'])
    htmlPrinter.write(report)
    fd.close()

  def summary(self):
    """
    Generates the XML summary with all the stored reports

    @param folder The folder where to store the file
    """
    archive = Archive(self.env.STORAGE_PRINTER['LOCATION'])
    t = Template(file = self.env.SUMMARY_TEMPLATE, searchList = [{'ARCHIVE': archive}, self.env])

    summary = os.path.join(self.env.STORAGE_PRINTER['LOCATION'], self.env.STORAGE_PRINTER['SUMMARY_FNAME'])
    Logger.debug("Printing summary to %s" % summary)

    # Print the XML file
    fd = open(summary, 'w')
    print >>fd, t
    fd.close()

    # Apply the configured stylesheets to the summary
    doc = libxml2.parseFile(summary)
    for style, out in self.env.STORAGE_PRINTER['SUMMARY_XSLT'].iteritems():
      if not style.startswith('/'):
        style    = os.path.join(self.env.STORAGE_PRINTER['LOCATION'], style)
      if not out.startswith('/'):
        out      = os.path.join(self.env.STORAGE_PRINTER['LOCATION'], out)

      Logger.debug("Applying style %s into %s" % (style, out))

      try:
        styledoc = libxml2.parseFile(style)
        style    = libxslt.parseStylesheetDoc(styledoc)
        result   = style.applyStylesheet(doc, None)
        html     = result.serialize()

        fd = open(out, 'w')
        print >>fd, html
        fd.close()
      except Exception, e:
        Logger.error("Error applying the stylesheet: %s" % str(e))
      
    

  def write(self, report):
    """
    Store in the specified path the logs

    @param report An instance of Report with all the information
    """

    # Create the folder that will contain all the logs
    folder = self._createFolder(report.time)

    # Generate the HTML with the all-in-one report and store it too
    fname = os.path.join(folder, 'report.xml')
    self._xmlReport(fname, report)

    # Apply the configured stylesheet to the report
    doc = libxml2.parseFile(fname)
    for style, out in self.env.STORAGE_PRINTER['XSLT'].iteritems():
      if not style.startswith('/'):
        style    = os.path.join(self.env.STORAGE_PRINTER['LOCATION'], style)
      if not out.startswith('/'):
        out      = os.path.join(folder, out)

      Logger.debug("Applying style %s to %s into %s" % (style, fname, out))

      try:
        styledoc = libxml2.parseFile(style)
        style    = libxslt.parseStylesheetDoc(styledoc)
        result   = style.applyStylesheet(doc, None)
        html     = result.serialize()

        fd = open(out, 'w')
        print >>fd, html
        fd.close()
      except Exception, e:
        Logger.error("Error applying the stylesheet: %s" % str(e))

    # Regenerate the summary
    self.summary()

