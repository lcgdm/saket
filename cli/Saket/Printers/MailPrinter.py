##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Uses HTML reporter to generate and output, and send it by mail
#
##############################################################################

from BasePrinter  import BasePrinter
from XMLPrinter   import XMLPrinter
from Saket.Logger import Logger
from StringIO     import StringIO
import smtplib
import libxml2
import libxslt

class MailPrinter(BasePrinter):
  """
  Generates a HTML document summarizing the report, and sends it by mail
  """

  def write(self, report):
    """
    Generates the report

    @param report An instance of Report with all the information
    """
    # Generate the output
    buffer = StringIO()

    htmlPrinter = XMLPrinter(buffer)
    htmlPrinter.write(report)

    # Generate the HTML
    string   = buffer.getvalue()
    doc      = libxml2.parseMemory(string, len(string))
    styledoc = libxml2.parseFile(self.env.MAIL_PRINTER['HTM_TEMPLATE'])
    style    = libxslt.parseStylesheetDoc(styledoc)
    result   = style.applyStylesheet(doc, None)
    htm      = result.serialize()

    # Add the header
    body = "From: %s\r\nTo: %s\r\nSubject: %s\r\nContent-type: text/html\r\n\r\n%s" % (self.env.MAIL_PRINTER['FROM_ADDR'],
                                                                                       ", ".join(self.env.MAIL_PRINTER['TO_ADDR']),
                                                                                       self.env.MAIL_PRINTER['SUBJECT'],
                                                                                       htm)

    # Send the mail
    server = smtplib.SMTP(self.env.MAIL_PRINTER['SMTP_HOST'])
    server.sendmail(self.env.MAIL_PRINTER['FROM_ADDR'], self.env.MAIL_PRINTER['TO_ADDR'], body)
    server.quit()
    buffer.close()

    Logger.debug("Mail submitted to %s" % self.env.MAIL_PRINTER['TO_ADDR'])
