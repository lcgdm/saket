##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Common routines for ETICS use
#
##############################################################################

import os
import sys
import Saket.Utils
from Saket.Logger import Logger

import warnings
warnings.simplefilter("ignore")

# Check if ETICS is in the path
eticsPath = Saket.Utils.pathOfCommand("etics-workspace-setup")
if not eticsPath:
  Logger.error("ETICS bin directory is not in the path")
  sys.exit(1)
Logger.debug("ETICS binaries under %s" % eticsPath)
sys.path.append(eticsPath)

# Add the ETICS libraries to the path
pythonVersion = ".".join(["%s" % i for i in sys.version_info[0:2]])
eticsLib = os.path.abspath("%s/../lib/python%s/site-packages" % (eticsPath, pythonVersion))
sys.path.append(eticsLib)
Logger.debug("ETICS libraries under %s" % eticsLib)

# ETICS need to do some setup
import CommandBase
