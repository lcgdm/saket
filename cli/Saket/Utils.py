##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Some utilities
#
##############################################################################

import os
import sys
import types
from stat import *

def getMod(module):
  """
  Imports a module at runtime

  @param module The module name (i.e. Saket.Reporters.HTMLReporter)
  @return A Module
  """
  try:
    aMod = sys.modules[module]
    if not isinstance(aMod, types.ModuleType):
      raise KeyError
  except KeyError:
    aMod = __import__(module, globals(), locals(), [''])
    sys.modules[module] = aMod

  return aMod

def getClass(fullClassName):
  """
  Load a module and retrieve a class

  @param fullClassName The name of the class, with the module (i.e Saket.Reporters.HTMLReporter.HTMLReporter)
  @return A Class (NOT an instance)
  """

  # Parse the path
  lastDot   = fullClassName.rfind('.')
  className = fullClassName[lastDot + 1:]
  module    = fullClassName[:lastDot]

  # Get the module
  try:
    aMod = getMod(module)
  except ImportError, ie:
    raise

  # And the class
  aClass = getattr(aMod, className)

  # Return
  return aClass


def pathOfCommand(cmd):
  """
  This function will look for a binary in the PATH environment. Will return
  the location of its parent folder if found. If not, None will be returned.

  @param cmd The command
  @return A string with the path, or None
  """

  pathList = os.environ['PATH'].split(os.pathsep)

  for path in pathList:
    filename = os.path.join(path, cmd)
    try:
      st = os.stat(filename)
    except os.error:
      continue

    mode = S_IMODE(st[ST_MODE])
    if mode & 0111:
      return path

  return None