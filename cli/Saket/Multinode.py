##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# This class wraps single node and multinode execution, delegating into the
# log parsers once found the logs
#
##############################################################################

import os.path
import tarfile
import urllib
import xml.dom.minidom
import Saket.Utils
from datetime          import datetime
from Saket.Report      import Log
from Saket.Logger      import Logger

class Multinode:
  """
  This class abstracts the multinode/single node execution, detecting which one
  it has been, and calling SaketParser accordingly.
  """

  def process(self, tgz, parserMeta):
    """
    This functions receives the tgz file as a parameters, processes it and
    call SaketParser as it is.

    @param  tgz        It can be either a string with a URI, a file object or a TarFile object
    @param  parserMeta The parser information associated with the task
    @return A n-tuple of deployment/test logs tuples (as returned by SaketParser), being n the number of nodes deployed
    """
    # Open the file
    try:
      if isinstance(tgz, basestring):
        (fname, headers) = urllib.urlretrieve(tgz)
        tar = tarfile.open(fname)
      elif isinstance(tgz, file):
        tar = tarfile(fileobj = tgz)
      elif isinstance(tgz,tarfile.TarFile):
        tar = tgz
      else:
        raise TypeError('Can not apply parse to a variable of type %s' % type(tgz))
    except:
      # Execution error
      deployLog = Log()
      deployLog.title  = "Error opening %s" % str(tgz)
      deployLog.type   = 'deployment'
      deployLog.startTime = deployLog.endTime = datetime.now()
      deployLog.status = 'execution error'
      Logger.error("Error opening %s" % str(tgz))
      return ((deployLog, None),)

    tupleList = []

    # Iterate through all entries
    for member in tar.getmembers():
      if member.isfile() and member.name.endswith(parserMeta['DEPLOY_LOG']):
        # We found a node test!
        relpath    = os.path.dirname(member.name)
        deployName = member.name
        testName   = os.path.join(relpath, parserMeta['TEST_LOG'])
        statusName = os.path.join(relpath, 'build-status.xml')

        # First, from build-status.xml get some information
        status = xml.dom.minidom.parse(tar.extractfile(statusName))
        root   = status.getElementsByTagName('project')[0]
        architecture = root.getAttribute('platform')
        config       = root.getAttribute('moduleconfig')
        module       = root.getAttribute('modulename')

        # Parse Yaimgen/Favourite specific files
        deployFile = tar.extractfile(deployName)
        try:
          testFile   = tar.extractfile(testName)
        except Exception, e:
          testFile = None
          Logger.error('Could not extract %s: %s' % (testName, str(e)))

        Logger.debug('Using parser %s for the deploy log' % parserMeta['DEPLOY_LOG_PARSER'])
        dParser   = Saket.Utils.getClass(parserMeta['DEPLOY_LOG_PARSER'])()
        deployLog = dParser.parse(deployFile)
        deployLog.architecture  = architecture
        deployLog.configuration = config
        deployLog.module        = module
        deployLog.log           = deployName
        
        if deployLog.status == 'success' and testFile is not None:
          Logger.debug('Using parser %s for the test log' % parserMeta['TEST_LOG_PARSER'])
          tParser = Saket.Utils.getClass(parserMeta['TEST_LOG_PARSER'])()
          testLog = tParser.parse(testFile)
          testLog.log = testName
        else:
          testLog = None

        tupleList.append((deployLog, testLog))
    # end for
    if len(tupleList) == 0:
      Logger.error('There are no logs: %s' % str(tgz))
      deployLog = Log()
      deployLog.title  = "There are no logs: %s" % str(tgz)
      deployLog.type   = 'deployment'
      deployLog.startTime = deployLog.endTime = datetime.now()
      deployLog.status = 'execution error'
      tupleList.append((deployLog, None))

    return tuple(tupleList)

