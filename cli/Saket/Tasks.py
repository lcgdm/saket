##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Classes that actually executes the orders
#
##############################################################################

from Logger import Logger
from Environment import Environment

def getAttr(dictStack, attr, default = None):
  for d in reversed(dictStack):
    if attr in d:
      return d[attr]
  return default

def getDict(dictStack, attr, env = None):
  builtD = {}
  
  if env is not None and env[attr]:
    builtD.update(env[attr])

  for d in dictStack:
    if attr in d and type(d[attr]) is dict:
      builtD.update(d[attr])

  return builtD

# Base class for all tasks
class Task:
  """
  Represents a generic task
  """

  def __init__(self, moduleStack, architecture, dictStack, environment):
    """
    Constructor.

    @param moduleStack  A Stack with this module and its parents (project, subsystem, component)
    @param architecture The architecture for the task
    @param dictStack    A stack with this module attributes and its parents'
    @param environment  An instance of Saket.Environment for configuration values
    """
    self.project              = getAttr(dictStack, 'PROJECT', moduleStack[0])
     
    self.configuration        = getAttr(dictStack, 'CONFIGURATION')
    self.projectConfiguration = getAttr(dictStack, 'PROJECT_CONFIGURATION')
    self.module               = moduleStack[-1]
    self.urlnamePrefix        = getAttr(dictStack, 'URLNAME_PREFIX')
    self.urlnameTestInfix     = getAttr(dictStack, 'URLNAME_TEST_INFIX')
    self.architecture         = architecture
    self.enabled              = getAttr(dictStack, 'ENABLED')
    if self.enabled == None:
      self.enabled = True
    self.environ              = getAttr(dictStack, 'ENVIRON')
    self.externalRepo         = getAttr(dictStack, 'EXTERNAL_REPO')

    if not self.configuration and self.externalRepo is None:
      self.configuration = self.projectConfiguration

    if len(moduleStack) > 1:
      self.subsystem = moduleStack[1]
    else:
      self.subsystem = None

    if len(moduleStack) > 2:
      self.component = self.module
    else:
      self.component = None

    self.id          = None
    self.status      = None
    self.executionID = None
    self.name        = "%s_%s" % (self.urlnamePrefix, self.architecture)

    self.eticsClient = getAttr(dictStack, 'ETICS_CLIENT')
    if self.eticsClient is None:
      self.eticsClient = environment['ETICS_CLIENT']

    self.checkoutExtra = getAttr(dictStack, 'CHECKOUT_EXTRA', '')
    self.buildExtra    = getAttr(dictStack, 'BUILD_EXTRA',    '')

    self.parser          = getDict(dictStack, 'PARSER', environment)
    self.test_properties = getDict(dictStack, 'TEST_PROPERTIES', environment)

  def finished(self):
    """
    Returns True if the task is in a finished status
    """
    return self.status != None and \
           self.status != 'running' and \
           not self.status.startswith('pending')

  def passed(self):
    """
    Returns True if the task is in a successful finished status
    """
    return self.status in ['success', 'external']
    
  def getString(self, tabs = 0):
    """
    Returns a string representation of this object

    @param tabs The level of nesting
    """
    tabString = '-' * (tabs * 2)
    if self.configuration is not None:
      return " %s+ %s: %s / %s (%s - %s (%s) - ETICS client %s)" % (tabString, self.__class__.__name__,
                                                               self.module, self.name,
                                                               self.configuration, self.project, self.projectConfiguration,
                                                               self.eticsClient)
    else:
      return " %s+ Using external repository '%s'" % (tabString, self.externalRepo)

  def __str__(self):
    """
    Returns a string representation of this object
    """
    return self.getString()

# Task list
class TaskList(list):

  def __init__(self):
    """
    Constructor
    """
    list.__init__(self)

  def __str__(self):
    """
    Returns a string representation of this object
    """
    repr = ""
    for task in self:
      repr += str(task) + '\n'
    return repr

# Build task
class BuildTask(Task, TaskList):

  def __init__(self, moduleStack, architecture, dictStack, environment):
    """
    Constructor.

    @param moduleStack  A Stack with this module and its parents (project, subsystem, component)
    @param architecture The architecture for the task
    @param dictStack    A stack with this module attributes and its parents'
    @param environment  An instance of Saket.Environment for configuration values
    """
    Task.__init__(self, moduleStack, architecture, dictStack, environment)
    TaskList.__init__(self)

  def append(self, item):
    """
    Appends a test task to this build task

    @param item The nested task
    """
    item.parent = self
    TaskList.append(self, item)

  def getString(self, tabs = 0):
    """
    Returns a string representation of this object

    @param tabs The nesting level
    """
    repr = Task.getString(self, tabs) + '\n'
    for child in self:
      repr += child.getString(tabs + 1) + '\n'
    return repr

# Test task
class TestTask(Task):

  def __init__(self, moduleStack, architecture, testLabel, testConfig, dictStack, environment):
    """
    Constructor.

    @param moduleStack  A Stack with this module and its parents (project, subsystem, component)
    @param architecture The architecture for the task
    @param testLabel    The test label
    @param testConfig   The test configuration. Tuple (component, configuration)
    @param dictStack    A stack with this module attributes and its parents'
    @param environment  An instance of Saket.Environment for configuration values
    """
    Task.__init__(self, moduleStack, architecture, dictStack, environment)
    self.label         = testLabel
    self.module        = testConfig[0]
    self.configuration = testConfig[1]
    self.parent        = None

    self.id   = None
    self.name = "%s_%s_%s" % (self.urlnamePrefix, self.architecture, self.label)

    self.jobs = dict()

    # Allow to override the ETICS client version for tests
    eticsTest = getAttr(dictStack, 'ETICS_CLIENT_TEST')
    if not eticsTest:
      eticsTest = environment['ETICS_CLIENT_TEST']
    if eticsTest:
      self.eticsClient = eticsTest

  def getString(self, tabs = 0):
    """
    Returns a string representation of this object

    @param tabs The nesting level
    """
    tabString = '-' * (tabs * 2)
    return " |%s%s: %s / %s (%s - %s - ETICS client %s)" % (tabString, self.__class__.__name__,
                                                     self.module, self.name,
                                                     self.configuration, self.projectConfiguration, self.eticsClient)

# Single job in ETICS (needed since Multinode is supported)
class Job:
  """
  Single job in ETICS. A TestTask may have multiple jobs, or only one
  """

  def __init__(self, configuration, platform, subID, executionID):
    """
    Constructor
    """
    self.configuration = configuration
    self.platform      = platform
    self.subID         = subID
    self.executionID   = executionID

  def finished(self):
    """
    Returns True if the task is in a finished status
    """
    return self.status != None and \
           self.status != 'running' and \
           not self.status.startswith('pending')


# Attach to the taskList the tasks from the dictStack
def processTasks(moduleStack, dictStack, taskList, environment):
  """
  Attach to the specified task list a new set of tasks comming from the
  module specified by moduleStack (project, subsystem, component)

  @param moduleStack  A Stack with this module and its parents (project, subsystem, component)
  @param dictStack    A stack with this module attributes and its parents'
  @param taskList     List where to append the new tasks
  @param environment  An instance of Saket.Environment for configuration values
  """
  topAttr = dictStack[-1]
  # Build
  if 'BUILD' in topAttr:
    for groupAttr in topAttr['BUILD'].values():
      dictStack.append(groupAttr)
      for architecture in groupAttr['ARCHITECTURES']:
        buildTask = BuildTask(moduleStack, architecture, dictStack, environment)
        if buildTask.enabled:
          taskList.append(buildTask)
          # Append the tests
          tests = getAttr(dictStack, 'TEST')
          if tests:
            for (tLabel, tConfig) in tests.iteritems():
              testTask = TestTask(moduleStack, architecture, tLabel, tConfig, dictStack, environment)
              buildTask.append(testTask)
      dictStack.pop()

# Populates the TaskList with the configuration passed
def generateTaskTree(project):
  """
  From the specified configuration (see default.py), populates a task list
  with nested levels:
    - 1st level: Build tasks
    - 2nd level: Test tasks

  @param project The dictionary with the configuration
  @return A task tree
  """
  taskList = TaskList()

  # Iterate through the projects
  dictStack   = []
  moduleStack = []
  environment = Environment()
  
  for prId, projectAttr in project.iteritems():
    dictStack.append(projectAttr)
    moduleStack.append(prId)
    # Add the build and test tasks for the project
    processTasks(moduleStack, dictStack, taskList, environment)
    # Iterate through the subsystems
    if 'SUBSYSTEMS' in projectAttr:
      for subId, subsysAttr in projectAttr['SUBSYSTEMS'].iteritems():
        dictStack.append(subsysAttr)
        moduleStack.append(subId)
        # Add the build and test tasks for the subsystem
        processTasks(moduleStack, dictStack, taskList, environment)
        # Iterate through the components
        if 'COMPONENTS' in subsysAttr:
          for compId, componentAttr in subsysAttr['COMPONENTS'].iteritems():
            dictStack.append(componentAttr)
            moduleStack.append(compId)
            processTasks(moduleStack, dictStack, taskList, environment)
            moduleStack.pop()
            dictStack.pop()
        # Remove the subsystem from the stack
        moduleStack.pop()
        dictStack.pop()
    # Remove the project from the stack
    moduleStack.pop()
    dictStack.pop()

  Logger.debug(str(taskList))
  return taskList
