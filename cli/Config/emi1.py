##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Configuration file for EMI1
#
##############################################################################

EMI1 = {
        'PROJECT': 'EMI',
        'ENABLED': True,
        'PROJECT_CONFIGURATION': 'emi_R_1_prod',
        'URLNAME_TEST_INFIX': 'test',
        'CHECKOUT_EXTRA': '--noask --continueonerror --verbose --frombinary --runtimedeps',
        'BUILD_EXTRA': '--verbose --continueonerror',

        'PARSER' : {
                'BUILD_LOG'      : 'build-status.xml',
                'BUILD_LOG_LINK' : 'index.html',
                'DEPLOY_LOG'     : 'deployment.xml',
                'DEPLOY_LOG_LINK': 'deployment.xml',
                'TEST_LOG'       : 'tests.xml',
                'TEST_LOG_LINK'  : 'tests.xml',

                'BUILD_LOG_PARSER' : 'Saket.Parsers.ETICSParser.ETICSParser',
                'DEPLOY_LOG_PARSER': 'Saket.Parsers.SaketParser.SaketParser',
                'TEST_LOG_PARSER'  : 'Saket.Parsers.SaketParser.SaketParser'
        },

        'SUBSYSTEMS': {

                # LCGDM
                'lcgdm': {
                        'TEST_PROPERTIES': {'voms': 'testers.eu-emi.eu'},
                        'BUILD': {
                                'EMI1-64-SL5': {
                                        'URLNAME_PREFIX': 'emi1_lcgdm_head_epel',
                                        'BUILD_EXTRA': '--verbose --continueonerror --repackage=emi-1-prod-sl5-x86_64',
                                        'CONFIGURATION': 'emi-lcgdm.EPEL',
                                        'ARCHITECTURES': ['sl5_x86_64_gcc412EPEL'],
                                },
                                'EMI1-32-SL5': {
                                        'URLNAME_PREFIX': 'emi1_lcgdm_head_epel',
                                        'BUILD_EXTRA': '--verbose --continueonerror --repackage=emi-1-sl5-i386',
                                        'CONFIGURATION': 'emi-lcgdm.EPEL',
                                        'ARCHITECTURES': ['sl5_ia32_gcc412EPEL'],
                                },
                        },
                        'TEST': {
                                'emi-dpm_mysql'  : ('emi.favourite', 'emi.favourite.HEAD'),
                                'emi-dpm_disk'   : ('emi.favourite', 'emi.favourite.HEAD'),
                                'emi-lfc_mysql'  : ('emi.favourite', 'emi.favourite.HEAD'),
                                'emi-lfc_oracle' : ('emi.favourite', 'emi.favourite.ORACLE')
                        }
                }, # LCGDM
        } # SUBSYSTEMS
} # EMI

