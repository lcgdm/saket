##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2010.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Configuration file
#
##############################################################################

__name__ = 'Default'

import datetime
import os.path

# Debug
DEBUG = False

# Projects
PROJECTS = {
  # PROJECT GLITE
  'org.glite': {
    'ENABLED': True,
    'URLNAME_TEST_INFIX': 'test',
    'CHECKOUT_EXTRA': '--noask --continueonerror --verbose --frombinary --runtimedeps',
    'BUILD_EXTRA': '--verbose --continueonerror',

    'PARSER' : {
      'BUILD_LOG'      : 'build-status.xml',
      'BUILD_LOG_LINK' : 'index.html',
      'DEPLOY_LOG'     : 'deployment.xml',
      'DEPLOY_LOG_LINK': 'deployment.xml',
      'TEST_LOG'       : 'tests.xml',
      'TEST_LOG_LINK'  : 'tests.xml',

      'BUILD_LOG_PARSER' : 'Saket.Parsers.ETICSParser.ETICSParser',
      'DEPLOY_LOG_PARSER': 'Saket.Parsers.SaketParser.SaketParser',
      'TEST_LOG_PARSER'  : 'Saket.Parsers.SaketParser.SaketParser'
    },
    
    'SUBSYSTEMS': {

      # LCGDM
      'lcgdm': {
        'CONFIGURATION': 'lcgdm.HEAD',
        'URLNAME_PREFIX': 'lcgdm_head',
        'PROJECT_CONFIGURATION': 'glite_3_2_cert',
        'BUILD': {
          'GL32': {
            'ARCHITECTURES': ['sl5_ia32_gcc412'],
            'TEST': {
              'glite-SE_dpm_mysql': ('yaimgen-test', 'yaimgen-test.FAV'),
              'glite-SE_dpm_disk' : ('yaimgen-test', 'yaimgen-test.FAV'),
              'glite-LFC_mysql'   : ('yaimgen-test', 'yaimgen-test.FAV'),
              'glite-LFC_oracle'  : ('yaimgen-test', 'yaimgen-test.FAV.ORA'),
            },
          },
          'GL32_64': {
            'ARCHITECTURES': ['sl5_x86_64_gcc412'],
          }
        },
        'TEST': {
          'glite-SE_dpm'     : ('multi.LCGDM', 'multi.glite-SE_dpm'),
          'glite-LFC_mysql'  : ('multi.LCGDM', 'multi.glite-LFC_mysql'),
          'glite-LFC_oracle' : ('multi.LCGDM', 'multi.glite-LFC_oracle')
        }
      },
      
      # DATA
      'org.glite.data': {
        'CONFIGURATION': 'org.glite.data.HEAD',
        'URLNAME_PREFIX': 'lcgutil_head',
        'BUILD': {
          'GL32': {
            'PROJECT_CONFIGURATION': 'glite_3_2_cert',
            'ARCHITECTURES': ['sl5_x86_64_gcc412'],
            'TEST': {
              'glite-lcgutil': ('multi.data', 'multi.lcgutil')
            },
          },
          'GL32-2': {
            'PROJECT_CONFIGURATION': 'glite_3_2_cert',
            'ARCHITECTURES': ['sl5_ia32_gcc412']
          }
        }
      },
     
    } # END SUBSYSTEMS
  } # END PROJECT GLITE
}

# This variables will be added to the environment
# This is usefull to pass some values to the etics-client
ENVIRON = {
  'NMI_submitter_host': 'etics.cern.ch',
  'ETICS_REPOSITORY_SERVER': 'etics-repository.cern.ch'
}

# ETICS base for the reports
ETICS_BASE      = 'http://etics-repository.cern.ch/repository/reports/id'
ETICS_REPO_BASE = 'http://etics-repository.cern.ch/repository/pm/volatile/repomd/id'
ETICS_REPO_BASE_NAME = 'http://etics-repository.cern.ch/repository/pm/volatile/repomd/name'

# Time saket-agents.py will wait between checks (in seconds)
AGENT_WAIT = 300
# Timeout for the agent
AGENT_TIMEOUT = datetime.timedelta(hours=10)

# Version of ETICS client
ETICS_CLIENT = None

# Parameters needed for the testing
# component and repository properties are automatically added
import passwd
TEST_PROPERTIES = {
  'usercert'          : '${HOME}/user-cert.pem',
  'userkey'           : '${HOME}/user-key.pem',
  'userpass.HIDDEN'   : passwd.USER_KEY_PASSWD,
  'oracleuser'        : passwd.ORACLE_USERS,
  'oraclepass.HIDDEN' : passwd.ORACLE_PASSWD,
  'syncuser'          : passwd.SYNC_USER,
  'syncpass.HIDDEN'   : passwd.SYNC_PASSWD,
}

# Printers
# This classes will be used to generate reports of the retrieved information
PRINTERS = (
  #'Saket.Printers.XMLPrinter.XMLPrinter',
  'Saket.Printers.MailPrinter.MailPrinter',
  #'Saket.Printers.PlainPrinter.PlainPrinter',
  'Saket.Printers.StoragePrinter.StoragePrinter',
)

# Template
REPORT_TEMPLATE  = os.path.join(os.path.dirname(__file__), 'Templates/report.xml')
SUMMARY_TEMPLATE = os.path.join(os.path.dirname(__file__), 'Templates/summary.xml')

# For the mail
import socket
HOSTNAME = socket.getfqdn()

MAIL_PRINTER = {
  'SMTP_HOST': 'localhost',
  'FROM_ADDR': 'SAKET <saket.dms@cern.ch>',
  'TO_ADDR':   ['it-dep-gt-dms@cern.ch'],
  'SUBJECT':   'Status of DM HEAD',
  'HTM_TEMPLATE': os.path.join(os.path.dirname(__file__), 'Templates/sak2htm.xsl')
}

# For the storage of logs
STORAGE_PRINTER = {
  'LOCATION':      '/afs/cern.ch/project/gd/www/dms/saket',
  'TEMPLATE':      REPORT_TEMPLATE,
  'REPORT_TITLE':  'Status of DM HEAD',
  'XSLT':          {'Templates/xslt/report.xsl': 'index.html'},     # Style: Absolute or relative to LOCATION / Out: Absolute or relative to the report folder
  'SUMMARY_TITLE': 'SAKET Archive',
  'SUMMARY_FNAME': 'summary.xml',
  'SUMMARY_XSLT':  {'Templates/xslt/summary.xsl':  'index.html',
                    'Templates/xslt/calendar.xsl': 'calendar.html'} # Style: Absolute or relative to LOCATION / Out: Absolute or relative to LOCATION
}
