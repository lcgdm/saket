##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# Configuration file
#
##############################################################################

__name__ = 'BAMBOO'

import datetime
import os.path

# Debug
DEBUG = True

# Projects
PROJECTS = {
  'EMI': {
    'ENABLED': True,
    'PROJECT_CONFIGURATION': 'emi_R_2_prod',
    'URLNAME_TEST_INFIX': 'test',
    'CHECKOUT_EXTRA': '--noask --continueonerror --verbose --frombinary --runtimedeps',
    'BUILD_EXTRA': '--verbose --continueonerror',

    'PARSER' : {
      'BUILD_LOG'      : 'build-status.xml',
      'BUILD_LOG_LINK' : 'index.html',
      'DEPLOY_LOG'     : 'deployment.xml',
      'DEPLOY_LOG_LINK': 'deployment.xml',
      'TEST_LOG'       : 'tests.xml',
      'TEST_LOG_LINK'  : 'tests.xml',

      'BUILD_LOG_PARSER' : 'Saket.Parsers.ETICSParser.ETICSParser',
      'DEPLOY_LOG_PARSER': 'Saket.Parsers.SaketParser.SaketParser',
      'TEST_LOG_PARSER'  : 'Saket.Parsers.SaketParser.SaketParser'
    },

    'SUBSYSTEMS': {

      # LCGDM
      'lcgdm': {
        'URLNAME_PREFIX': 'bamboo_lcgdm',
        'CONFIGURATION': None,
        'EXTERNAL_REPO': 'http://svn.cern.ch/guest/lcgdm/extras/build/repos/lcgdm-cbuilds-el5-nightly.repo',
        'BUILD': {
          'SL5': {
            'ARCHITECTURES': ['sl5_x86_64_gcc412EPEL'],
          }
        },
        'TEST': {
          'emi-dpm_mysql'  : ('emi.favourite', 'emi.favourite.HEAD'),
          'emi-dpm_disk'   : ('emi.favourite', 'emi.favourite.HEAD'),
          'emi-lfc_mysql'  : ('emi.favourite', 'emi.favourite.HEAD'),
          'emi-lfc_oracle' : ('emi.favourite', 'emi.favourite.ORACLE')
        }
      },
     
    }
  }
}

# This variables will be added to the environment
# This is usefull to pass some values to the etics-client
ENVIRON = {
  'NMI_submitter_host': 'etics.cern.ch',
  'ETICS_REPOSITORY_SERVER': 'etics-repository.cern.ch'
}

# ETICS base for the reports
ETICS_BASE      = 'http://etics-repository.cern.ch/repository/reports/id'
ETICS_REPO_BASE = 'http://etics-repository.cern.ch/repository/pm/volatile/repomd/id'
ETICS_REPO_BASE_NAME = 'http://etics-repository.cern.ch/repository/pm/volatile/repomd/name'

# Time saket-agents.py will wait between checks (in seconds)
AGENT_WAIT = 20
# Timeout for the agent
AGENT_TIMEOUT = datetime.timedelta(hours=10)

# Version of ETICS client
ETICS_CLIENT = None

# Parameters needed for the testing
# component and repository properties are automatically added
import passwd
TEST_PROPERTIES = {
  'usercert'          : '${HOME}/user-cert.pem',
  'userkey'           : '${HOME}/user-key.pem',
  'userpass.HIDDEN'   : passwd.USER_KEY_PASSWD,
  'oracleuser'        : passwd.ORACLE_USERS,
  'oraclepass.HIDDEN' : passwd.ORACLE_PASSWD,
  'syncuser'          : passwd.SYNC_USER,
  'syncpass.HIDDEN'   : passwd.SYNC_PASSWD,
  'voms'              : 'testers.eu-emi.eu',
  'oracleversion'     : '11',
}

# Printers
# This classes will be used to generate reports of the retrieved information
PRINTERS = (
  #'Saket.Printers.XMLPrinter.XMLPrinter',
  'Saket.Printers.MailPrinter.MailPrinter',
  #'Saket.Printers.PlainPrinter.PlainPrinter',
  'Saket.Printers.StoragePrinter.StoragePrinter',
)

# Template
REPORT_TEMPLATE  = os.path.join(os.path.dirname(__file__), 'Templates/report.xml')
SUMMARY_TEMPLATE = os.path.join(os.path.dirname(__file__), 'Templates/summary.xml')

# For the mail
import socket
HOSTNAME = socket.getfqdn()

MAIL_PRINTER = {
  'SMTP_HOST': 'localhost',
  'FROM_ADDR': 'SAKET <saket.dms@cern.ch>',
  'TO_ADDR':   ['dpm-devel@cern.ch'],
  'SUBJECT':   'Status of DM HEAD - Bamboo',
  'HTM_TEMPLATE': os.path.join(os.path.dirname(__file__), 'Templates/sak2htm.xsl')
}

# For the storage of logs
STORAGE_PRINTER = {
  'LOCATION':      '/afs/cern.ch/project/gd/www/dms/saket-bamboo',
  'TEMPLATE':      REPORT_TEMPLATE,
  'REPORT_TITLE':  'Status of DM HEAD - Bamboo',
  'XSLT':          {'Templates/xslt/report.xsl': 'index.html'},     # Style: Absolute or relative to LOCATION / Out: Absolute or relative to the report folder
  'SUMMARY_TITLE': 'SAKET Archive - EMI',
  'SUMMARY_FNAME': 'summary.xml',
  'SUMMARY_XSLT':  {'Templates/xslt/summary.xsl':  'index.html',
                    'Templates/xslt/calendar.xsl': 'calendar.html'} # Style: Absolute or relative to LOCATION / Out: Absolute or relative to LOCATION
}

