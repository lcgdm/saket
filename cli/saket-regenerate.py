#! /usr/bin/python
##############################################################################
#  Copyright 2011 the European Middleware Initiative
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez Ayllon <alejandro.alvarez.ayllon@cern.ch>, CERN
#
# This script will regenerate the static HTML file from the reports
#
##############################################################################

import sys
from Saket.Environment import setupEnvironment

# Main function
def main():
  from Saket.Printers.StoragePrinter import StoragePrinter
  printer = StoragePrinter()
  printer.summary()
  

##############################################################################
# Executing this module from the command line
##############################################################################
if __name__ == "__main__":
    try:
        setupEnvironment(sys.argv[1:])
        sys.exit(main())
    except KeyboardInterrupt:
        print '\nExecution interrupted by the user!'
        sys.exit(1)
